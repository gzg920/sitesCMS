undertow.resourcePath = src/main/webapp,classpath:webapp,G:/upload

# gzip 压缩开关
undertow.gzip.enable=true
# 配置压缩级别，默认值 -1。 可配置 1 到 9。 1 拥有最快压缩速度，9 拥有最高压缩率
undertow.gzip.level=-1
# 触发压缩的最小内容长度
undertow.gzip.minLength=1024