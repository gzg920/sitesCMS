#sql("getBySiteSign")
	select * from site t where t.siteSign = ? limit 1
#end

#sql("getAllSites")
	select * from site t order by t.status asc,t.createTime desc
#end

#sql("cancelMainSite")
	update site t set t.status = '2' where t.status = '1'
#end

#sql("getMainSite")
	select * from site t where t.status = '1' limit 1
#end