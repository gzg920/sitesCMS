#namespace("account")
	#include("account.sql")
#end

#namespace("role")
	#include("role.sql")
#end

#namespace("permission")
	#include("permission.sql")
#end

#namespace("site")
	#include("site.sql")
#end

#namespace("column")
	#include("column.sql")
#end

#namespace("article")
	#include("article.sql")
#end

#namespace("accessLog")
	#include("accessLog.sql")
#end

###下面都是cds使用的

#namespace("directive")
	#include("directive.sql")
#end

#namespace("cds")
	#include("cds.sql")
#end