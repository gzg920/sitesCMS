/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50096
Source Host           : localhost:3307
Source Database       : sitescms

Target Server Type    : MYSQL
Target Server Version : 50096
File Encoding         : 65001

Date: 2020-10-03 16:44:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for accesslog
-- ----------------------------
DROP TABLE IF EXISTS `accesslog`;
CREATE TABLE `accesslog` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(50) default NULL COMMENT '访问类型（cds、cms）',
  `ip` varchar(50) default NULL COMMENT '访问者ip地址',
  `visitor` varchar(50) default NULL COMMENT '访问者姓名，cms访问记录用户登录名',
  `actionKey` varchar(512) default NULL COMMENT 'actionKey',
  `parameter` text COMMENT '访问携带的参数',
  `accessTime` datetime default NULL COMMENT '访问时间',
  `remark` varchar(512) default NULL COMMENT '其他信息',
  `siteId` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of accesslog
-- ----------------------------
INSERT INTO `accesslog` VALUES ('1', 'cds', '172.18.127.36', null, '/', null, '2020-09-21 20:07:44', null, '0');
INSERT INTO `accesslog` VALUES ('2', 'cms', '172.18.127.36', 'wumoxi', '/cms', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 20:07:50', null, '0');
INSERT INTO `accesslog` VALUES ('3', 'cds', '172.18.127.36', null, '/', null, '2020-09-21 20:07:54', null, '0');
INSERT INTO `accesslog` VALUES ('4', 'cms', '172.18.127.36', 'wumoxi', '/cms', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 20:07:56', null, '0');
INSERT INTO `accesslog` VALUES ('5', 'cms', '172.18.127.36', 'wumoxi', '/cmsLog/enterCdsLogView', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 20:08:02', null, '0');
INSERT INTO `accesslog` VALUES ('6', 'cds', '172.18.127.36', null, '/enterColPicArts', 'Parameter   : col=case  limit=4  ', '2020-09-21 20:08:12', null, '0');
INSERT INTO `accesslog` VALUES ('7', 'cms', '172.18.127.36', 'wumoxi', '/cmsLog/enterCdsLogView', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 20:08:15', null, '0');
INSERT INTO `accesslog` VALUES ('8', 'cms', '172.18.127.36', 'wumoxi', '/cms', null, '2020-09-21 20:18:37', null, '0');
INSERT INTO `accesslog` VALUES ('9', 'cms', '172.18.127.36', 'wumoxi', '/cmsSite/enterSiteMng', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 20:19:18', null, '0');
INSERT INTO `accesslog` VALUES ('10', 'cms', '192.168.3.7', 'wumoxi', '/cmsSite/enterSiteMng', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 20:19:29', null, '0');
INSERT INTO `accesslog` VALUES ('11', 'cms', '192.168.3.7', 'wumoxi', '/cms', null, '2020-09-21 20:40:18', null, '0');
INSERT INTO `accesslog` VALUES ('12', 'cms', '192.168.3.7', 'wumoxi', '/cmsSite/enterSiteMng', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 20:40:23', null, '0');
INSERT INTO `accesslog` VALUES ('13', 'cds', '192.168.3.7', null, '/', null, '2020-09-21 20:40:55', null, '0');
INSERT INTO `accesslog` VALUES ('14', 'cds', '192.168.3.7', null, '/enterColPicArts', 'Parameter   : col=case  limit=4  ', '2020-09-21 20:41:01', null, '0');
INSERT INTO `accesslog` VALUES ('15', 'cds', '192.168.3.7', null, '/', null, '2020-09-21 20:41:08', null, '0');
INSERT INTO `accesslog` VALUES ('16', 'cms', '192.168.3.7', 'wumoxi', '/cmsArt/enterArtAdd', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 20:41:15', null, '0');
INSERT INTO `accesslog` VALUES ('17', 'cms', '127.0.0.1', 'wumoxi', '/cmsArt/saveArt', 'Parameter   : thumbnail=169  isTop=0  subtitle=  column=28  contentText=环保科技公司-仿这是一个纯仿站点，使用的是一个现成的模板直接复制的将模板上的标签改成sitesCMS的自定义指令就能直接用了这个仅仅是仿制用于演示，所以功能并不完整  type=1  title=环保科技公司-仿  content=<p>环保科技公司-仿</p><p>这是一个纯仿站点，使用的是一个现成的模板直接复制的</p><p>将模板上的标签改成sitesCMS的自定义指令就能直接用了</p><p>这个仅仅是仿制用于演示，所以...  ', '2020-09-21 20:46:42', null, '0');
INSERT INTO `accesslog` VALUES ('18', 'cms', '127.0.0.1', 'wumoxi', '/cmsArt/enterArtAdd', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 20:46:42', null, '0');
INSERT INTO `accesslog` VALUES ('19', 'cds', '127.0.0.1', null, '/', null, '2020-09-21 20:46:46', null, '0');
INSERT INTO `accesslog` VALUES ('20', 'cms', '127.0.0.1', 'wumoxi', '/cmsArt/enterArtMng', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 20:46:56', null, '0');
INSERT INTO `accesslog` VALUES ('21', 'cms', '127.0.0.1', 'wumoxi', '/cmsArt/deleteArt', 'UrlPara     : 75\n', '2020-09-21 20:47:04', null, '0');
INSERT INTO `accesslog` VALUES ('22', 'cms', '127.0.0.1', 'wumoxi', '/cmsArt/enterArtMng', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 20:47:04', null, '0');
INSERT INTO `accesslog` VALUES ('23', 'cms', '127.0.0.1', 'wumoxi', '/cmsArt/enterArtMng', 'Parameter   : column=28  title=  type=0  ', '2020-09-21 20:47:12', null, '0');
INSERT INTO `accesslog` VALUES ('24', 'cms', '127.0.0.1', 'wumoxi', '/cmsArt/enterArtEdit', 'UrlPara     : 59-1\nParameter   : _pjax=#rightMain  ', '2020-09-21 20:47:15', null, '0');
INSERT INTO `accesslog` VALUES ('25', 'cms', '127.0.0.1', 'wumoxi', '/cmsArt/updateArt', 'Parameter   : thumbnail=168  isTop=1  subtitle=  column=28  contentText=sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体...  id=59  title=sitesCMS官网  content=<p>sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCM...  ', '2020-09-21 20:47:20', null, '0');
INSERT INTO `accesslog` VALUES ('26', 'cms', '127.0.0.1', 'wumoxi', '/cmsArt/enterArtEdit', 'UrlPara     : 59-1\nParameter   : _pjax=#rightMain  ', '2020-09-21 20:47:20', null, '0');
INSERT INTO `accesslog` VALUES ('27', 'cds', '127.0.0.1', null, '/', null, '2020-09-21 20:47:27', null, '0');
INSERT INTO `accesslog` VALUES ('28', 'cms', '127.0.0.1', 'wumoxi', '/cmsSite/enterSiteAdd', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 20:54:02', null, '0');
INSERT INTO `accesslog` VALUES ('29', 'cms', '192.168.3.7', 'wumoxi', '/cmsSite/saveSite', 'Parameter   : siteDes=由layui模板改造而成  siteUrl=  fileMaxSize=5  fileSuffix=  siteName=类友网络-仿  siteSign=layuiNet  ', '2020-09-21 20:55:28', null, '0');
INSERT INTO `accesslog` VALUES ('30', 'cms', '192.168.3.7', 'wumoxi', '/cmsSite/enterSiteAdd', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 20:55:28', null, '0');
INSERT INTO `accesslog` VALUES ('31', 'cms', '192.168.3.7', 'wumoxi', '/cmsSite/enterSiteMng', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 21:11:21', null, '0');
INSERT INTO `accesslog` VALUES ('32', 'cms', '192.168.3.7', 'wumoxi', '/cmsSite/enterSiteEdit', 'UrlPara     : 14\nParameter   : _pjax=#rightMain  ', '2020-09-21 21:11:55', null, '0');
INSERT INTO `accesslog` VALUES ('33', 'cms', '192.168.3.7', 'wumoxi', '/cmsSite/enterSiteAdd', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 21:12:18', null, '0');
INSERT INTO `accesslog` VALUES ('34', 'cms', '192.168.3.7', 'wumoxi', '/cmsSite/enterSiteMng', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 21:12:27', null, '0');
INSERT INTO `accesslog` VALUES ('35', 'cms', '192.168.3.7', 'wumoxi', '/cmsSite/enterSiteAdd', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 21:12:29', null, '0');
INSERT INTO `accesslog` VALUES ('36', 'cms', '192.168.3.7', 'wumoxi', '/cmsSite/enterSiteMng', 'Parameter   : _pjax=#rightMain  ', '2020-09-21 21:12:39', null, '0');
INSERT INTO `accesslog` VALUES ('37', 'cds', '192.168.3.7', null, '/', null, '2020-10-03 16:44:36', null, '0');

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` int(11) NOT NULL auto_increment,
  `nickName` varchar(50) default NULL COMMENT '昵称',
  `userName` varchar(128) default NULL COMMENT '登录名',
  `password` varchar(128) default NULL COMMENT '密码',
  `status` varchar(1) default '0' COMMENT '状态（0正常，1锁定，2删除）',
  `createTime` datetime default NULL COMMENT '创建时间',
  `updateTime` datetime default NULL COMMENT '更新时间',
  `department` int(11) default NULL,
  `siteId` int(11) default NULL COMMENT '站点id',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES ('1', '测试用户1', 'test1', '3d2172418ce305c7d16d4b05597c6a59', '1', null, '2020-01-08 22:11:11', null, '0');
INSERT INTO `account` VALUES ('2', '测试用户2', 'test2', '96e79218965eb72c92a549dd5a330112', '1', null, null, null, '0');
INSERT INTO `account` VALUES ('3', '测试用户3', 'test3', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-04 22:18:50', null, null, '0');
INSERT INTO `account` VALUES ('4', '测试用户4', 'test4', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-05 17:21:52', null, null, '0');
INSERT INTO `account` VALUES ('7', '测试用户7', 'test7', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-05 18:34:58', null, null, '0');
INSERT INTO `account` VALUES ('8', '测试用户8', 'test8', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-05 18:35:04', null, null, '0');
INSERT INTO `account` VALUES ('9', '测试用户9', 'test9', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-05 18:35:10', null, null, '0');
INSERT INTO `account` VALUES ('10', '测试用户10', 'test10', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-05 18:35:16', null, null, '0');
INSERT INTO `account` VALUES ('11', '测试用户11', 'test11', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-05 18:35:45', null, null, '0');
INSERT INTO `account` VALUES ('12', '测试用户12', 'test12', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-07 21:44:42', null, null, '0');
INSERT INTO `account` VALUES ('13', '测试用户13', 'test13', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-07 21:45:34', null, null, '0');
INSERT INTO `account` VALUES ('14', '测试用户14', 'test14', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-07 21:47:25', null, null, '0');
INSERT INTO `account` VALUES ('15', '测试用户15', 'test15', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-07 22:01:24', null, null, '0');
INSERT INTO `account` VALUES ('16', '15', '15', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-07 22:06:43', null, null, '0');
INSERT INTO `account` VALUES ('17', '16', '16', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-07 22:09:31', null, null, '0');
INSERT INTO `account` VALUES ('18', '17', '17', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-07 22:10:10', null, null, '0');
INSERT INTO `account` VALUES ('19', '1811', '18', '9db06bcff9248837f86d1a6bcf41c9e7', '1', '2020-01-07 22:14:50', null, null, '0');
INSERT INTO `account` VALUES ('20', '1911', '19', '9db06bcff9248837f86d1a6bcf41c9e7', '2', '2020-01-07 22:15:18', null, null, '0');
INSERT INTO `account` VALUES ('21', '2022', '20', 'e3ceb5881a0a1fdaad01296d7554868d', '1', '2020-01-08 20:03:51', '2020-01-08 22:10:43', null, '0');
INSERT INTO `account` VALUES ('23', '测试2020', '2020', '96e79218965eb72c92a549dd5a330112', '2', '2020-01-09 21:30:33', null, null, '0');
INSERT INTO `account` VALUES ('24', '永字诀11', 'yzj', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-24 12:02:10', '2020-01-24 12:02:30', null, '0');
INSERT INTO `account` VALUES ('25', '小张', 'xiaozhang', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-29 22:37:39', null, null, '0');
INSERT INTO `account` VALUES ('26', '小李', 'xiaoli', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-29 22:38:18', null, null, '0');
INSERT INTO `account` VALUES ('27', '五摩西', 'wumoxi', '96e79218965eb72c92a549dd5a330112', '1', '2020-01-31 12:10:29', null, null, '0');
INSERT INTO `account` VALUES ('28', '11112222', '111', '96e79218965eb72c92a549dd5a330112', '2', '2020-01-31 12:34:42', '2020-03-08 21:27:37', null, '0');
INSERT INTO `account` VALUES ('35', '五摩西', 'wumoxi', '96e79218965eb72c92a549dd5a330112', '1', '2020-02-09 17:10:03', null, null, '12');
INSERT INTO `account` VALUES ('36', '五摩西', 'wumoxi', '96e79218965eb72c92a549dd5a330112', '1', '2020-09-21 20:26:08', null, null, '13');
INSERT INTO `account` VALUES ('37', '五摩西', 'wumoxi', '96e79218965eb72c92a549dd5a330112', '1', '2020-09-21 20:55:24', null, null, '14');

-- ----------------------------
-- Table structure for accountrole
-- ----------------------------
DROP TABLE IF EXISTS `accountrole`;
CREATE TABLE `accountrole` (
  `accountId` int(11) default NULL COMMENT 'account表id',
  `roleId` int(11) default NULL COMMENT 'role表id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of accountrole
-- ----------------------------
INSERT INTO `accountrole` VALUES ('25', '5');
INSERT INTO `accountrole` VALUES ('26', '4');
INSERT INTO `accountrole` VALUES ('22', '4');
INSERT INTO `accountrole` VALUES ('22', '5');
INSERT INTO `accountrole` VALUES ('33', '16');
INSERT INTO `accountrole` VALUES ('33', '17');
INSERT INTO `accountrole` VALUES ('24', '269');
INSERT INTO `accountrole` VALUES ('23', '269');
INSERT INTO `accountrole` VALUES ('33', '23');
INSERT INTO `accountrole` VALUES ('33', '1');
INSERT INTO `accountrole` VALUES ('26', '1');

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(128) default NULL,
  `subtitle` varchar(512) default NULL COMMENT '栏目英文标识，站点内唯一',
  `column` int(11) default NULL,
  `type` varchar(1) default NULL,
  `content` text COMMENT '文章类型（1普通文章，2图集）',
  `contentText` text COMMENT '纯文本格式的文章内容',
  `status` varchar(1) default NULL COMMENT '状态（0默认发布，1删除，2草稿）',
  `clickNum` int(11) default NULL COMMENT '点击数',
  `collectNum` int(11) default NULL COMMENT '收藏数',
  `favoriteCount` int(11) default NULL COMMENT '点赞数',
  `isTop` varchar(1) default NULL COMMENT '置顶',
  `createAccount` int(11) default NULL,
  `createTime` datetime default NULL,
  `updateAccount` int(11) default NULL,
  `updateTime` datetime default NULL,
  `thumbnail` int(11) default '0' COMMENT '缩略图',
  `files` varchar(256) default NULL COMMENT '附件id串，多个以,分割',
  `siteId` int(11) default NULL COMMENT '站点id',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', '测试文章', '测试文章测试文章测试文章测试文章', '8', '1', '<p>测试文章 测试文章测试文章测试文章测试文章测试文章测试文章&nbsp;</p>', null, '2', '0', '0', '0', '0', '27', '2020-02-05 15:58:13', null, null, '0', null, '0');
INSERT INTO `article` VALUES ('2', '测试文章2', '测试文章测试文章测试文章测试文章', '8', '1', '<p>测试文章 测试文章测试文章测试文章测试文章测试文章测试文章测试文章 </p>', null, '1', '0', '0', '0', '0', '27', '2020-02-05 15:59:49', '27', '2020-02-07 18:19:36', '0', '', '0');
INSERT INTO `article` VALUES ('3', '测试文章3', '测试文章测试文章测试文章测试文章-修改', '8', '1', '<p>测试文章</p><p>测试文章</p><p>测试文章</p><p>修改到电影栏目</p>', null, '1', '0', '0', '0', '0', '27', '2020-02-05 16:04:44', '27', '2020-02-07 17:28:08', '0', '', '0');
INSERT INTO `article` VALUES ('4', '测试文章4', '测试文章测试文章测试文章测试文章', '9', '1', '<p>测试文章测试文章</p><p>修改时还是没有缩略图</p>', null, '1', '0', '0', '0', '0', '27', '2020-02-05 16:06:23', '27', '2020-02-07 17:35:22', '0', '', '0');
INSERT INTO `article` VALUES ('5', '附件测试', '附件测试', '8', '1', '<p>附件测试</p>', null, '2', '0', '0', '0', '0', '27', '2020-02-06 16:24:53', '27', '2020-02-07 18:08:50', '0', '30', '0');
INSERT INTO `article` VALUES ('6', '附件测试', '附件测试', '9', '1', '<p>附件测试</p>', null, '1', '0', '0', '0', '0', '27', '2020-02-06 16:28:59', '27', '2020-02-07 18:19:50', '0', '32,33,34', '0');
INSERT INTO `article` VALUES ('7', '不带附件的文章', null, '9', '1', '<p>不带附件的文章</p>', null, '1', '0', '0', '0', '0', '27', '2020-02-06 16:33:15', null, null, '0', null, '0');
INSERT INTO `article` VALUES ('8', '一个附件的文章', null, '8', '1', '<p>一个附件的文章</p>', null, '1', '0', '0', '0', '0', '27', '2020-02-06 16:33:51', null, null, '0', '35', '0');
INSERT INTO `article` VALUES ('9', '多个附件的文章', null, '8', '1', '<p>多个附件的文章</p>', null, '1', '0', '0', '0', '0', '27', '2020-02-06 16:34:33', null, null, '0', '36,37,38', '0');
INSERT INTO `article` VALUES ('10', '富文本编辑器测试1', null, '5', '1', '<h1>标题</h1><p><span style=\"font-weight: bold;\">加粗</span></p><p><span style=\"font-size: x-large;\">字号</span></p><p><span style=\"font-size: large; font-family: 宋体;\">字体</span></p><p><span style=\"font-size: large; font-family: 宋体; font-style: italic;\">斜体</span></p><p><span style=\"text-decoration-line: underline;\">下划线</span><span style=\"font-size: large; font-family: 宋体; font-style: italic;\"><br></span></p><p><span style=\"text-decoration-line: line-through;\">删除线</span><span style=\"text-decoration-line: underline;\"><br></span></p><p><span style=\"color: rgb(194, 79, 74);\">文字颜色</span><span style=\"text-decoration-line: line-through;\"><br></span></p><p><span style=\"background-color: rgb(194, 79, 74);\">背景颜色</span></p><p><a href=\"http://www.baidu.com\" target=\"_blank\">插入连接</a><br></p><ol><li>有序列表</li><li>有序列表</li></ol><p style=\"text-align: center;\">居中</p><p style=\"text-align: right;\">靠右</p><blockquote style=\"text-align: left;\">引用一段话</blockquote><table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tbody><tr><th>&nbsp;1</th><th>2&nbsp;</th></tr><tr><td>&nbsp;11</td><td>22&nbsp;</td></tr></tbody></table><pre><code>select * from table</code></pre><p><br></p>', null, '1', '4', '0', '0', '0', '27', '2020-02-06 18:36:08', null, null, '0', null, '0');
INSERT INTO `article` VALUES ('11', '富文本图片测试', null, '9', '1', '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAzMAAAHMCAIAAABukmEEAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAACdMSURBVHhe7dfroqrIEqDb8/4v3acaP3frUhScXhIY40+tGRGZQt7I+v/+DwAAY3AzAwAYhZsZAMAo3MwAAEbhZgYAMAo3MwCAUbiZAQCMws0MAGAUbmYAAKNwMwMAGIWbGQDAKNzMAABG4WYGADAKNzMAgFG4mQEAjMLNDABgFG5mAACjcDMDABiFmxkAwCjczAAARuFmBgAwCjczAIBRuJkBsB//340SsBGWLACb1M3rmaphIyxZADamO9eMimCbrGAAtqT716QQ7IhlDcDouohdKAG7Y3EDMLTuYhdKwB5Z3wCMq7uY2xiHYa0DMJyuY2dF4QAsdwAG0l3sWjk4AMsdgFF0EZsUgoOx9AH4va5jk0JwSDYAAD/WjWxSCI7KHgDgl7qRuZPBxE4A4De6kU0KweHZDAD8QDeySSHAzQyA7+tG5k4GN+wKAL6nG9mkEHDBxgDgS7qRTQoB1+wNAL6hG5k7GTxkhwDwWd3IJoWAGTYJAJ/SdeysKDDPPgHgI7qOTQoBz9gtALxZ17FJIWAZewaAd+pGNikELGbbAPA23cjcyeBVNg8Af9V17KwosJ79A8CfdB27UAJYz/4B4HXdxdzG4E3sJQBe51oG72U7AfCK053spBC8qpV0ocTx2E4ArNbHc1IIFmvpPFP1wdhRAKzTZ9OdjIdaJYvV7PAMBAAr9BX1HeVCa2JaFf3rmVNDbhkaABbpizopxFG1Dp6pmjWMGgDP9aWdFGK/mulrc/GTU0P+bpNDaREAfNPp1P1Pf7M7TfAateTd3MwAeOR05P6nv9m+ZvShSvm6Dd/M7qoCgHfobHW6blAzN/nnz7tOrfi5rc5E62ixmgGwTKfnpBCjap7WqCXj2dvctOJWqjEAkw7HSSHG0KwsUxs25SjT1iJdoAYAB+Y8HMppOh6rlO0zl/9X6/pGaYCDcQZ+32nMT/758x+nevbKBP+rhX+jNMDedeo59/6scZzxtOB/Tr1xEOb7kfbEPVUA7EgH3KQQ6zWC69WeY7MOFmnTAMC1x9+I00cElrNoXtSeA4AbfSpgPasHgP5vsz+A37EPAY7OtQzGYSsCHNrpWvaf/gZ+ylYEOK4uZa5lMAy7EeCIupFNCgEDsCEBDqcb2aQQMAZ7EuBYupG5k8GQ7EyAo+hGNikEDGbQzenUAHiv04XspBAwnhH3ZyfHWVEA/sCJCpuwgZvZ/5QGYKWOUQcpDG8Du7Tj5EIJAJ7p3JwUAga2mY3auXKjNAA3OigdlbAdm9yunTTXygEw6XB0PMKmbHvHdupcKwdwVJ2Gk0LARuxn03YIXSsHcBgdfw5A2KYdbt3OpHuqANipDjvHHWzWnndv59NDlQJsX+eakw227FgbuENrRkUAW9MpNikEbNPR93An2Y3SAGPrzJoUArbMTv5/OtuulQMYT+fUpBCwcTbzHZ1zN0oDjMHRBPtjSz9xOvgeqxTgi5w/sEt29VKnQ3CtGgO8VUeMQwZ2x65+gw7IBWoA8KpOk0khYEds7M/q+JxREcAynR2TQsC+2Ntf1YE6oyKAezopnBWwa3b4L3XK3lMFwKSjweEAe2eTD6Rzd0ZFwPF0CjgH4ADs80F1DD9UKbBrbXhbHo7BVt+MzuaHKgX2or1td8Nh2O0b1oH9PvULDKBtOSkEHIANvyud4n9Wd8CPtBVtRjge2570HbhRGviW9p7dB4dk53NHn4UbpYGPabPZbnBUNj9P9JW4URp4k7bWpBBwPPY/K/TRWKnGwLx2i/0Ch+cU4EV9RtarPXDW3rA7ADczvqBvzoyK4KjaCfYCMHEW8G19he6pAo6hdT8pBBye44Af67u0Uo1hs1rKFjNwzaHAQPpSrVRj2I7WrtUL3HAusEl91q6Vg7G1Xq1Y4B5HA9vWJ+5aORhPa9QqBWY4HdiPvngXSvABDfEB9MLvUI9WJjDPAcEO9fW7Vu5IenM+oCFeo5auZcBDzgj2rC/hAjXYhV7pfer3qBqFZ6qeUZFrGXzXFjedY4JDOH0U/67uxtPz3SjNuzW+91RxoYTpgC9q100KbYSTAv6vtu9iNfupHuVGab6r0b/wT/BUBnxC2+yeKrbDYQErtNEvlHi3el+sZgygKblQAviAttk9VWyNIwNe0b6/UOIldbFADRheE3atHPA37ahr5bbPSQF/0pFwocSMip6pmm1qFqd57F8zTvXAQu2ca+X2wrkA79EJcaHEs2/zf6pjF5rUm2ktOq864Fo75Fq5PXIWwDt1Zsyrjp1qmhdPdNXXygH39kiJ/XIEwBMdBh/Tz7B9zehLc1rLG6XhYNoAZ0WPwbaHOzoMHqp0jVouUAM2ommbFHpVvdxTBexXa/1aucOw1TmWNvqNx6mP6mceqpQhNUkfmKb6vVEadqTFfaHE8djhHEV7fZna/E7PcU8VjKFZ+cq89Es3SsM2tY7Pih6YIWD/2u5nRTelR7+nCn6hOfj6LPSr18rBprR8J4UOz0CwW+31s6Lb1/vcUwVf0aAPMOw9x6QQDK8lOynEZA/D0cTeKM1hNPE3Su9UL/lQpbxPIzvS2PZAF0rAeFqjk0KcbX5Emtg1asmWNZfPVH0YvfZiNWONxm5SaCQ92YyK4EdaiGdFubbncWnm16glo2qeZlTEjQZomdpwowGaFBpbz3qj9PvULxvXdH5AP3CtHDeOODQtisVqxu80ExdK8DeN5kOVHl7DMSm0Qb3ApNBL6oKdaprfpE7PijLPGF1p4cyrjq9o0C+U4JMa63uqOJ7efy8j0MtcmIufPEj9z6kHNq25vFDiVfUyKcQCBuu5ltWN0rxb43utHF/XBNwofQC98JCv3JN9XT/PTjXNF0osUINr5VjGeK3TKnumatZrBM+KMoZm5UbpPeoNx3vHHutv6uuQM8sSrYOHKp0UulCCNYza61p3a9SSexqjSSEG1lTdKL0Xo73U6XkulXiTOr1RmkNqEaxUY9Yzdh/RwlypxsfT+08KsSlN3o3S29Q7TAr9Ts9xocQn9Us3SsOkZfFM1SxgsL6qFbpYzXaql7xQgi1rLv+s7n6hJzgr+gs9wYUS39Vv3yjNkTT391RxVnSNWh6egRhIa3NedRvXy1wrx740u+9Qj5/X700KfUw/s0xtBtADPVQp+9Ls3ij9UKWf0W/shf0ztBbdYjUbUo94oQTcaInMq+6t6npS6A/q6M/qbjw93zNVs03N4rVyH9APvEM9bpNtsyWtuMVq9lM9yoUSsEarZ0ZFr6qXSaH1ar9GLfeoN1yjlgyjiblWbjA93LVyG2Qz7ErrcUZF39KvXigBf9aSmlHRMrWZFFqsZvdUcXgNx6vqhW9p3C+U4IsM+p61sWZU9G71fqEEfExL7aFKr5V7ll2iNryqcXymat6qwb1Rmu8y7gfSVptR0avq5UIJ+LqW4Of1e3xMA80XNfT8jjk4rnbhMrWZUdGkEPxaK/IP6oiRNDe8W+PLAEwGaXf+QR3BT7Uc363eAT7MccMifZ0Wqxl8SyvvQol7qpipKbdADQDex8nCan2UFqsZfECL7EKJGRVNCi1TmwVqAPAShwgr9OU5K3pPFdfKwZ+1pC6UeKjS9y3FulumNgAPOSxYpG/LWdFlanOtHKy839yql4cqnRT6mH5mmdoAnDkXeKSvx1nRV9XLjIrYnSZ4vdr/Wd39eo31ECvVGDgM2577+iycFX2TOl2sZmxHM7dMbT6j3xh1FfVwy9QG2DVbnSt9AS6U+Lx+b73a82vNx43Sv9ATbHmR9AI3SgO7Y3uTzvuzomPomZapDV/RoN8o/VM9yo6WRO8zrzpgy+xkhr6TLdFz31MFL2kQF6vZGHqmna6B3m2NWgLDs10PrTP7rOjG9TIL1OBgevk3qdOR9GSTQkfVKFwoAQzMRj2iDukLJXan13tJXexCr7RSjbej5z4ripGBTbFFD6RT+Vq5Q2oIXlUvA+tBr5XbnV5vUogbDdC1csAY7MlD6AC+UIJ5jdRiNRtAD3St3E71khb2Mg3WvOqAX7ADd66D9qwo79CYzqjoW/rVa+X2rre1vF/V8M2oCPgWu263OlbPivIxDfQytZlX3Uvq4hh6Zyv8rRrTZ6oG3srW2puOzAsl+K5G/4v64SPpzS3yT2qIn6ka+DPbaQ86Gm+UZjzN0B/U0YE1EIbid5qAa+WAV9lFm9dxeFaUwTQ9Myo6K7pADQ6ml58U4qeajBkVAcvYMxvWsTcpxDCamBkV/UEdHWzqe+ezogyjiVmmNsA1e2OTOtgmhfiRpuGZqt+t3g+wDHrPs6JsQXP2qnqBw7Dot6SD6kIJPq8RX6xmn9fvzatuy3qTSSG2rxl9SV3AHlnfo+sculGaT2qsH6r0p3qUd6jHkfRk1vxRNf3zqoO9sKbH1alzoQQf1nBfK7cXvdW86n6tp7H4udayWKAGsB1W7Yg6Uc6K8kmN9bVyR9KbnxX9hZ5gUgjuaZWsVGMYj9U5ls6Ms6J8TAN9rdyBNRD3VPFh/Zi54M9aSfOqg2FYlAPpnJgU4jMa5WvlOGtcFqjBm9SpGeGTWmQXSsCvWYtD6GCYFOKtGtwbpVmsgXum6pVqPCkEH9aCW6xm8DEW2Y+11yeFeJOG9Z4qeJOG9Z4qFqiB2eEXWnx/UEfwZxbTb7SVL5TgbxrNG6X5lsb9Qol7qpgUglG1UudVB6+yhr6tvXuhBC9pEO+pgp9qMs6KXihhvti41vGN0rCYRfMl7dELJVivEbynCgbT9Fz4J3gqgx1oTc+oCOZZJZ/VXrxQgpUavhul2Yim7UIJ2KkW+rzq4Mya+Ig23IUS3NMYLVYzNqgpfKZq2J2W+Bq13Jqe/iV18QGf7v8tnIDvdJryf5Q7sAbib+qLLWsuJ4WeLY+K+LMG9Cv6SZZp1Faq8ah6yj+ru7+pr2vlhmQLvU2zfVb0kBqCxWrGfjXTk0IPVXqhBC9pEL+oH+YdGtP3qd+P6Wf+8EO1nxR6Vb1cKDEw++cNmu1JoePp/e+pgqNqHaxfCTW7UZpnGq+zoh/Wj00K8QEN8fvU76vq5UKJP6ijSaE1ajkptBF2zl817Xs/g3rJZWrD4bUgJoVeVS8XSnBPY3St3Of1e2dFGUYTc08VK9X4Qol3qMcFfVZ3o/R22DOva84nhXahV1qpxnCzhIq+SZ3OqOjYGotJoV/oCc6KDqaH+7O627LeZDz/e7bTc946Ze+qYmv2sJ6+rzmfFNqOnnulGsNDLZcLJT6gH1imNnvX204K/VpPc1b0F3qCD+vHtql3GFvPOil0ocTG7eQ1vqbJPyu6BT3xAjWANVo9k0K/0BM8VOm+9G5nRUfSk00KfVK/tEANXlIX69V+s3qNs7vB/5zicyo6K3qj9Nlt5D+nyt3Y2/t8SJN/ocTwetwLJeB9WlsDr66e76FKN6VHv1BiVD3lpNA71OMzVf9BHb1VXW9Hz33x5P19VnSZ2kwK3Sh9TxX7ss+3+rvm/Ebp4fW4F0rAW7W8JoUG1oMuVrMh9YgXSgyvx50UWqnGz1T9PvV7rdyFEpNCN0pfKze2nvX8tP1xdgquVeNJoQslrpXbqZ2/3lrN+T1VjK1nvVACPqBFtpdl1ss8VOnv9BwXSmxNTz8ptExtbpT+mH5m8Q9VvaC+ukmhIfWIk3/+/M+p5i/qaMZlzenfO7b/N1ziNNmXSgypR3ymaviM1tmuV1pvuEANPqAfuKeKzeo1zoqeFZ1R0Vf0k5NCy9RmUmhedZNCw+ixZlT0N/V1TxWTQuMN0Xvt/PUeaHqvlfudnuMP6gg+qdV2vPXWaz9U6TvU44USO9KLrVTjz+v3JoVWWt72VHmpxE/1KPdU8Qd1dKP0PU8LdmDnr3fpNJ1zKvqWfvUldQE/0kK0FC80IivV+J4qJoV2rVe9Vu5HeohJoZfUxVnRedVdKPFd/fbZP5FTzV/U0YV/4qc/bz3O7sPOX+8/p1m8VXq92n9GvwFDaplOCnGjAVqsZpNCZ0V3rVe9p4qv6+fPiv5NfU0KLVCDs6Kf1C/Nq26NWs6o6MJc/OSU/U9/79Fu362pu1BipRq/W73DRrRwJ4X4swZ0RkU71UteKDEzLOU+rB87K/o+9bum5xpcK/du9X52GzxFlqvZjIpulH5W0B97tLd3O03Ypbn4rYVl/zn1CQfRup8U4h0a02vl9qv3vFDiWrkbpT+gH5gU+oB+YFJogRrcKP1ndTcp9OcBqeWk0GI1m2lY7n2vP5qdvFizdG0uvsqpfzimtsGk0Db1DkN68ISn1D70ShdKLFObs6LvUI9nRT+pX7pQYrGaTQq9ql4m//z5P6fKp6q+Vm692s/08CC1A9t+sdPc/ONBHFiunbOFvdODTv75c3Cn579U4kbprenpr5V7SV2cFX1JXVwo8S396oUSy9TmrOgytbkwF3yq0nuqeFW93OtnLr4Pm3yx05T840EcWKXNMyk0mB5ujVpuSo9+rdzwetwLJf6s7t6nfn+hJ7hWbpnavE/9PlP1tXLvM9ftKf6f/t6XLb1V83DtQRx4QVtoyE3Uk91TxX71njdKj6QnOyv6Gf3Gq+plGD3WWdFlavM39TWvumvlPqAfuPcTJcabxL8b95Ua8pVqDLykjfTTrdQTPFTpITUEF0r8Wk9zoQQrNXyf0W8sU5sZFX1YP3bv5+biWzfoK52Ge7maAX/Qdlq8oap+h4W9nX6X/2lcbpT+ln71Qgn+ptF8kzpdpjb3VPFF/fDNTxfd3Xob8X0a6WvlgM9opz3baxV9WD/GMo3ajdKf1C+dFWWbmsVr5X5t7mHm4ptmIwGPPu39a8apOaNpeq6Ve4d6vFCCDWoKb5QeQ89081R3g1tnO8HRnY62hWrD8JqwG6XXqOWMitiOZu6eKobUI14/5G1kB2wqOJDTKfZAdexOE/w+9ctGNG0zKhpej3vxwP29rwVpd8FudWItUxv2rvleqcZsUFN4rdwG3T7/KXJSaOPsN9iVzqd5/5Sd/gR2o719o/TG9TKTQpNCu3hN5zJsRgfP2W3kH6dW/yg3KQTsRXv7Wrkdufted4Nb5GiGoZ3OmuVqNqMidzLYnfb2pNB+9Z7Xb1po+6/vgIbf6AhZqcYvqQvXMti4dvKMivaut50U2ssp54yGz+qcmPzz5xKnTv6oviaFgA1qG8+o6DB67bO7kS1yTMPbdBjcOyDuOrX6tH7seKc27EZ7+EKJw2s4LvwTP/25LWYX3qAzYEZFv9ATOMdha9q6N0pzowGaXP55ym6IOYY/aeufFR1ADzQpBAys7TqjIp5pvK4vZ/85ZTdhiGdt2GBsj9fqaTEPomdymsPXtffeoR75g4byrOjYfv+UjRZsVkt5GD2WYx0+r832JnXKBzTEWxhk6wD2o4NnUgh4SRvpJXXBYLYyNRYQ7MTpk/Cf/gZWagutVGN4E0sKNq/vw6QQ8FAb5qFK4busPNiwPiCTQsA97ZNnqobfsQphq/qSTArBsbUflqkNDMbShO3pwzIpBIfXlphXHYzNSoUt6QszKQTH0Lp/pmrYLIsYNqMvj28PB9BaX6/2sFkWMWxA35xJIdiLVvYCNYBds9BhdH2UfJbYixb0M1XDwVj6MLS+Ub5SbF9L+Z4qADczGFafrEkh2I7W7oyKgBu2B4yoz5cPGNvU8r1RGphnn8BY+oJNCsHYWq83SgNr2Dkwir5mk0IwttbrjIqANewcGEKfskkhGE9r9EZp4M9sJ/ixvmyTQjCYFug9VQBvYlPBz/RlmxSC32ktPlM18Bn2GPxGXznfOcbQcpxXHfBhP95s7fiD6eU5sJaCxcDvtASvlQN+55f7sJPg2BoLDqOJnxSCr2jZzasO+Clb8Qc6BW+UZr+aaXPNV7Ta5lUHjMTO/LEOyBul2ZGm1uTyFa22a+WAgdmoA+nsnFcdW9P8TQrBH7SYFqgBsB327aA6VkfSk7FSw2cA+YPW0Eo1BjbF1t2SjttR9ZQL1OCnepQP68d8I1mjRfNQpcDu2N4s1Qdh73rbP6u7SSF4phVzTxXA3tntHFSfu8VqtkxtPqPfYC+a12vlvqVf5ZBaBAzDlMCVziqWadRYplF7qNKv6CdhXmuFLzLo8DadZJNCH9APDK/H5axxuaeKb+lXr5XjeFoBy9SGTzLK8B6dWwc7uXrnNWp5AL3wtXKf1C+d3Ub+51QPD7RWZlTEWxlWeINOKefUhUZkDD3Th/VjD1X6qno5u40sceoKXtAamlcdf2AQ4U86jSaFeKjB+qke5a3q+lq5V9XLerWHD2vBrVd77jE68LrOGKfM8JqnGRW9ql4mhV5VLw9VCkNqma5UYyaGA17UieJM2aBmbkZFz1R9rdwatbynCtidlvg9VRyYIYBXdIQ4RHahubynigslLpR4qNJnqoaDaQPMq+4YHASwTueEj+h+NcHf0q8C63dfzfbFoQBLdRJMCrF3zfef1R3wkjbSMrXZLOcFLNKOnxRi15rsa3fjp/r/KXpPFcCbtLWeqXojnBTwRDt7Uohda7KfqfqZqu+pAnirNth6tf81RwPMarNOCrF3zffknz9PTmWvqYt7qgA+oG22QA1+ynEAd7RHJ4XYnSZ4sZq9T/2uVGNgp4bb5J09C9QA3q0VNinEljWXr6qXD+vH3qqugU3Z8M3sH7WHP2gxTQqxKU3eq+plbD3rerUHxrbhvdphs1jN4J5WyaQQY2u2FqjBpNBZ0Z3qJWdUBAxmP5uzw2aBGsBZK2NSiJE0NwvU4Ebpa+UOo9e+pwpgAAfakJ1AZ0U5tlbDpBA/1WQsUIMFanBW9PAajocqBb7oiBuvI2dGRRxAU27Sf6cJeKbql9SFWX6oMVqmNsBnHHePdcY8UzX70uxOCvEtjfuMit6hHieFWKnhe0ldACvZPP/qUFmv9oyt2ZoU4vMa8Rul36quz4ryPo3sm9QpcGZXPNHhsVKNGUzTMynExzTQN0p/QD9woQS/0BysV3s4KnvgbTpUrpXj15qPSSHeqsGdUdHH9DNnRdmC5uyhSuEYrPj36yx5qFI+rxGfFOIdGtN51X1YP3ZWlC1rLpepDeyIZf1BnRyL1Yz3aWQnhXhV4zijoi/qh8+KskfN8QI1gC2zjn+jU2SNWrJMozYpxIyGaaUaf10/f6EEh9QiWK/2MB6rcyAdGAvUgHsao0khLjQ069X+R3qIa+XgrJWxRi1hGBblZnSKXCvHWeMyKXR4Dce86kbVU14oAX/QYrpQAn7NWtyeTpEZFR1P7z8pdGANxD1VbEQPPSkEb9XyulACfsQS3LBOkZfUxV70Vsc+UhuCG6U3ooe+Vg4+qdU2oyL4PKttbzpFXlUv29FzTwodSW9+o/TW9PTXysFXtOzWqCW8iSV1UJ0oD1U6qp5yUugYeucbpbepd5gUgpG0OleqMaxh3XCl4+RG6TH0TGdFd61XvVF6a3r6G6VhU1q+z1QNz1grzOo4eajSL+qHJ4X2q/e8Vm6zeo0bpWFHWtw3SsM91gfPdZasUcu3qutJoX3p3e6pYvt6H18mjqelP686Ds9S4E86URar2Uo1nhTahV5pXnVb1pvcKA2H1DZYpjYchinnIzpR1qv9WdFJoU3p0ZepzY70YtfKAdfaIYvVjN0xtQyhk+bsbmRwPegatdypXtL3A96nTbVADdggk8dYOlQeqvR3eo4FanAwvfykEPBu7bE1asnYzBMD6fB4SV18TD8zr7pjaywmhYBfaB8+UzUjMSsMoUPirOiMipapzavq5Vo5Jg3KtXLASNqfMyri18wEP9aRMCn0krp4qNIFanCtHBcamgslgC1o395TBV9n6PmNtv5Z0Q/oB26UvlbuWjluNECGCHah/TyjIj7PWPMDbfRJoa/oJ5epDfc0RpNCwL60w2dUxAcYXL6qPT0p9CM9xKvq5TB67WvlgANo218rx1sZVr6kfTwp9CM9xLVyZ0UXq9ke9YY3SgOH1EFwrRx/Yxz5hnbtpNDX9fMXSrykLmZUtH29z6QQwKSj4VX1wg1Dw2e1BSeFvqgfvlburep6gRqMrWc9KwrwUEfGerVnYjj4lDbcpNC39KvXyn1FP7lGLX+qR7lQAuDdOmXmVXc8Tl7er111VvTD+rEbpYfRY61Ry0/qly6UAPi8zp151R2D85d3ag+dFf2kfulauU3p0der/avq5awowK91Kj1T9Y44iHmPtshZ0Q/oB26U3qPecIEaLFCDCyUAxtM59ap62QjHMX/Vwp8Uerd6v6eKA2sgbpS+p4qzogAb1EH2TNVb4FDmRS32s6JvUqf3VME9jdGMfwpOTQAYitOZV/RtnxR6kzq9Vo7FGrhnqgZgGI5m1umTPin0DvV4oQTv0Ji+ql4A+DxnLiv0oX7fp7ruLpTgJQ3ijIoulFisZgB8jKP2G/qsMbwmbIN6gXnVvaQuLpQA4N2csJ/Vd4zNaiIH1oOeFf2MfmOBGgCwkgP0r/oQPVP11vT0k0IvqYsLJQbTw91TxTB6rAslPq/fW6xmACzg0HxR35xnqt6m3uEPb1H7CyW2o+cew9zznB51KD3ZQ5UCcMHhuE6flGvldqQXmxRao5YXSmxZbzKSnmx4Pe686gD478zsvzzTN+Ss6O70epNCC9TgnirgRktkmdoA7J3z7rm+DGdF96g3nBSaUdG86mCBFs0atQTYHQfcI30EzoruUW84KXRPFTdKw8e01J6pGmCzHGR3dMZfKLFTveT8a5a+UAJ+pIU4rzqArXF+/atzfVJov3rPSaFr5c6KwqhaqfdUATA2p9WVjvDD38lKXCgBG9HCfVW9AHydA+j/6Ug+8LWs0IUSsH2t6fVqD/AVDp3/qwN4UmineslJoZkvVjk4pLbBjIoAPuDoR0wH7VnRPeoNJ7eR/zmlgP9pbzxTNcDfHPo06UCdFNqdXu+ZqoEF2jbPVA2wxhHPjk7Ns6L70rs9UzXwZ22qhyoFmHe4k6ID8qzovvRuMyoCPqwtt17tgUM66M2sP/bl9Gp3VQH8VBtyjVoCh3Ggbd85Nym0cb3MvOqATWkDXysH7N3+d3un2oUS29Q7zKsO2L529T1VALuz8+3dGTYptE29w7V/Uqc/gf1pky9TG2Cb9ryHO6U2e0719NfKTQpNCgHH0M5fr/bAqPa5SzuBJoVG1VM+U/WFEs5Z4FpHw2I1A8awtz3ZSXNWdDA93AI1uFZuUghgmc6OBWoAfNeu9l7HyaTQSHqya+WWqc2kEMDfdKY8UzXwYeNutg6DPeoN16jlpBDAJ3XiPFQp8D6D7qs2/b70bivV+KwowHd1Bs2oCPizzW+nToVJob3orc6KAgyj4+laOeAl295CHQPuZAA/1Wn1UKXAQ1vdKm30SaFd6JXOigJsR+fXjIqAGZvcJO3vSaHt633OigLsRafbpBBwY3vbo23tTgawQZ1018oBm7uZtYn3so17mbOiAHvXqXdPFXBUW9oD7dpd7Nve5KwowCF1FM6oCI5hMyu+Dbq7a1khAFzRYCs3szblvq5l/Q3AQx2aZ0VhpzawxNuL29+NvcakEADLdHpeKwc7spmbWX9s0+kVTgoBsF4n6UOVwjaNvoLbZ5vdaT39WVEA3qGz9Z4qYGuGXrttr21usB79rCgAH9axOykE2zHoqm1LTQptSo8+KQTAF3UEnxWF4Y24WNtG7mQA/EFn8Y3SMKThFmj7ZuPXsv4GYAydzvdUAWMYZUW2P86KbkfPPSkEwJA6rO+pAn5niFXYhjgruhE99KQQANvRCX5PFfBFlt3r2rhnRQHYrA70GRXBJ1lnL2qbTgoBsC+d8jdKwwdYXqu1LyeFADiAjv6zovBWFtY6bUcbEuCo+gycFYU3saSWagtOCgFwVH0PLpSAv7GSFmnb2XgAXOjbcKM0rGf1PNEmmxQCgGt9J+6pApaxYma1pSaFAGCBPh43SsM8q+Rf7Z4LJQBgpT4kN0rDDYsj7ZULJQDgHfq6XCsHZ9bE/9X+OCsKAB/Qx+ZGaY7t6Oug3TApBABf0efnnio4noPOfQv/rCgA/E7fpGvlOIzDTXkr/UIJABhD36cZFbFTx5rgFrVlDcAW9NF6qFL2Yv8z2sq9UAIANqiP2Y3SbNxuJ7J1eqM0AOxCn7d7qmBT9jltLcmzogCwU33w5lXH8HY4Va1BqxCAY+tzeE8VjGdXc9NymxQCgMPr0/hM1fzUTqahNTUpBADM6JP5UKV81ybHvSVzTxUAwEp9SudVxydtb5RbHTdKAwDv0Pf1Rmk+Y2Pj26KwLADg6/oG3yjNO2xgNJv2CyUAgO/qS7xMbVhj6FFrYq+VAwAG0Od5mdowb9wxag4nhQCA7egrPqMiro04Ls3YpBAAsH193deo5WGM9cJNwqQQALBHfe9fUhd79Pt3a4yvlQMAjq2bwT1V7MuP36qhvVACAGBGl4Z51W2QmxAAsD1dwZ6pejt2dTNrEgD4ug5iGEPr8kKJ4e1nLzXwAPxa5zL8WivyQomB2T8AvEHfvWvlYAAtyhulh2HbAPBmffGulYOfajneU8Wv2SoAfEpfvGeqhq9rCV4o8Tv2AwAf10fvVfUCn9RqmxT6BcsdgB/rY7hMbeADWmQXSnyRJQ7A6PpI3igN79YKOyv6FZY1AFvSp/JGaXirltek0IdZygBsVR/MedXBq1pJF0p8jFULwOb1zVysZrBGq2dS6AOsTgB2q6/oYjWDGS2Ua+XexCoE4HD6oq5RS7i3fkq8g6UGALP68J4VhbNWxqTQ31hkAPBc396HKuV4WgGTQq+yjABgqb69a9SSA2jK/zbpVgwAvEef5WVqA9esDAD4oC5iD1UKbmYA8BNdyp6pmsMw5QDwS13BFqgBu2aaAWBcXcoeqpRdMJ0AsA1dxBarGZti2gBg27qIPVQpwzNVALBPXcpulGZIpgcA9q9L2YyKGIDJAIAD6S62TG34IoMOAEfXRWylGvNWhhUAeK7r2IyK+DNDCQC8okvZjdK8xPABAH/VpWyZ2nCP0QEA3qn710o1PjwDAQB8VXexlWq8d25mAMDvdf96SV3sgpsZALAB3cKeqXqz3MwAgG3rUnZPFdvhZgYA7Ec3spVqPAA3MwBgt7p5LVaz33EzAwCOrnvZQ5V+mJsZAMD/1RVsjVq+j5sZAMBz3cUWqMFL3MwAAF7UXeyZqhdwMwMAeKeuYzdKP+RmBgDwWV3NFlzO3MwAAD5uybXsP25mAACjcDMDABiFmxkAwCjczAAARuFmBgAwCjczAIBRuJkBAIzCzQwAYBRuZgAAo3AzAwAYhZsZAMAo3MwAAEbhZgYAMAo3MwCAUbiZAQCMws0MAGAUbmYAAKNwMwMAGIWbGQDAKNzMAABG4WYGADAKNzMAgFG4mQEAjMLNDABgFG5mAACjcDMDABiFmxkAwCjczAAARuFmBgAwCjczAIBRuJkBAIzCzQwAYBRuZgAAo3AzAwAYhZsZAMAo3MwAAEbhZgYAMIb/83/+fyM/aAX67h+SAAAAAElFTkSuQmCC\" style=\"max-width:100%;\"><br></p>', null, '1', '0', '0', '0', '0', '27', '2020-02-06 20:44:43', null, null, '0', null, '0');
INSERT INTO `article` VALUES ('12', '附件类型测试', null, '8', '1', '<p>附件类型测试</p>', null, '1', '0', '0', '0', '0', '27', '2020-02-07 11:24:04', null, null, '0', '40', '0');
INSERT INTO `article` VALUES ('13', '缩略图测试', null, '8', '1', '<p>缩略图测试</p>', null, '1', '0', '0', '0', '0', '27', '2020-02-07 11:54:38', null, null, '43', null, '0');
INSERT INTO `article` VALUES ('14', '缩略图测试2', '新增一个缩略图', '8', '1', '<p>缩略图测试2</p>', null, '1', '0', '0', '0', '0', '27', '2020-02-07 11:55:33', '27', '2020-02-07 17:37:41', '0', '', '0');
INSERT INTO `article` VALUES ('15', '缩略图+附件', '取消附件和缩略图', '8', '1', '<p>缩略图+附件</p>', null, '1', '0', '0', '0', '0', '27', '2020-02-07 11:56:29', '27', '2020-02-07 17:36:10', '0', '', '0');
INSERT INTO `article` VALUES ('16', '基本文章', '没有缩略图', '9', '1', '<p>没有缩略图</p><p>修改的时候还是没有缩略图</p>', null, '1', '0', '0', '0', '0', '27', '2020-02-07 17:31:51', '27', '2020-02-07 17:32:35', '0', '', '0');
INSERT INTO `article` VALUES ('17', '有缩略图', null, '8', '1', '<p>有缩略图</p><p>第一次修改：去除缩略图</p><p>第二次修改：再次添加缩略图</p>', null, '1', '0', '0', '0', '0', '27', '2020-02-07 17:41:07', '27', '2020-02-07 17:44:30', '59', '', '0');
INSERT INTO `article` VALUES ('18', '只有附件', null, '8', '1', '<p>只有附件</p><p>第一次修改：去除附件</p><p>第二次修改：添加附件</p><p>第三次修改：添加多个附件</p><p>第四次修改：添加缩略图</p>', null, '1', '0', '0', '0', '0', '27', '2020-02-07 17:42:18', '27', '2020-02-07 17:46:02', '62', '60,61', '0');
INSERT INTO `article` VALUES ('19', '中共中央 国务院关于抓好“三农”领域重点工作确保如期实现全面小康的意见', null, '6', '1', '<p>党的十九大以来，党中央围绕打赢脱贫攻坚战、实施乡村振兴战略作出一系列重大部署，出台一系列政策举措。农业农村改革发展的实践证明，党中央制定的方针政策是完全正确的，今后一个时期要继续贯彻执行。</p><p>2020年是全面建成小康社会目标实现之年，是全面打赢脱贫攻坚战收官之年。党中央认为，完成上述两大目标任务，脱贫攻坚最后堡垒必须攻克，全面小康“三农”领域突出短板必须补上。小康不小康，关键看老乡。脱贫攻坚质量怎么样、小康成色如何，很大程度上要看“三农”工作成效。全党务必深刻认识做好2020年“三农”工作的特殊重要性，毫不松懈，持续加力，坚决夺取第一个百年奋斗目标的全面胜利。</p><p>做好2020年“三农”工作总的要求是，坚持以习近平新时代中国特色社会主义思想为指导，全面贯彻党的十九大和十九届二中、三中、四中全会精神，贯彻落实中央经济工作会议精神，对标对表全面建成小康社会目标，强化举措、狠抓落实，集中力量完成打赢脱贫攻坚战和补上全面小康“三农”领域突出短板两大重点任务，持续抓好农业稳产保供和农民增收，推进农业高质量发展，保持农村社会和谐稳定，提升农民群众获得感、幸福感、安全感，确保脱贫攻坚战圆满收官，确保农村同步全面建成小康社会。</p><p>一、坚决打赢脱贫攻坚战</p><p>（一）全面完成脱贫任务。脱贫攻坚已经取得决定性成就，绝大多数贫困人口已经脱贫，现在到了攻城拔寨、全面收官的阶段。要坚持精准扶贫，以更加有力的举措、更加精细的工作，在普遍实现“两不愁”基础上，全面解决“三保障”和饮水安全问题，确保剩余贫困人口如期脱贫。进一步聚焦“三区三州”等深度贫困地区，瞄准突出问题和薄弱环节集中发力，狠抓政策落实。对深度贫困地区贫困人口多、贫困发生率高、脱贫难度大的县和行政村，要组织精锐力量强力帮扶、挂牌督战。对特殊贫困群体，要落实落细低保、医保、养老保险、特困人员救助供养、临时救助等综合社会保障政策，实现应保尽保。各级财政要继续增加专项扶贫资金，中央财政新增部分主要用于“三区三州”等深度贫困地区。优化城乡建设用地增减挂钩、扶贫小额信贷等支持政策。深入推进抓党建促脱贫攻坚。</p><p>（二）巩固脱贫成果防止返贫。各地要对已脱贫人口开展全面排查，认真查找漏洞缺项，一项一项整改清零，一户一户对账销号。总结推广各地经验做法，健全监测预警机制，加强对不稳定脱贫户、边缘户的动态监测，将返贫人口和新发生贫困人口及时纳入帮扶，为巩固脱贫成果提供制度保障。强化产业扶贫、就业扶贫，深入开展消费扶贫，加大易地扶贫搬迁后续扶持力度。扩大贫困地区退耕还林还草规模。深化扶志扶智，激发贫困人口内生动力。</p><p>（三）做好考核验收和宣传工作。严把贫困退出关，严格执行贫困退出标准和程序，坚决杜绝数字脱贫、虚假脱贫，确保脱贫成果经得起历史检验。加强常态化督导，及时发现问题、督促整改。开展脱贫攻坚普查。扎实做好脱贫攻坚宣传工作，全面展现新时代扶贫脱贫壮阔实践，全面宣传扶贫事业历史性成就，深刻揭示脱贫攻坚伟大成就背后的制度优势，向世界讲好中国减贫生动故事。</p><p>（四）保持脱贫攻坚政策总体稳定。坚持贫困县摘帽不摘责任、不摘政策、不摘帮扶、不摘监管。强化脱贫攻坚责任落实，继续执行对贫困县的主要扶持政策，进一步加大东西部扶贫协作、对口支援、定点扶贫、社会扶贫力度，稳定扶贫工作队伍，强化基层帮扶力量。持续开展扶贫领域腐败和作风问题专项治理。对已实现稳定脱贫的县，各省（自治区、直辖市）可以根据实际情况统筹安排专项扶贫资金，支持非贫困县、非贫困村贫困人口脱贫。</p><p>（五）研究接续推进减贫工作。脱贫攻坚任务完成后，我国贫困状况将发生重大变化，扶贫工作重心转向解决相对贫困，扶贫工作方式由集中作战调整为常态推进。要研究建立解决相对贫困的长效机制，推动减贫战略和工作体系平稳转型。加强解决相对贫困问题顶层设计，纳入实施乡村振兴战略统筹安排。抓紧研究制定脱贫攻坚与实施乡村振兴战略有机衔接的意见。</p><p>二、对标全面建成小康社会加快补上农村基础设施和公共服务短板</p><p>（六）加大农村公共基础设施建设力度。推动“四好农村路”示范创建提质扩面，启动省域、市域范围内示范创建。在完成具备条件的建制村通硬化路和通客车任务基础上，有序推进较大人口规模自然村（组）等通硬化路建设。支持村内道路建设和改造。加大成品油税费改革转移支付对农村公路养护的支持力度。加快农村公路条例立法进程。加强农村道路交通安全管理。完成“三区三州”和抵边村寨电网升级改造攻坚计划。基本实现行政村光纤网络和第四代移动通信网络普遍覆盖。落实农村公共基础设施管护责任，应由政府承担的管护费用纳入政府预算。做好村庄规划工作。</p><p>（七）提高农村供水保障水平。全面完成农村饮水安全巩固提升工程任务。统筹布局农村饮水基础设施建设，在人口相对集中的地区推进规模化供水工程建设。有条件的地区将城市管网向农村延伸，推进城乡供水一体化。中央财政加大支持力度，补助中西部地区、原中央苏区农村饮水安全工程维修养护。加强农村饮用水水源保护，做好水质监测。</p><p>（八）扎实搞好农村人居环境整治。分类推进农村厕所革命，东部地区、中西部城市近郊区等有基础有条件的地区要基本完成农村户用厕所无害化改造，其他地区实事求是确定目标任务。各地要选择适宜的技术和改厕模式，先搞试点，证明切实可行后再推开。全面推进农村生活垃圾治理，开展就地分类、源头减量试点。梯次推进农村生活污水治理，优先解决乡镇所在地和中心村生活污水问题。开展农村黑臭水体整治。支持农民群众开展村庄清洁和绿化行动，推进“美丽家园”建设。鼓励有条件的地方对农村人居环境公共设施维修养护进行补助。</p><p>（九）提高农村教育质量。加强乡镇寄宿制学校建设，统筹乡村小规模学校布局，改善办学条件，提高教学质量。加强乡村教师队伍建设，全面推行义务教育阶段教师“县管校聘”，有计划安排县城学校教师到乡村支教。落实中小学教师平均工资收入水平不低于或高于当地公务员平均工资收入水平政策，教师职称评聘向乡村学校教师倾斜，符合条件的乡村学校教师纳入当地政府住房保障体系。持续推进农村义务教育控辍保学专项行动，巩固义务教育普及成果。增加学位供给，有效解决农民工随迁子女上学问题。重视农村学前教育，多渠道增加普惠性学前教育资源供给。加强农村特殊教育。大力提升中西部地区乡村教师国家通用语言文字能力，加强贫困地区学前儿童普通话教育。扩大职业教育学校在农村招生规模，提高职业教育质量。</p><p>（十）加强农村基层医疗卫生服务。办好县级医院，推进标准化乡镇卫生院建设，改造提升村卫生室，消除医疗服务空白点。稳步推进紧密型县城医疗卫生共同体建设。加强乡村医生队伍建设，适当简化本科及以上学历医学毕业生或经住院医师规范化培训合格的全科医生招聘程序。对应聘到中西部地区和艰苦边远地区乡村工作的应届高校医学毕业生，给予大学期间学费补偿、国家助学贷款代偿。允许各地盘活用好基层卫生机构现有编制资源，乡镇卫生院可优先聘用符合条件的村医。加强基层疾病预防控制队伍建设，做好重大疾病和传染病防控。将农村适龄妇女宫颈癌和乳腺癌检查纳入基本公共卫生服务范围。</p><p>（十一）加强农村社会保障。适当提高城乡居民基本医疗保险财政补助和个人缴费标准。提高城乡居民基本医保、大病保险、医疗救助经办服务水平，地级市域范围内实现“一站式服务、一窗口办理、一单制结算”。加强农村低保对象动态精准管理，合理提高低保等社会救助水平。完善农村留守儿童和妇女、老年人关爱服务体系。发展农村互助式养老，多形式建设日间照料中心，改善失能老年人和重度残疾人护理服务。</p><p>（十二）改善乡村公共文化服务。推动基本公共文化服务向乡村延伸，扩大乡村文化惠民工程覆盖面。鼓励城市文艺团体和文艺工作者定期送文化下乡。实施乡村文化人才培养工程，支持乡土文艺团组发展，扶持农村非遗传承人、民间艺人收徒传艺，发展优秀戏曲曲艺、少数民族文化、民间文化。保护好历史文化名镇（村）、传统村落、民族村寨、传统建筑、农业文化遗产、古树名木等。以“庆丰收、迎小康”为主题办好中国农民丰收节。</p><p>（十三）治理农村生态环境突出问题。大力推进畜禽粪污资源化利用，基本完成大规模养殖场粪污治理设施建设。深入开展农药化肥减量行动，加强农膜污染治理，推进秸秆综合利用。在长江流域重点水域实行常年禁捕，做好渔民退捕工作。推广黑土地保护有效治理模式，推进侵蚀沟治理，启动实施东北黑土地保护性耕作行动计划。稳步推进农用地土壤污染管控和修复利用。继续实施华北地区地下水超采综合治理。启动农村水系综合整治试点。</p><p>三、保障重要农产品有效供给和促进农民持续增收</p><p>（十四）稳定粮食生产。确保粮食安全始终是治国理政的头等大事。粮食生产要稳字当头，稳政策、稳面积、稳产量。强化粮食安全省长责任制考核，各省（自治区、直辖市）2020年粮食播种面积和产量要保持基本稳定。进一步完善农业补贴政策。调整完善稻谷、小麦最低收购价政策，稳定农民基本收益。推进稻谷、小麦、玉米完全成本保险和收入保险试点。加大对大豆高产品种和玉米、大豆间作新农艺推广的支持力度。抓好草地贪夜蛾等重大病虫害防控，推广统防统治、代耕代种、土地托管等服务模式。加大对产粮大县的奖励力度，优先安排农产品加工用地指标。支持产粮大县开展高标准农田建设新增耕地指标跨省域调剂使用，调剂收益按规定用于建设高标准农田。深入实施优质粮食工程。以北方农牧交错带为重点扩大粮改饲规模，推广种养结合模式。完善新疆棉花目标价格政策。拓展多元化进口渠道，增加适应国内需求的农产品进口。扩大优势农产品出口。深入开展农产品反走私综合治理专项行动。</p><p>（十五）加快恢复生猪生产。生猪稳产保供是当前经济工作的一件大事，要采取综合性措施，确保2020年年底前生猪产能基本恢复到接近正常年份水平。落实“省负总责”，压实“菜篮子”市长负责制，强化县级抓落实责任，保障猪肉供给。坚持补栏增养和疫病防控相结合，推动生猪标准化规模养殖，加强对中小散养户的防疫服务，做好饲料生产保障工作。严格落实扶持生猪生产的各项政策举措，抓紧打通环评、用地、信贷等瓶颈。纠正随意扩大限养禁养区和搞“无猪市”、“无猪县”问题。严格执行非洲猪瘟疫情报告制度和防控措施，加快疫苗研发进程。加强动物防疫体系建设，落实防疫人员和经费保障，在生猪大县实施乡镇动物防疫特聘计划。引导生猪屠宰加工向养殖集中区转移，逐步减少活猪长距离调运，推进“运猪”向“运肉”转变。加强市场监测和调控，做好猪肉保供稳价工作，打击扰乱市场行为，及时启动社会救助和保障标准与物价上涨挂钩联动机制。支持奶业、禽类、牛羊等生产，引导优化肉类消费结构。推进水产绿色健康养殖，加强渔港建设和管理改革。</p><p>（十六）加强现代农业设施建设。提早谋划实施一批现代农业投资重大项目，支持项目及早落地，有效扩大农业投资。以粮食生产功能区和重要农产品生产保护区为重点加快推进高标准农田建设，修编建设规划，合理确定投资标准，完善工程建设、验收、监督检查机制，确保建一块成一块。如期完成大中型灌区续建配套与节水改造，提高防汛抗旱能力，加大农业节水力度。抓紧启动和开工一批重大水利工程和配套设施建设，加快开展南水北调后续工程前期工作，适时推进工程建设。启动农产品仓储保鲜冷链物流设施建设工程。加强农产品冷链物流统筹规划、分级布局和标准制定。安排中央预算内投资，支持建设一批骨干冷链物流基地。国家支持家庭农场、农民合作社、供销合作社、邮政快递企业、产业化龙头企业建设产地分拣包装、冷藏保鲜、仓储运输、初加工等设施，对其在农村建设的保鲜仓储设施用电实行农业生产用电价格。依托现有资源建设农业农村大数据中心，加快物联网、大数据、区块链、人工智能、第五代移动通信网络、智慧气象等现代信息技术在农业领域的应用。开展国家数字乡村试点。</p><p>（十七）发展富民乡村产业。支持各地立足资源优势打造各具特色的农业全产业链，建立健全农民分享产业链增值收益机制，形成有竞争力的产业集群，推动农村一二三产业融合发展。加快建设国家、省、市、县现代农业产业园，支持农村产业融合发展示范园建设，办好农村“双创”基地。重点培育家庭农场、农民合作社等新型农业经营主体，培育农业产业化联合体，通过订单农业、入股分红、托管服务等方式，将小农户融入农业产业链。继续调整优化农业结构，加强绿色食品、有机农产品、地理标志农产品认证和管理，打造地方知名农产品品牌，增加优质绿色农产品供给。有效开发农村市场，扩大电子商务进农村覆盖面，支持供销合作社、邮政快递企业等延伸乡村物流服务网络，加强村级电商服务站点建设，推动农产品进城、工业品下乡双向流通。强化全过程农产品质量安全和食品安全监管，建立健全追溯体系，确保人民群众“舌尖上的安全”。引导和鼓励工商资本下乡，切实保护好企业家合法权益。制定农业及相关产业统计分类并加强统计核算，全面准确反映农业生产、加工、物流、营销、服务等全产业链价值。</p><p>（十八）稳定农民工就业。落实涉企减税降费等支持政策，加大援企稳岗工作力度，放宽失业保险稳岗返还申领条件，提高农民工技能提升补贴标准。农民工失业后，可在常住地进行失业登记，享受均等化公共就业服务。出台并落实保障农民工工资支付条例。以政府投资项目和工程建设领域为重点，开展农民工工资支付情况排查整顿，执行拖欠农民工工资“黑名单”制度，落实根治欠薪各项举措。实施家政服务、养老护理、医院看护、餐饮烹饪、电子商务等技能培训，打造区域性劳务品牌。鼓励地方设立乡村保洁员、水管员、护路员、生态护林员等公益性岗位。开展新业态从业人员职业伤害保障试点。深入实施农村创新创业带头人培育行动，将符合条件的返乡创业农民工纳入一次性创业补贴范围。</p><p>四、加强农村基层治理</p><p>（十九）充分发挥党组织领导作用。农村基层党组织是党在农村全部工作和战斗力的基础。要认真落实《中国共产党农村基层组织工作条例》，组织群众发展乡村产业，增强集体经济实力，带领群众共同致富；动员群众参与乡村治理，增强主人翁意识，维护农村和谐稳定；教育引导群众革除陈规陋习，弘扬公序良俗，培育文明乡风；密切联系群众，提高服务群众能力，把群众紧密团结在党的周围，筑牢党在农村的执政基础。全面落实村党组织书记县级党委备案管理制度，建立村“两委”成员县级联审常态化机制，持续整顿软弱涣散村党组织，发挥党组织在农村各种组织中的领导作用。严格村党组织书记监督管理，建立健全党委组织部门牵头协调，民政、农业农村等部门共同参与、加强指导的村务监督机制，全面落实“四议两公开”。加大农村基层巡察工作力度。强化基层纪检监察组织与村务监督委员会的沟通协作、有效衔接，形成监督合力。加大在青年农民中发展党员力度。持续向贫困村、软弱涣散村、集体经济薄弱村派驻第一书记。加强村级组织运转经费保障。健全激励村干部干事创业机制。选优配强乡镇领导班子特别是乡镇党委书记。在乡村开展“听党话、感党恩、跟党走”宣讲活动。</p><p>（二十）健全乡村治理工作体系。坚持县乡村联动，推动社会治理和服务重心向基层下移，把更多资源下沉到乡镇和村，提高乡村治理效能。县级是“一线指挥部”，要加强统筹谋划，落实领导责任，强化大抓基层的工作导向，增强群众工作本领。建立县级领导干部和县直部门主要负责人包村制度。乡镇是为农服务中心，要加强管理服务，整合审批、服务、执法等方面力量，建立健全统一管理服务平台，实现一站式办理。充实农村人居环境整治、宅基地管理、集体资产管理、民生保障、社会服务等工作力量。行政村是基本治理单元，要强化自我管理、自我服务、自我教育、自我监督，健全基层民主制度，完善村规民约，推进村民自治制度化、规范化、程序化。扎实开展自治、法治、德治相结合的乡村治理体系建设试点示范，推广乡村治理创新性典型案例经验。注重发挥家庭家教家风在乡村治理中的重要作用。</p><p>（二十一）调处化解乡村矛盾纠纷。坚持和发展新时代“枫桥经验”，进一步加强人民调解工作，做到小事不出村、大事不出乡、矛盾不上交。畅通农民群众诉求表达渠道，及时妥善处理农民群众合理诉求。持续整治侵害农民利益行为，妥善化解土地承包、征地拆迁、农民工工资、环境污染等方面矛盾。推行领导干部特别是市县领导干部定期下基层接访制度，积极化解信访积案。组织开展“一村一法律顾问”等形式多样的法律服务。对直接关系农民切身利益、容易引发社会稳定风险的重大决策事项，要先进行风险评估。</p><p>（二十二）深入推进平安乡村建设。推动扫黑除恶专项斗争向纵深推进，严厉打击非法侵占农村集体资产、扶贫惠农资金和侵犯农村妇女儿童人身权利等违法犯罪行为，推进反腐败斗争和基层“拍蝇”，建立防范和整治“村霸”长效机制。依法管理农村宗教事务，制止非法宗教活动，防范邪教向农村渗透，防止封建迷信蔓延。加强农村社会治安工作，推行网格化管理和服务。开展农村假冒伪劣食品治理行动。打击制售假劣农资违法违规行为。加强农村防灾减灾能力建设。全面排查整治农村各类安全隐患。</p><p>五、强化农村补短板保障措施</p><p>（二十三）优先保障“三农”投入。加大中央和地方财政“三农”投入力度，中央预算内投资继续向农业农村倾斜，确保财政投入与补上全面小康“三农”领域突出短板相适应。地方政府要在一般债券支出中安排一定规模支持符合条件的易地扶贫搬迁和乡村振兴项目建设。各地应有序扩大用于支持乡村振兴的专项债券发行规模。中央和省级各部门要根据补短板的需要优化涉农资金使用结构。按照“取之于农、主要用之于农”要求，抓紧出台调整完善土地出让收入使用范围进一步提高农业农村投入比例的意见。调整完善农机购置补贴范围，赋予省级更大自主权。研究本轮草原生态保护补奖政策到期后的政策。强化对“三农”信贷的货币、财税、监管政策正向激励，给予低成本资金支持，提高风险容忍度，优化精准奖补措施。对机构法人在县域、业务在县域的金融机构，适度扩大支农支小再贷款额度。深化农村信用社改革，坚持县域法人地位。加强考核引导，合理提升资金外流严重县的存贷比。鼓励商业银行发行“三农”、小微企业等专项金融债券。落实农户小额贷款税收优惠政策。符合条件的家庭农场等新型农业经营主体可按规定享受现行小微企业相关贷款税收减免政策。合理设置农业贷款期限，使其与农业生产周期相匹配。发挥全国农业信贷担保体系作用，做大面向新型农业经营主体的担保业务。推动温室大棚、养殖圈舍、大型农机、土地经营权依法合规抵押融资。稳妥扩大农村普惠金融改革试点，鼓励地方政府开展县域农户、中小企业信用等级评价，加快构建线上线下相结合、“银保担”风险共担的普惠金融服务体系，推出更多免抵押、免担保、低利率、可持续的普惠金融产品。抓好农业保险保费补贴政策落实，督促保险机构及时足额理赔。优化“保险+期货”试点模式，继续推进农产品期货期权品种上市。</p><p>（二十四）破解乡村发展用地难题。坚守耕地和永久基本农田保护红线。完善乡村产业发展用地政策体系，明确用地类型和供地方式，实行分类管理。将农业种植养殖配建的保鲜冷藏、晾晒存贮、农机库房、分拣包装、废弃物处理、管理看护房等辅助设施用地纳入农用地管理，根据生产实际合理确定辅助设施用地规模上限。农业设施用地可以使用耕地。强化农业设施用地监管，严禁以农业设施用地为名从事非农建设。开展乡村全域土地综合整治试点，优化农村生产、生活、生态空间布局。在符合国土空间规划前提下，通过村庄整治、土地整理等方式节余的农村集体建设用地优先用于发展乡村产业项目。新编县乡级国土空间规划应安排不少于10%的建设用地指标，重点保障乡村产业发展用地。省级制定土地利用年度计划时，应安排至少5%新增建设用地指标保障乡村重点产业和项目用地。农村集体建设用地可以通过入股、租用等方式直接用于发展乡村产业。按照“放管服”改革要求，对农村集体建设用地审批进行全面梳理，简化审批审核程序，下放审批权限。推进乡村建设审批“多审合一、多证合一”改革。抓紧出台支持农村一二三产业融合发展用地的政策意见。</p><p>（二十五）推动人才下乡。培养更多知农爱农、扎根乡村的人才，推动更多科技成果应用到田间地头。畅通各类人才下乡渠道，支持大学生、退役军人、企业家等到农村干事创业。整合利用农业广播学校、农业科研院所、涉农院校、农业龙头企业等各类资源，加快构建高素质农民教育培训体系。落实县域内人才统筹培养使用制度。有组织地动员城市科研人员、工程师、规划师、建筑师、教师、医生下乡服务。城市中小学教师、医生晋升高级职称前，原则上要有1年以上农村基层工作服务经历。优化涉农学科专业设置，探索对急需紧缺涉农专业实行“提前批次”录取。抓紧出台推进乡村人才振兴的意见。</p><p>（二十六）强化科技支撑作用。加强农业关键核心技术攻关，部署一批重大科技项目，抢占科技制高点。加强农业生物技术研发，大力实施种业自主创新工程，实施国家农业种质资源保护利用工程，推进南繁科研育种基地建设。加快大中型、智能化、复合型农业机械研发和应用，支持丘陵山区农田宜机化改造。深入实施科技特派员制度，进一步发展壮大科技特派员队伍。采取长期稳定的支持方式，加强现代农业产业技术体系建设，扩大对特色优势农产品覆盖范围，面向农业全产业链配置科技资源。加强农业产业科技创新中心建设。加强国家农业高新技术产业示范区、国家农业科技园区等创新平台基地建设。加快现代气象为农服务体系建设。</p><p>（二十七）抓好农村重点改革任务。完善农村基本经营制度，开展第二轮土地承包到期后再延长30年试点，在试点基础上研究制定延包的具体办法。鼓励发展多种形式适度规模经营，健全面向小农户的农业社会化服务体系。制定农村集体经营性建设用地入市配套制度。严格农村宅基地管理，加强对乡镇审批宅基地监管，防止土地占用失控。扎实推进宅基地使用权确权登记颁证。以探索宅基地所有权、资格权、使用权“三权分置”为重点，进一步深化农村宅基地制度改革试点。全面推开农村集体产权制度改革试点，有序开展集体成员身份确认、集体资产折股量化、股份合作制改革、集体经济组织登记赋码等工作。探索拓宽农村集体经济发展路径，强化集体资产管理。继续深化供销合作社综合改革，提高为农服务能力。加快推进农垦、国有林区林场、集体林权制度、草原承包经营制度、农业水价等改革。深化农业综合行政执法改革，完善执法体系，提高执法能力。</p><p>做好“三农”工作，关键在党。各级党委和政府要深入学习贯彻习近平总书记关于“三农”工作的重要论述，全面贯彻党的十九届四中全会精神，把制度建设和治理能力建设摆在“三农”工作更加突出位置，稳定农村基本政策，完善新时代“三农”工作制度框架和政策体系。认真落实《中国共产党农村工作条例》，加强党对“三农”工作的全面领导，坚持农业农村优先发展，强化五级书记抓乡村振兴责任，落实县委书记主要精力抓“三农”工作要求，加强党委农村工作机构建设，大力培养懂农业、爱农村、爱农民的“三农”工作队伍，提高农村干部待遇。坚持从农村实际出发，因地制宜，尊重农民意愿，尽力而为、量力而行，把当务之急的事一件一件解决好，力戒形式主义、官僚主义，防止政策执行简单化和“一刀切”。把党的十九大以来“三农”政策贯彻落实情况作为中央巡视重要内容。</p><p>让我们更加紧密地团结在以习近平同志为核心的党中央周围，坚定信心、锐意进取，埋头苦干、扎实工作，坚决打赢脱贫攻坚战，加快补上全面小康“三农”领域突出短板，为决胜全面建成小康社会、实现第一个百年奋斗目标作出应有的贡献！</p>', null, '1', '6', '0', '0', '0', '27', '2020-02-07 18:19:09', null, '2020-02-07 18:19:09', '0', null, '0');
INSERT INTO `article` VALUES ('20', '人大将加大环境保护立法', null, '16', '1', '<p>人大将加大环境保护立法人大将加大环境保护立法人大将加大环境保护立法</p><p>人大将加大环境保护立法人大将加大环境保护立法</p>', null, '1', '0', '0', '0', '0', '35', '2020-02-09 21:01:15', null, '2020-02-09 21:01:15', '0', null, '12');
INSERT INTO `article` VALUES ('21', '污水治理', null, '16', '1', '<p>污水治理污水治理污水治理</p><p><br></p><p><br></p><p>污水治理污水治理污水治理</p>', null, '1', '0', '0', '0', '0', '35', '2020-02-09 21:01:47', null, '2020-02-09 21:01:47', '0', null, '12');
INSERT INTO `article` VALUES ('22', '新南65市', null, '16', '1', '<p>新南65市新南65市新南65市</p>', null, '1', '0', '0', '0', '0', '35', '2020-02-09 21:02:55', null, '2020-02-09 21:02:55', '0', null, '12');
INSERT INTO `article` VALUES ('23', '引入高科技技术', null, '16', '1', '<p>新南65市新南65市新南65市</p>', null, '1', '0', '0', '0', '0', '35', '2020-02-09 21:04:01', null, '2020-02-09 21:04:01', '0', null, '12');
INSERT INTO `article` VALUES ('24', '济南首个光伏', null, '16', '1', '<p>济南</p>', null, '1', '1', '0', '0', '0', '35', '2020-02-09 21:04:25', null, '2020-02-09 21:04:25', '0', null, '12');
INSERT INTO `article` VALUES ('25', '祝贺环保设备', null, '16', '1', '<p>祝贺</p>', null, '1', '15', '0', '0', '0', '35', '2020-02-09 21:04:57', null, '2020-02-09 21:04:57', '0', null, '12');
INSERT INTO `article` VALUES ('26', '空气净化器厂房', null, '18', '1', '<p>空气净化器厂房</p>', null, '1', '0', '0', '0', '0', '35', '2020-02-09 21:06:31', null, '2020-02-09 21:06:31', '0', null, '12');
INSERT INTO `article` VALUES ('27', '旋风式厂房', null, '18', '1', '<p>厂房</p>', null, '1', '0', '0', '0', '0', '35', '2020-02-09 21:07:07', null, '2020-02-09 21:07:07', '0', null, '12');
INSERT INTO `article` VALUES ('28', '厂房3', null, '18', '1', '<p>333333</p>', null, '1', '0', '0', '0', '0', '35', '2020-02-09 21:07:26', null, '2020-02-09 21:07:26', '0', null, '12');
INSERT INTO `article` VALUES ('29', '厂房4', null, '18', '1', '<p>4444444444</p>', null, '1', '1', '0', '0', '0', '35', '2020-02-09 21:07:38', null, '2020-02-09 21:07:38', '0', null, '12');
INSERT INTO `article` VALUES ('30', '厂房5', null, '18', '1', '<p>555555555555</p>', null, '1', '0', '0', '0', '0', '35', '2020-02-09 21:07:50', null, '2020-02-09 21:07:50', '0', null, '12');
INSERT INTO `article` VALUES ('31', '过滤器1', null, '24', '1', '<p>过滤器</p>', null, '1', '3', '0', '0', '0', '35', '2020-02-09 21:13:34', null, '2020-02-09 21:13:34', '66', null, '12');
INSERT INTO `article` VALUES ('32', '过滤空气', null, '24', '1', '<p>过滤器</p>', null, '1', '1', '0', '0', '0', '35', '2020-02-09 21:13:54', null, '2020-02-09 21:13:54', '67', null, '12');
INSERT INTO `article` VALUES ('33', '过滤空气2', null, '24', '1', '<p>过滤框</p>', null, '1', '2', '0', '0', '0', '35', '2020-02-09 21:14:13', null, '2020-02-09 21:14:13', '68', null, '12');
INSERT INTO `article` VALUES ('34', '公司介绍', '公司介绍摘要', '3', '1', '<p>拼图：国内优秀的HTML、CSS、JS跨屏响应式开源前端框架，使用最新浏览器技术，为快速的前端开发提供一系列的文本、图标、媒体、表格、表单、按钮、菜单、网格系统等样式工具包，占用资源小，使用拼图可以快速构建简洁、优雅而且自动适应手机、平板、桌面电脑等设备的前端界面，让前端开发像玩游戏一样快乐而轻松。&nbsp;&nbsp;<br></p>', null, '1', '0', '0', '0', '0', '27', '2020-02-16 20:54:03', null, '2020-02-16 20:54:03', '70', null, '0');
INSERT INTO `article` VALUES ('35', 'jfinal官网', null, '28', '1', '<p>jfinal官网</p>', null, '1', '1', '0', '0', '0', '27', '2020-02-16 21:15:26', null, '2020-02-16 21:15:26', '71', null, '0');
INSERT INTO `article` VALUES ('36', '拼图', null, '28', '1', '<p>拼图</p>', null, '1', '8', '0', '0', '0', '27', '2020-02-16 21:15:52', null, '2020-02-16 21:15:52', '72', null, '0');
INSERT INTO `article` VALUES ('37', 'layui', null, '28', '1', '<p>layui</p>', null, '1', '2', '0', '0', '0', '27', '2020-02-16 21:16:16', null, '2020-02-16 21:16:16', '73', null, '0');
INSERT INTO `article` VALUES ('38', 'bootstrap', null, '28', '1', '<p>bootstrap</p>', null, '1', '1', '0', '0', '0', '27', '2020-02-16 21:16:52', null, '2020-02-16 21:16:52', '74', null, '0');
INSERT INTO `article` VALUES ('39', 'vue', null, '28', '1', '<p>vue</p>', null, '1', '5', '0', '0', '0', '27', '2020-02-16 21:17:07', null, '2020-02-16 21:17:07', '75', null, '0');
INSERT INTO `article` VALUES ('40', '响应式站点', null, '28', '1', '<p>响应式站点</p><p>纯文本内容修改测试</p><p>纯文本内容修改测试&nbsp;&nbsp;<br></p>', '响应式站点纯文本内容修改测试纯文本内容修改测试&nbsp;&nbsp;', '1', '9', '0', '0', '0', '27', '2020-02-16 21:17:54', '27', '2020-02-16 22:00:18', '76', '', '0');
INSERT INTO `article` VALUES ('41', '公司简介', '纯文本内容测试', '3', '1', '<p>sitesCMS 是基于&nbsp;<strong>JFinal</strong>&nbsp;的&nbsp;<strong>多站点</strong>&nbsp;CMS内容管理系统，遵循JFinal极简设计理念，开发迅速、代码量少、学习简单、轻量级、易扩展，除JFinal外无其他重度依赖。精简的多站点功能设计，极易二次开发，一天一个网站不是梦。&nbsp;&nbsp;<br></p><p>系统源码全部开源，只要保留作者声明，你可以拿它做任何你想做的事，包括二次开发售卖，但是不能犯法。</p>', 'sitesCMS 是基于&nbsp;JFinal&nbsp;的&nbsp;多站点&nbsp;CMS内容管理系统，遵循JFinal极简设计理念，开发迅速、代码量少、学习简单、轻量级、易扩展，除JFinal外无其他重度依赖。精简的多站点功能设计，极易二次开发，一天一个网站不是梦。&nbsp;&nbsp;系统源码全部开源，只要保留作者声明，你可以拿它做任何你想做的事，包括二次开发售卖，但是不能犯法。', '1', '0', '0', '0', '0', '27', '2020-02-16 21:56:14', '27', '2020-09-20 18:34:31', '167', '', '0');
INSERT INTO `article` VALUES ('42', '联系我们', null, '27', '1', '<p><b>微信</b>：2453773196</p><p><span style=\"font-weight: bold;\">地址</span>：山东省济南市历城区</p><p><span style=\"font-weight: bold;\">邮箱</span>：2453773196@qq.com</p><p><br></p>', '微信：2453773196地址：山东省济南市历城区邮箱：2453773196@qq.com', '1', '0', '0', '0', '0', '27', '2020-02-17 22:02:23', '27', '2020-09-21 19:50:22', '0', '', '0');
INSERT INTO `article` VALUES ('43', '图以移动设备为基点，优先适应于移动设备', null, '4', '1', '<p>拼图：国内优秀的HTML、CSS、JS跨屏响应式开源前端框架，使用最新浏览器技术，为快速的前端开发提供一系列的文本、图标、媒体、表格、表单、按钮、菜单、网格系统等样式工具包，占用资源小，使用拼图可以快速构建简洁、优雅而且自动适应手机、平板、桌面电脑等设备的前端界面，让前端开发像玩游戏一样快乐而轻松。</p><p>拼图前端框架在传统CSS框架的基本上，增加了HTML5、CSS3、JS等技术组合应用，应用最新的浏览器技术，同时兼容较早的浏览器，新旧结合，承前启后，开发者只需把框架文件引入到项目中，就可以初现快速的共同开发，改变以往建立PC网站同时，再建立手机网站的局面，实现一站响应所有设备，大大提高了开发效率。</p><p>移动优先、跨屏响应：拼图以移动设备为基点，优先适应于移动设备；从移动设备扩大到平板、桌面电脑等设备，实现跨屏响应，兼容桌面浏览器的同时，更适应了移动互联网的潮流。</p><p>组件丰富、海量插件：拼图前端框架重新定义了CSS基础、常用元件及JS组件，可快速构架前端界面，实现跨屏响应。同时兼容所有jQuery插件，在项目中可以灵活使用，让前端开发如虎添翼。</p><p>轻量高效、国产开源：拼图基于应用广泛jQuery插件，轻量高效；相对于国外的前端框架，拼图前端框架侧重于对中文的支持，符合国人的视觉及体验，实现到国内主流浏览器的支持，减少兼容性测试时间，提高开发效率。</p>', '拼图：国内优秀的HTML、CSS、JS跨屏响应式开源前端框架，使用最新浏览器技术，为快速的前端开发提供一系列的文本、图标、媒体、表格、表单、按钮、菜单、网格系统等样式工具包，占用资源小，使用拼图可以快速构建简洁、优雅而且自动适应手机、平板、桌面电脑等设备的前端界面，让前端开发像玩游戏一样快乐而轻松。拼图前端框架在传统CSS框架的基本上，增加了HTML5、CSS3、JS等技术组合应用，应用最新的浏览器技术，同时兼容较早的浏览器，新旧结合，承前启后，开发者只需把框架文件引入到项目中，就可以初现快速的共同开发，改变以往建立PC网站同时，再建立手机网站的局面，实现一站响应所有设备，大大提高了开发效率。移动优先、跨屏响应：拼图以移动设备为基点，优先适应于移动设备；从移动设备扩大到平板、桌面电脑等设备，实现跨屏响应，兼容桌面浏览器的同时，更适应了移动互联网的潮流。组件丰富、海量插件：拼图前端框架重新定义了CSS基础、常用元件及JS组件，可快速构架前端界面，实现跨屏响应。同时兼容所有jQuery插件，在项目中可以灵活使用，让前端开发如虎添翼。轻量高效、国产开源：拼图基于应用广泛jQuery插件，轻量高效；相对于国外的前端框架，拼图前端框架侧重于对中文的支持，符合国人的视觉及体验，实现到国内主流浏览器的支持，减少兼容性测试时间，提高开发效率。', '1', '6', '0', '0', '0', '27', '2020-02-19 20:13:20', null, '2020-02-19 20:13:20', '0', null, '0');
INSERT INTO `article` VALUES ('44', '让前端开发像玩游戏一样快乐而轻松', null, '4', '1', '<p>拼图：国内优秀的HTML、CSS、JS跨屏响应式开源前端框架，使用最新浏览器技术，为快速的前端开发提供一系列的文本、图标、媒体、表格、表单、按钮、菜单、网格系统等样式工具包，占用资源小，使用拼图可以快速构建简洁、优雅而且自动适应手机、平板、桌面电脑等设备的前端界面，让前端开发像玩游戏一样快乐而轻松。</p><p>拼图前端框架在传统CSS框架的基本上，增加了HTML5、CSS3、JS等技术组合应用，应用最新的浏览器技术，同时兼容较早的浏览器，新旧结合，承前启后，开发者只需把框架文件引入到项目中，就可以初现快速的共同开发，改变以往建立PC网站同时，再建立手机网站的局面，实现一站响应所有设备，大大提高了开发效率。</p><p>移动优先、跨屏响应：拼图以移动设备为基点，优先适应于移动设备；从移动设备扩大到平板、桌面电脑等设备，实现跨屏响应，兼容桌面浏览器的同时，更适应了移动互联网的潮流。</p><p>组件丰富、海量插件：拼图前端框架重新定义了CSS基础、常用元件及JS组件，可快速构架前端界面，实现跨屏响应。同时兼容所有jQuery插件，在项目中可以灵活使用，让前端开发如虎添翼。</p><p>轻量高效、国产开源：拼图基于应用广泛jQuery插件，轻量高效；相对于国外的前端框架，拼图前端框架侧重于对中文的支持，符合国人的视觉及体验，实现到国内主流浏览器的支持，减少兼容性测试时间，提高开发效率。</p>', '拼图：国内优秀的HTML、CSS、JS跨屏响应式开源前端框架，使用最新浏览器技术，为快速的前端开发提供一系列的文本、图标、媒体、表格、表单、按钮、菜单、网格系统等样式工具包，占用资源小，使用拼图可以快速构建简洁、优雅而且自动适应手机、平板、桌面电脑等设备的前端界面，让前端开发像玩游戏一样快乐而轻松。拼图前端框架在传统CSS框架的基本上，增加了HTML5、CSS3、JS等技术组合应用，应用最新的浏览器技术，同时兼容较早的浏览器，新旧结合，承前启后，开发者只需把框架文件引入到项目中，就可以初现快速的共同开发，改变以往建立PC网站同时，再建立手机网站的局面，实现一站响应所有设备，大大提高了开发效率。移动优先、跨屏响应：拼图以移动设备为基点，优先适应于移动设备；从移动设备扩大到平板、桌面电脑等设备，实现跨屏响应，兼容桌面浏览器的同时，更适应了移动互联网的潮流。组件丰富、海量插件：拼图前端框架重新定义了CSS基础、常用元件及JS组件，可快速构架前端界面，实现跨屏响应。同时兼容所有jQuery插件，在项目中可以灵活使用，让前端开发如虎添翼。轻量高效、国产开源：拼图基于应用广泛jQuery插件，轻量高效；相对于国外的前端框架，拼图前端框架侧重于对中文的支持，符合国人的视觉及体验，实现到国内主流浏览器的支持，减少兼容性测试时间，提高开发效率。', '1', '15', '0', '0', '0', '27', '2020-02-19 20:13:55', '27', '2020-02-20 21:02:17', '0', '78,79', '0');
INSERT INTO `article` VALUES ('45', 'JAVA软件工程师', null, '26', '1', '<p><span style=\"font-weight: bold; font-size: medium;\">基本说明</span>：</p><p>学历：本科及以上</p><p>人数：2人</p><p>专业：计算机及相关专业</p><p><br><span style=\"font-weight: bold; font-size: medium;\">职位描述</span>：</p><p>1、负责定制类网站建设；设计师可“天马行空”的实现自己的创意，以满足客户对网站效果的需求；</p><p>2、对设计效果用采用W3C标准构建页面，Flash动画效果，简单的Javascript效果制作；</p><p>3、负责页面优化，使页面访问快捷，更加人性化，具备更为良好的易用性；<br><br><span style=\"font-weight: bold; font-size: medium;\">职位要求</span>：</p><p>1、一年以上商业网站设计，有平面设计经验者优先；精通Photoshop， Dreamweaver,flash等软件；</p><p>2、有较强的美术功底和良好的设计表现能力，能够准确把握网站的整体风格及视觉表现，对网站的结构策划有一定经验；</p><p>3、有较深美术功底，有丰富的创造力想象力，设计富有渲染力; 对设计趋势有较强的敏感度，具备开阔的眼界；</p><p>4、精通DIV+CSS书写标准，能编写多浏览器兼容页面,熟悉Html语言, 熟悉Javascript脚本语言，能够与程序员配合完成项目；</p><p>5、良好的沟通能力及团队协作能力，富有责任心，学习能力强，能承受较强的工作压力；</p><p>6、诚实正直、学习能力强、责任心强，创新意识强，具有强烈的团队合作精神。<br></p><p><br></p>', '基本说明：学历：本科及以上人数：2人专业：计算机及相关专业职位描述：1、负责定制类网站建设；设计师可“天马行空”的实现自己的创意，以满足客户对网站效果的需求；2、对设计效果用采用W3C标准构建页面，Flash动画效果，简单的Javascript效果制作；3、负责页面优化，使页面访问快捷，更加人性化，具备更为良好的易用性；职位要求：1、一年以上商业网站设计，有平面设计经验者优先；精通Photoshop， Dreamweaver,flash等软件；2、有较强的美术功底和良好的设计表现能力，能够准确把握网站的整体风格及视觉表现，对网站的结构策划有一定经验；3、有较深美术功底，有丰富的创造力想象力，设计富有渲染力; 对设计趋势有较强的敏感度，具备开阔的眼界；4、精通DIV+CSS书写标准，能编写多浏览器兼容页面,熟悉Html语言, 熟悉Javascript脚本语言，能够与程序员配合完成项目；5、良好的沟通能力及团队协作能力，富有责任心，学习能力强，能承受较强的工作压力；6、诚实正直、学习能力强、责任心强，创新意识强，具有强烈的团队合作精神。', '1', '0', '0', '0', '0', '27', '2020-02-19 21:49:05', '27', '2020-03-10 22:23:40', '0', '', '0');
INSERT INTO `article` VALUES ('46', '数据库管理员', null, '26', '1', '<p><span style=\"font-weight: bold; font-size: medium;\">职位描述</span>：</p><p>1、参与项目设计，负责项目B/S网站方面的整体架构设计、需求分析、系统建模、设计开发和内部测试工作，并参与项目实施；</p><p>2、根据系统设计方案、项目开发进度和任务分配，在规定的时间内完成高质量的编码工作；</p><p>3、对所编写的模块或程序进行严格的综合测试，进行软件故障的诊断、定位、分析和调试，以实施产品测试方案，协助测试人员完成软件系统测试。</p><p><br><span style=\"font-weight: bold; font-size: medium;\">职位要求</span>：</p><p>1、至少1年以上PHP开发经验，有大型网站架构设计经验，具备OOP开发思想、一定的系统设计能力，熟悉三层/多层模式开发架构、MVC以及主流开源框架；</p><p>2、熟悉HTTP协议，熟练掌握XML、HTML/XHTML、CSS、Javascript、AJAX、JSON、Jquery等Web页面技术；</p><p>3、熟练掌握MySQL数据库，熟练编写存储过程，具备一定的数据库管理、优化经验；</p><p>4、具备良好的编程习惯及较强的文档编写能力；具备强烈的进取心、求知欲及团队合作精神；能够承受适当压力，独挡一面；</p><p>5、有良好的沟通、表达、分析及解决问题能力，有团队合作精神，有学习新知识的渴望和优秀的学习能力，能承受一定的工作压力。<br></p><p><br></p>', '职位描述：1、参与项目设计，负责项目B/S网站方面的整体架构设计、需求分析、系统建模、设计开发和内部测试工作，并参与项目实施；2、根据系统设计方案、项目开发进度和任务分配，在规定的时间内完成高质量的编码工作；3、对所编写的模块或程序进行严格的综合测试，进行软件故障的诊断、定位、分析和调试，以实施产品测试方案，协助测试人员完成软件系统测试。职位要求：1、至少1年以上PHP开发经验，有大型网站架构设计经验，具备OOP开发思想、一定的系统设计能力，熟悉三层/多层模式开发架构、MVC以及主流开源框架；2、熟悉HTTP协议，熟练掌握XML、HTML/XHTML、CSS、Javascript、AJAX、JSON、Jquery等Web页面技术；3、熟练掌握MySQL数据库，熟练编写存储过程，具备一定的数据库管理、优化经验；4、具备良好的编程习惯及较强的文档编写能力；具备强烈的进取心、求知欲及团队合作精神；能够承受适当压力，独挡一面；5、有良好的沟通、表达、分析及解决问题能力，有团队合作精神，有学习新知识的渴望和优秀的学习能力，能承受一定的工作压力。', '1', '0', '0', '0', '0', '27', '2020-02-19 21:49:44', '27', '2020-03-10 22:24:29', '0', '', '0');
INSERT INTO `article` VALUES ('47', '综保部文章', null, '30', '1', '<p>综保部文章</p>', '综保部文章', '2', '0', '0', '0', '0', '27', '2020-03-08 12:01:27', '27', '2020-03-08 12:01:27', '0', null, '0');
INSERT INTO `article` VALUES ('48', '置顶文章测试', null, '5', '1', '<p>置顶文章测试</p>', '置顶文章测试', '1', '7', '0', '0', '1', '27', '2020-03-08 13:22:23', '27', '2020-03-08 13:25:45', '0', '', '0');
INSERT INTO `article` VALUES ('49', '图文测试', null, '8', '2', null, '图文测试', '1', '0', '0', '0', null, '27', '2020-03-08 13:36:27', null, '2020-03-08 13:36:27', '120', '121,123,122,124,127,125,126', '0');
INSERT INTO `article` VALUES ('50', '类型测试', null, '5', '1', '<p>类型测试</p>', '类型测试', '1', '1', '0', '0', '0', '27', '2020-03-08 13:41:01', '27', '2020-03-08 17:35:13', '141', '142', '0');
INSERT INTO `article` VALUES ('51', '类型测试测试', null, '8', '2', null, '类型测试', '1', '0', '0', '0', null, '27', '2020-03-08 13:41:20', null, '2020-03-08 13:41:20', '0', null, '0');
INSERT INTO `article` VALUES ('52', 'Jfinal生成静态页面，Jfina生成html页面静态化', null, '4', '1', '<p>部分Web项目需要将页面静态化，JFinal增加几行代码即可搞定。</p><p>例如：</p><p>假定原有Controller Action中首页方法如下</p><pre><code>/**<br>	 * 首页<br>	 */<br>	public void index()  {<br>		//原模版路径<br>		String yourTplPath = \"index.html\";<br>		render(yourTplPath);<br>	}</code></pre><p>静态化只需要增加几行代码即可，</p><p>经过波总的指导，有更方便的写法。</p><pre><code>/**<br>	 * 首页<br>	 */<br>	public void index()  {<br>		//原模版路径<br>		String yourTplPath = \"index.html\";<br>		//静态html文件保存的路径<br>		String staticHtmlPath = \"/jfinal.com/index.html\";<br>		Template template = RenderManager.me().getEngine().getTemplate(yourTplPath);<br>		template.render(null, outPath);//直接输出，简洁<br>		render(yourTplPath);//是否继续输出到浏览器按自己的业务需求决定<br>	}</code></pre><p>以上只是示例，给有需要的朋友。</p><p>另外在静态化的过程中，有个小坑，需要注意下：</p><p>部分应用可能会存在部署目录，即常用的ctx,Jfinal中配置如下：</p><pre><code>public void configHandler(Handlers me) {<br>		me.add(new ContextPathHandler(\"base\"));<br>	}</code></pre><p>原模版中使用 #(base) 输出没问题，即为“”，但在生成静态化的文件时，直接输出了 null,兼容的写法为 #(base??\"\")<br></p><p>EOF</p>', '部分Web项目需要将页面静态化，JFinal增加几行代码即可搞定。例如：假定原有Controller Action中首页方法如下/**	 * 首页	 */	public void index()  {		//原模版路径		String yourTplPath = \"index.html\";		render(yourTplPath);	}静态化只需要增加几行代码即可，经过波总的指导，有更方便的写法。/**	 * 首页	 */	public void index()  {		//原模版路径		String yourTplPath = \"index.html\";		//静态html文件保存的路径		String staticHtmlPath = \"/jfinal.com/index.html\";		Template template = RenderManager.me().getEngine().getTemplate(yourTplPath);		template.render(null, outPath);//直接输出，简洁		render(yourTplPath);//是否继续输出到浏览器按自己的业务需求决定	}以上只是示例，给有需要的朋友。另外在静态化的过程中，有个小坑，需要注意下：部分应用可能会存在部署目录，即常用的ctx,Jfinal中配置如下：public void configHandler(Handlers me) {		me.add(new ContextPathHandler(\"base\"));	}原模版中使用 #(base) 输出没问题，即为“”，但在生成静态化的文件时，直接输出了 null,兼容的写法为 #(base??\"\")EOF', '2', '5', '0', '0', '1', '27', '2020-03-08 17:04:03', '27', '2020-09-20 18:29:35', '0', null, '0');
INSERT INTO `article` VALUES ('53', '图片展示', null, '8', '2', null, '下载图片展示', '1', '0', '0', '0', '0', '27', '2020-03-08 17:05:25', '27', '2020-03-08 17:40:08', '128', '129,144', '0');
INSERT INTO `article` VALUES ('54', '中央指导组：为打赢湖北保卫战、武汉保卫战贡献巾帼力量', null, '4', '1', '<p>新华社武汉3月8日电（记者 胡浩 赵文君）今天是“三八”国际劳动妇女节，中共中央政治局委员、国务院副总理孙春兰向奋战在疫情防控第一线的广大妇女同胞，转达习近平总书记和党中央、国务院的亲切关怀和节日祝福。</p><p>在视频连线女医务人员、公安干警、疾控人员、社区工作者、新闻工作者和志愿者等代表时，孙春兰详细询问她们的身体和工作生活情况，充分肯定了她们在疫情防控中的“半边天”作用。她指出，在疫情防控进入决胜决战的关键时期，习近平总书记专门向奋战在疫情防控第一线和各条战线的广大妇女同胞表示诚挚的慰问和节日的问候，既是肯定和褒扬，更是激励和鞭策。孙春兰说，职责面前无性别，广大妇女柔肩担重任，危难显芳华。要继续发扬不畏艰苦、连续作战的精神，坚定信念、慎终如始，为打赢湖北保卫战、武汉保卫战贡献巾帼力量。希望大家加强科学防护，注意保重身体，期待在战胜疫情、摘下口罩的那一天，绽放出更加美丽的笑容。</p><p>孙春兰强调，各级党委和政府要关心关爱疫情防控第一线的妇女同胞，各级妇联组织要解决她们在生活、家庭等方面的实际困难，舒缓心理压力，让广大妇女同胞切实感受到党和政府的关心和温暖。</p><p>湖北省妇联、武汉市妇联为在鄂在汉抗疫一线的妇女同胞赠送了慰问品。</p>', '新华社武汉3月8日电（记者 胡浩 赵文君）今天是“三八”国际劳动妇女节，中共中央政治局委员、国务院副总理孙春兰向奋战在疫情防控第一线的广大妇女同胞，转达习近平总书记和党中央、国务院的亲切关怀和节日祝福。在视频连线女医务人员、公安干警、疾控人员、社区工作者、新闻工作者和志愿者等代表时，孙春兰详细询问她们的身体和工作生活情况，充分肯定了她们在疫情防控中的“半边天”作用。她指出，在疫情防控进入决胜决战的关键时期，习近平总书记专门向奋战在疫情防控第一线和各条战线的广大妇女同胞表示诚挚的慰问和节日的问候，既是肯定和褒扬，更是激励和鞭策。孙春兰说，职责面前无性别，广大妇女柔肩担重任，危难显芳华。要继续发扬不畏艰苦、连续作战的精神，坚定信念、慎终如始，为打赢湖北保卫战、武汉保卫战贡献巾帼力量。希望大家加强科学防护，注意保重身体，期待在战胜疫情、摘下口罩的那一天，绽放出更加美丽的笑容。孙春兰强调，各级党委和政府要关心关爱疫情防控第一线的妇女同胞，各级妇联组织要解决她们在生活、家庭等方面的实际困难，舒缓心理压力，让广大妇女同胞切实感受到党和政府的关心和温暖。湖北省妇联、武汉市妇联为在鄂在汉抗疫一线的妇女同胞赠送了慰问品。', '1', '5', '0', '0', '1', '27', '2020-03-08 21:22:34', '27', '2020-03-08 21:26:38', '0', '', '0');
INSERT INTO `article` VALUES ('55', '漫画：大力支持', null, '31', '2', null, '3月6日，四川省地方金融监管局、商务厅以及人民银行成都分行、四川银保监局联合印发《关于加强金融支持我省餐饮企业疫情防控及复工复产工作的通知》，从加大融资支持力度、提升金融服务水平、降低企业融资成本、推进金融支持措施落地等四个方面提出9条措施，支持餐饮企业疫情防控和有序复工复产。', '1', '1', '0', '0', null, '27', '2020-03-08 21:25:09', null, '2020-03-08 21:25:09', '145', '146,147,148,149', '0');
INSERT INTO `article` VALUES ('56', '关于有偿提供拼图响应式后台的通知', null, '4', '1', '<p>拼图响应式前端框架版响应式后台正式发布。</p><p>考虑到目前拼图的状况，我们不打算免费开放下载，但也不会收各位朋友1分钱，该版后台将有偿提供给各位给予拼图贡献的朋友。</p><p>废话不多说，一切皆以有图有真相，下面上图，欢迎各种果断围观，各种喷。</p><p>1，首先我们先看下，功能机（ldpi 320x442）分辨率下的效果。</p><p>2，然后我们再通过拼图响应式测试工具直接查看不同设备下该款后台的样子。</p><p><a href=\"https://www.pintuer.com/documents/pintuer/1.x/tools//test.html\" target=\"_blank\">https://www.pintuer.com/documents/pintuer/1.x/tools//test.html</a></p><p>3，目前该款后台暂时只提供登录及初始页。详细访问地址：<a href=\"https://www.pintuer.com/themes/pintuer/1.x/04/\" target=\"_blank\">https://www.pintuer.com/themes/pintuer/1.x/04/</a></p><p>4，如果希望得到该款响应式后台的所有代码，可以通过以下途径获得。<br>a,将本文章分别复制转载到博客园、CSDN任意一种方式即可。<br>b,最好有自己使用拼图的感受，并在博文里向朋友推荐拼图响应式前端框架。<br>c,做完以上步骤后，将文章地址，电子邮箱留在<a href=\"https://www.pintuer.com/blog/?post=7\" target=\"_blank\">https://www.pintuer.com/blog/?post=7</a>评论即可。<br>d,我们验证完成后会将该款响应式后台源码发送到电子邮箱里。</p>', '拼图响应式前端框架版响应式后台正式发布。考虑到目前拼图的状况，我们不打算免费开放下载，但也不会收各位朋友1分钱，该版后台将有偿提供给各位给予拼图贡献的朋友。废话不多说，一切皆以有图有真相，下面上图，欢迎各种果断围观，各种喷。1，首先我们先看下，功能机（ldpi 320x442）分辨率下的效果。2，然后我们再通过拼图响应式测试工具直接查看不同设备下该款后台的样子。https://www.pintuer.com/documents/pintuer/1.x/tools//test.html3，目前该款后台暂时只提供登录及初始页。详细访问地址：https://www.pintuer.com/themes/pintuer/1.x/04/4，如果希望得到该款响应式后台的所有代码，可以通过以下途径获得。a,将本文章分别复制转载到博客园、CSDN任意一种方式即可。b,最好有自己使用拼图的感受，并在博文里向朋友推荐拼图响应式前端框架。c,做完以上步骤后，将文章地址，电子邮箱留在https://www.pintuer.com/blog/?post=7评论即可。d,我们验证完成后会将该款响应式后台源码发送到电子邮箱里。', '1', '3', '0', '0', '0', '27', '2020-03-15 11:23:44', null, '2020-03-15 11:23:44', '0', null, '0');
INSERT INTO `article` VALUES ('57', '图文测试', null, '32', '2', null, '图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试图文测试', '1', '14', '0', '0', null, '27', '2020-03-15 11:26:00', null, '2020-03-15 11:26:00', '150', '151,152,153,154,155,156,157', '0');
INSERT INTO `article` VALUES ('58', '最新版JFinal从入门到实战视频教程【60集】获得众多好评', '最新版JFinal从入门到实战视频教程【60集】获得众多好评', '5', '1', '<p>最新版JFinal从入门到实战视频教程【60集】自上线以来获得众多同学喜爱及好评，分享下几个同学的截图。</p><p>先放几个波总的点评</p><p><img src=\"\\202009\\2b745a1a47334758b12d2fb6cd7b4df2.png\" style=\"max-width:100%;\"><br></p><p><img src=\"\\202009\\6b4fdd29762848bfa360e2a5e3b90dff.png\" style=\"max-width:100%;\"><br></p><p><img src=\"\\202009\\92191e43d3874283977bb478f2220c1d.png\" style=\"max-width:100%;\"><br></p><p>再摘选几个其他同学的学习感受</p><p><img src=\"\\202009\\8c5668aa594f490a9cd15bb26a493ae7.png\" style=\"max-width:100%;\"><br></p><p><img src=\"\\202009\\8bee1e5e795144adbebe36f4da910b2a.png\" style=\"max-width:100%;\"><br></p><p><img src=\"\\202009\\f9c300086e9b448fa073990a66a708bf.png\" style=\"max-width:100%;\"><br></p><p><img src=\"\\202009\\c1b68ead988349e4a397a2f377dcf945.png\" style=\"max-width:100%;\"><br></p><p><img src=\"\\202009\\73a1dd408142439c98038c30d4bde14b.png\" style=\"max-width:100%;\"><br></p>', '最新版JFinal从入门到实战视频教程【60集】自上线以来获得众多同学喜爱及好评，分享下几个同学的截图。先放几个波总的点评再摘选几个其他同学的学习感受', '1', '3', '0', '0', '1', '27', '2020-09-20 18:28:25', null, '2020-09-20 18:28:25', '0', null, '0');
INSERT INTO `article` VALUES ('59', 'sitesCMS官网', null, '28', '1', '<p>sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。</p><p><br></p><p>以下都是重复内容，目的是想让文字多一点。</p><p>sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。 </p><p>sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。</p><p>sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。</p><p>sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。</p><p>sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。</p><p>sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。</p><p>sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。</p><p>sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。</p><p>sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。 <br></p>', 'sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。以下都是重复内容，目的是想让文字多一点。sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。 sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。sitesCMS官网（演示版）是使用拼图pintuer1.X的模板改造的，主要目的是演示sitesCMS的功能，一个是演示可以适用各种样式，二是演示自定义指令获取数据，三还可以感受sitesCMS整体结构。 ', '1', '2', '0', '0', '1', '27', '2020-09-20 20:27:13', '27', '2020-09-21 20:47:20', '168', '', '0');
INSERT INTO `article` VALUES ('60', '新闻测试', null, '4', '1', '<p>新闻测试新闻测试新闻测试</p><p>新闻测试</p><p>新闻测试</p><p>新闻测试</p><p>新闻测试新闻测试</p><p>新闻测试</p>', '新闻测试新闻测试新闻测试新闻测试新闻测试新闻测试新闻测试新闻测试新闻测试', '1', '0', '0', '0', '0', '27', '2020-09-20 20:45:57', null, '2020-09-20 20:45:57', '0', null, '0');
INSERT INTO `article` VALUES ('61', '新闻测试', null, '4', '1', '<p>新闻测试</p><p>新闻测试</p><p>新闻测试</p><p>新闻测试</p><p>新闻测试</p>', '新闻测试新闻测试新闻测试新闻测试新闻测试', '1', '0', '0', '0', '0', '27', '2020-09-20 20:46:12', null, '2020-09-20 20:46:12', '0', null, '0');
INSERT INTO `article` VALUES ('62', '新闻测试', null, '4', '1', '<p>新闻测试</p>', '新闻测试', '1', '0', '0', '0', '0', '27', '2020-09-20 20:46:19', null, '2020-09-20 20:46:19', '0', null, '0');
INSERT INTO `article` VALUES ('63', '新闻测试', null, '4', '1', '<p>新闻测试</p>', '新闻测试', '1', '0', '0', '0', '0', '27', '2020-09-20 20:46:25', null, '2020-09-20 20:46:25', '0', null, '0');
INSERT INTO `article` VALUES ('64', '新闻测试', null, '4', '1', '<p>新闻测试</p>', '新闻测试', '1', '0', '0', '0', '0', '27', '2020-09-20 20:46:32', null, '2020-09-20 20:46:32', '0', null, '0');
INSERT INTO `article` VALUES ('65', '新闻测试', null, '4', '1', '<p>新闻测试</p>', '新闻测试', '1', '0', '0', '0', '0', '27', '2020-09-20 20:46:39', null, '2020-09-20 20:46:39', '0', null, '0');
INSERT INTO `article` VALUES ('66', '新闻测试', null, '4', '1', '<p>新闻测试</p>', '新闻测试', '1', '0', '0', '0', '0', '27', '2020-09-20 20:46:45', null, '2020-09-20 20:46:45', '0', null, '0');
INSERT INTO `article` VALUES ('67', '新闻测试', null, '4', '1', '<p>新闻测试</p>', '新闻测试', '1', '0', '0', '0', '0', '27', '2020-09-20 20:46:51', null, '2020-09-20 20:46:51', '0', null, '0');
INSERT INTO `article` VALUES ('68', '新闻测试', null, '4', '1', '<p>新闻测试</p>', '新闻测试', '1', '0', '0', '0', '0', '27', '2020-09-20 20:46:57', null, '2020-09-20 20:46:57', '0', null, '0');
INSERT INTO `article` VALUES ('69', '公司新闻测试', null, '5', '1', '<p>公司新闻测试</p><p>公司新闻测试</p><p>公司新闻测试</p><p>公司新闻测试</p><p>公司新闻测试公司新闻测试</p><p>公司新闻测试</p><p>公司新闻测试</p>', '公司新闻测试公司新闻测试公司新闻测试公司新闻测试公司新闻测试公司新闻测试公司新闻测试公司新闻测试', '1', '0', '0', '0', '0', '27', '2020-09-21 19:53:42', null, '2020-09-21 19:53:42', '0', null, '0');
INSERT INTO `article` VALUES ('70', '公司新闻测试', '公司新闻测试', '5', '1', '<p>公司新闻测试</p>', '公司新闻测试', '1', '0', '0', '0', '0', '27', '2020-09-21 19:53:56', null, '2020-09-21 19:53:56', '0', null, '0');
INSERT INTO `article` VALUES ('71', '公司新闻测试', null, '5', '1', '<p>公司新闻测试</p>', '公司新闻测试', '1', '0', '0', '0', '0', '27', '2020-09-21 19:54:03', null, '2020-09-21 19:54:03', '0', null, '0');
INSERT INTO `article` VALUES ('72', '公司新闻测试', null, '5', '1', '<p>公司新闻测试</p>', '公司新闻测试', '1', '0', '0', '0', '0', '27', '2020-09-21 19:54:09', null, '2020-09-21 19:54:09', '0', null, '0');
INSERT INTO `article` VALUES ('73', '公司新闻测试', null, '5', '1', '<p>公司新闻测试</p>', '公司新闻测试', '1', '0', '0', '0', '0', '27', '2020-09-21 19:54:16', null, '2020-09-21 19:54:16', '0', null, '0');
INSERT INTO `article` VALUES ('74', '环保科技公司-仿', null, '28', '1', '<p>环保科技公司-仿</p><p>这是一个纯仿站点，使用的是一个现成的模板直接复制的</p><p>将模板上的标签改成sitesCMS的自定义指令就能直接用了</p><p>这个仅仅是仿制用于演示，所以功能并不完整</p>', '环保科技公司-仿这是一个纯仿站点，使用的是一个现成的模板直接复制的将模板上的标签改成sitesCMS的自定义指令就能直接用了这个仅仅是仿制用于演示，所以功能并不完整', '1', '0', '0', '0', '0', '27', '2020-09-21 20:42:56', null, '2020-09-21 20:42:56', '169', null, '0');
INSERT INTO `article` VALUES ('75', '环保科技公司-仿', null, '28', '1', '<p>环保科技公司-仿</p><p>这是一个纯仿站点，使用的是一个现成的模板直接复制的</p><p>将模板上的标签改成sitesCMS的自定义指令就能直接用了</p><p>这个仅仅是仿制用于演示，所以功能并不完整</p>', '环保科技公司-仿这是一个纯仿站点，使用的是一个现成的模板直接复制的将模板上的标签改成sitesCMS的自定义指令就能直接用了这个仅仅是仿制用于演示，所以功能并不完整', '2', '0', '0', '0', '0', '27', '2020-09-21 20:46:42', '27', '2020-09-21 20:47:04', '169', null, '0');
INSERT INTO `article` VALUES ('76', '响应式', '从小屏逐步扩展到大屏，最终实现所有屏幕适配，最终实现所有屏幕适配，适应移动互联潮流。', '33', '1', '<p>从小屏逐步扩展到大屏，最终实现所有屏幕适配，最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。  <br></p>', '从小屏逐步扩展到大屏，最终实现所有屏幕适配，最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。  ', '1', '0', '0', '0', '0', '37', '2020-09-22 20:20:48', '37', '2020-09-22 20:33:48', '175', '174', '14');
INSERT INTO `article` VALUES ('77', '兼容性', '从小屏逐步扩展到大屏，最终实现所有屏幕适配，最终实现所有屏幕适配，适应移动互联潮流。', '33', '1', '<p>从小屏逐步扩展到大屏，最终实现所有屏幕适配，最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。  <br></p>', '从小屏逐步扩展到大屏，最终实现所有屏幕适配，最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。  ', '1', '0', '0', '0', '0', '37', '2020-09-22 20:21:25', '37', '2020-09-22 20:33:56', '176', '177', '14');
INSERT INTO `article` VALUES ('78', 'CSS处理', '从小屏逐步扩展到大屏，最终实现所有屏幕适配，最终实现所有屏幕适配，适应移动互联潮流。', '33', '1', '<p>从小屏逐步扩展到大屏，最终实现所有屏幕适配，最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。  <br></p>', '从小屏逐步扩展到大屏，最终实现所有屏幕适配，最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。  ', '1', '0', '0', '0', '0', '37', '2020-09-22 20:22:01', '37', '2020-09-22 20:34:08', '178', '179', '14');
INSERT INTO `article` VALUES ('79', 'JS基础库', '从小屏逐步扩展到大屏，最终实现所有屏幕适配，最终实现所有屏幕适配，适应移动互联潮流。', '33', '1', '<p>从小屏逐步扩展到大屏，最终实现所有屏幕适配，最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。  <br></p>', '从小屏逐步扩展到大屏，最终实现所有屏幕适配，最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。最终实现所有屏幕适配，适应移动互联潮流。  ', '1', '0', '0', '0', '0', '37', '2020-09-22 20:22:30', '37', '2020-09-22 20:34:16', '180', '181', '14');
INSERT INTO `article` VALUES ('80', '一直在你身边对你好，你却没有发现。', '找老婆要找爱发脾气的女人。永远不会发脾气的女人就如同一杯白开水，解渴，却无味。而发脾气的女人正如烈酒般，刺激而令人无法忘怀。', '34', '1', '<p>找老婆要找爱发脾气的女人。永远不会发脾气的女人就如同一杯白开水，解渴，却无味。而发脾气的女人正如烈酒般，刺激而令人无法忘怀。&nbsp;&nbsp;<br></p>', '找老婆要找爱发脾气的女人。永远不会发脾气的女人就如同一杯白开水，解渴，却无味。而发脾气的女人正如烈酒般，刺激而令人无法忘怀。&nbsp;&nbsp;', '1', '2', '0', '0', '0', '37', '2020-09-23 23:05:26', null, '2020-09-23 23:05:26', '182', null, '14');
INSERT INTO `article` VALUES ('81', '写经验交流材料的技巧全在这了！', '看不到您的原稿，这样对空发议论，估计对您的指导性是不大的。建议您将原稿贴出来，好让老师们针对指导。这里简单给出意见', '34', '1', '<p>看不到您的原稿，这样对空发议论，估计对您的指导性是不大的。建议您将原稿贴出来，好让老师们针对指导。这里简单给出意见&nbsp;&nbsp;<br></p>', '看不到您的原稿，这样对空发议论，估计对您的指导性是不大的。建议您将原稿贴出来，好让老师们针对指导。这里简单给出意见&nbsp;&nbsp;', '1', '9', '0', '0', '0', '37', '2020-09-23 23:35:21', null, '2020-09-23 23:35:21', '183', null, '14');
INSERT INTO `article` VALUES ('82', '名牌工厂店', '一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。', '35', '1', '<p>一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。&nbsp;&nbsp;<br></p>', '一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。&nbsp;&nbsp;', '1', '0', '0', '0', '0', '37', '2020-09-24 19:27:04', null, '2020-09-24 19:27:04', '184', null, '14');
INSERT INTO `article` VALUES ('83', '名牌工厂店', '一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。', '35', '1', '<p>一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。&nbsp;&nbsp;<br></p>', '一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。&nbsp;&nbsp;', '1', '0', '0', '0', '0', '37', '2020-09-24 19:27:32', null, '2020-09-24 19:27:32', '185', null, '14');
INSERT INTO `article` VALUES ('84', '名牌工厂店', '一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。', '35', '1', '<p>一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。&nbsp;&nbsp;<br></p>', '一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。&nbsp;&nbsp;', '1', '0', '0', '0', '0', '37', '2020-09-24 19:27:58', null, '2020-09-24 19:27:58', '186', null, '14');
INSERT INTO `article` VALUES ('85', '名牌工厂店', '一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。', '35', '1', '<p>一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。&nbsp;&nbsp;<br></p>', '一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。&nbsp;&nbsp;', '1', '0', '0', '0', '0', '37', '2020-09-24 19:34:46', null, '2020-09-24 19:34:46', '187', null, '14');
INSERT INTO `article` VALUES ('86', '名牌工厂店', '一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。', '35', '1', '<p>一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。&nbsp;&nbsp;<br></p>', '一家工厂企业的商品展示网站，主要以卖高端服饰为主。主要以卖高端服饰为主。主要以卖高端服饰为主。&nbsp;&nbsp;', '1', '0', '0', '0', '0', '37', '2020-09-24 19:35:39', null, '2020-09-24 19:35:39', '188', null, '14');
INSERT INTO `article` VALUES ('87', '前端开发工程师', null, '38', '1', '<p>&gt; 职位描述</p><ol><li>前端开发及维护工作；</li><li>有良好的技术基础，熟悉 Web 标准，熟练掌握多种 Web 前端技术；</li><li>掌握行业内流行的类库，Vue.js， React 等主流框架；</li><li>参与公司前端工程的设计、研发；</li><li>了解不同浏览器之间的差异，移动设备之间的差异。</li></ol>', '&gt; 职位描述前端开发及维护工作；有良好的技术基础，熟悉 Web 标准，熟练掌握多种 Web 前端技术；掌握行业内流行的类库，Vue.js， React 等主流框架；参与公司前端工程的设计、研发；了解不同浏览器之间的差异，移动设备之间的差异。', '1', '0', '0', '0', '0', '37', '2020-09-24 19:40:57', null, '2020-09-24 19:40:57', '0', null, '14');
INSERT INTO `article` VALUES ('88', 'IOS 开发工程师', null, '38', '1', '<p>&gt; 职位描述</p><ol><li>前端开发及维护工作；</li><li>有良好的技术基础，熟悉 Web 标准，熟练掌握多种 Web 前端技术；</li><li>掌握行业内流行的类库，Vue.js， React 等主流框架；</li><li>参与公司前端工程的设计、研发；</li><li>了解不同浏览器之间的差异，移动设备之间的差异。</li></ol>', '&gt; 职位描述前端开发及维护工作；有良好的技术基础，熟悉 Web 标准，熟练掌握多种 Web 前端技术；掌握行业内流行的类库，Vue.js， React 等主流框架；参与公司前端工程的设计、研发；了解不同浏览器之间的差异，移动设备之间的差异。', '1', '0', '0', '0', '0', '37', '2020-09-24 19:41:21', null, '2020-09-24 19:41:21', '0', null, '14');
INSERT INTO `article` VALUES ('89', 'JAVA 工程师', null, '38', '1', '<p>&gt; 职位描述</p><ol><li>前端开发及维护工作；</li><li>有良好的技术基础，熟悉 Web 标准，熟练掌握多种 Web 前端技术；</li><li>掌握行业内流行的类库，Vue.js， React 等主流框架；</li><li>参与公司前端工程的设计、研发；</li><li>了解不同浏览器之间的差异，移动设备之间的差异。</li></ol>', '&gt; 职位描述前端开发及维护工作；有良好的技术基础，熟悉 Web 标准，熟练掌握多种 Web 前端技术；掌握行业内流行的类库，Vue.js， React 等主流框架；参与公司前端工程的设计、研发；了解不同浏览器之间的差异，移动设备之间的差异。', '1', '0', '0', '0', '0', '37', '2020-09-24 19:41:44', null, '2020-09-24 19:41:44', '0', null, '14');

-- ----------------------------
-- Table structure for column
-- ----------------------------
DROP TABLE IF EXISTS `column`;
CREATE TABLE `column` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `enName` varchar(50) default NULL COMMENT '栏目英文标识，站点内唯一',
  `parent` int(11) default NULL,
  `children` varchar(512) default NULL COMMENT '子栏目id拼接的字符串，以|拼接',
  `status` varchar(1) default NULL COMMENT '状态',
  `createTime` datetime default NULL,
  `updateTime` datetime default NULL,
  `siteId` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of column
-- ----------------------------
INSERT INTO `column` VALUES ('1', '新闻动态', 'news', '0', '4|6|5|32', '1', '2020-02-03 17:09:25', null, '0');
INSERT INTO `column` VALUES ('2', '关于我们', 'aboutUs', '0', '3|27', '1', '2020-02-03 17:24:23', null, '0');
INSERT INTO `column` VALUES ('3', '公司简介', 'company', '2', null, '1', '2020-02-03 17:25:33', null, '0');
INSERT INTO `column` VALUES ('4', '行业新闻', 'IndustryNews', '1', null, '1', '2020-02-03 17:29:24', null, '0');
INSERT INTO `column` VALUES ('5', '公司新闻', 'companyNews', '1', null, '1', '2020-02-03 17:29:51', '2020-02-03 17:36:16', '0');
INSERT INTO `column` VALUES ('6', '通知公告', 'notice', '1', null, '1', '2020-02-03 17:34:53', null, '0');
INSERT INTO `column` VALUES ('7', '娱乐新闻', 'Entertainment', '0', '8|9', '1', '2020-02-03 17:39:24', '2020-02-03 17:48:51', '0');
INSERT INTO `column` VALUES ('8', '电影', 'movie', '7', null, '1', '2020-02-03 17:39:57', null, '0');
INSERT INTO `column` VALUES ('9', '电视', 'tv', '7', null, '1', '2020-02-03 17:40:10', null, '0');
INSERT INTO `column` VALUES ('10', '政策法规', 'laws', '0', null, '2', '2020-02-04 18:16:20', '2020-03-08 10:31:19', '0');
INSERT INTO `column` VALUES ('11', '人力资源部', 'test2', '12', null, '1', '2020-02-05 15:59:06', '2020-02-07 18:22:21', '0');
INSERT INTO `column` VALUES ('12', '部门园地', 'department', '0', '13|11|29|30', '2', '2020-02-07 18:21:21', '2020-03-08 11:51:07', '0');
INSERT INTO `column` VALUES ('13', '信息管理部', 'IT', '12', null, '1', '2020-02-07 18:21:48', null, '0');
INSERT INTO `column` VALUES ('14', '关于我们', 'aboutUs', '0', '20|21|22', '1', '2020-02-09 17:13:37', null, '12');
INSERT INTO `column` VALUES ('15', '产品展示', 'productShow', '0', '23|24', '1', '2020-02-09 17:13:56', null, '12');
INSERT INTO `column` VALUES ('16', '新闻动态', 'news', '0', null, '1', '2020-02-09 17:14:21', null, '12');
INSERT INTO `column` VALUES ('17', '客户中心', 'customerCenter', '0', null, '1', '2020-02-09 17:15:02', null, '12');
INSERT INTO `column` VALUES ('18', '厂房设备', 'device', '0', null, '1', '2020-02-09 17:15:48', null, '12');
INSERT INTO `column` VALUES ('19', '联系我们', 'contactUs', '0', null, '1', '2020-02-09 17:16:25', null, '12');
INSERT INTO `column` VALUES ('20', '公司概况', 'gsgk', '14', null, '1', '2020-02-09 18:00:13', null, '12');
INSERT INTO `column` VALUES ('21', '企业文化', 'qywh', '14', null, '1', '2020-02-09 18:00:25', null, '12');
INSERT INTO `column` VALUES ('22', '联系我们', 'lxwm', '14', null, '1', '2020-02-09 18:00:51', null, '12');
INSERT INTO `column` VALUES ('23', '除尘系列', 'ccxl', '15', null, '1', '2020-02-09 18:01:13', null, '12');
INSERT INTO `column` VALUES ('24', '过滤系列', 'glxl', '15', null, '1', '2020-02-09 18:01:37', null, '12');
INSERT INTO `column` VALUES ('25', '产品中心', 'product', '0', null, '2', '2020-02-16 16:06:55', '2020-03-07 19:16:49', '0');
INSERT INTO `column` VALUES ('26', '人才招聘', 'jobs', '0', null, '1', '2020-02-16 16:08:14', null, '0');
INSERT INTO `column` VALUES ('27', '联系我们', 'contactUs', '2', null, '1', '2020-02-16 16:08:51', null, '0');
INSERT INTO `column` VALUES ('28', '案例', 'case', '0', null, '1', '2020-02-16 21:09:03', '2020-03-08 10:29:51', '0');
INSERT INTO `column` VALUES ('29', '财务部', 'finance', '12', null, '2', '2020-03-08 10:22:18', '2020-03-08 11:35:21', '0');
INSERT INTO `column` VALUES ('30', '综保部', 'ensure', '12', null, '2', '2020-03-08 10:25:28', '2020-03-08 10:34:06', '0');
INSERT INTO `column` VALUES ('31', '图册', 'pic', '0', null, '1', '2020-03-08 21:24:06', '2020-03-10 21:49:46', '0');
INSERT INTO `column` VALUES ('32', '图说新闻', 'picNews', '1', null, '1', '2020-03-15 11:22:13', null, '0');
INSERT INTO `column` VALUES ('33', '产品', 'product', '0', null, '1', '2020-09-22 20:13:04', null, '14');
INSERT INTO `column` VALUES ('34', '动态', 'news', '0', null, '1', '2020-09-22 20:13:17', null, '14');
INSERT INTO `column` VALUES ('35', '案例', 'case', '0', null, '1', '2020-09-22 20:13:29', null, '14');
INSERT INTO `column` VALUES ('36', '关于', 'about', '0', '37|38|39', '1', '2020-09-22 20:14:38', null, '14');
INSERT INTO `column` VALUES ('37', '公司简介', 'company', '36', null, '1', '2020-09-22 20:15:23', null, '14');
INSERT INTO `column` VALUES ('38', '招贤纳士', 'jobs', '36', null, '1', '2020-09-22 20:16:27', null, '14');
INSERT INTO `column` VALUES ('39', '发展历程', 'history', '36', null, '1', '2020-09-22 20:16:56', null, '14');

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `parent` int(11) default NULL,
  `children` varchar(512) default NULL COMMENT '子部门id拼接的字符串，以|拼接',
  `createTime` datetime default NULL,
  `updateTime` datetime default NULL,
  `siteId` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------

-- ----------------------------
-- Table structure for files
-- ----------------------------
DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(50) default NULL COMMENT '文件类型：annex附件，thumbnail缩略图，Album相册',
  `name` varchar(128) default NULL COMMENT '文件名',
  `originalName` varchar(128) default NULL COMMENT '原始文件名',
  `path` varchar(256) default NULL COMMENT '文件保存路径',
  `contentType` varchar(256) default NULL,
  `createTime` datetime default NULL COMMENT '创建时间',
  `remark` varchar(512) default NULL COMMENT '文件说明',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of files
-- ----------------------------
INSERT INTO `files` VALUES ('1', 'annex', '几个文件2.docx', '几个文件.docx', 'G:\\workspaces\\workspace4jee\\sitesCMS\\src\\main\\webapp\\upload\\几个文件2.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 21:34:59', null);
INSERT INTO `files` VALUES ('2', 'annex', '就业网培训1.docx', '就业网培训.docx', 'G:\\workspaces\\workspace4jee\\sitesCMS\\src\\main\\webapp\\upload\\就业网培训1.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 21:36:28', null);
INSERT INTO `files` VALUES ('3', 'annex', '网站环境搭建过程_简版.docx', '网站环境搭建过程_简版.docx', 'G:\\workspaces\\workspace4jee\\sitesCMS\\src\\main\\webapp\\upload\\网站环境搭建过程_简版.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 21:38:22', null);
INSERT INTO `files` VALUES ('4', 'annex', '网站环境搭建过程_简版1.docx', '网站环境搭建过程_简版.docx', 'G:\\workspaces\\workspace4jee\\sitesCMS\\src\\main\\webapp\\upload\\网站环境搭建过程_简版1.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 21:44:58', null);
INSERT INTO `files` VALUES ('5', 'annex', '就业网培训2.docx', '就业网培训.docx', 'G:\\workspaces\\workspace4jee\\sitesCMS\\src\\main\\webapp\\upload\\就业网培训2.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 21:48:58', null);
INSERT INTO `files` VALUES ('6', 'annex', '几个文件3.docx', '几个文件.docx', 'G:\\workspaces\\workspace4jee\\sitesCMS\\src\\main\\webapp\\upload\\几个文件3.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 21:51:35', null);
INSERT INTO `files` VALUES ('7', 'annex', '网站环境搭建过程_简版2.docx', '网站环境搭建过程_简版.docx', 'G:\\workspaces\\workspace4jee\\sitesCMS\\src\\main\\webapp\\upload\\网站环境搭建过程_简版2.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 21:53:18', null);
INSERT INTO `files` VALUES ('8', 'annex', '网站环境搭建过程_简版3.docx', '网站环境搭建过程_简版.docx', 'G:\\workspaces\\workspace4jee\\sitesCMS\\src\\main\\webapp\\upload\\网站环境搭建过程_简版3.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 22:01:46', null);
INSERT INTO `files` VALUES ('9', 'annex', '几个文件4.docx', '几个文件.docx', 'G:\\workspaces\\workspace4jee\\sitesCMS\\src\\main\\webapp\\upload\\几个文件4.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 22:15:22', null);
INSERT INTO `files` VALUES ('10', 'annex', '网站环境搭建过程_简版4.docx', '网站环境搭建过程_简版.docx', 'G:\\workspaces\\workspace4jee\\sitesCMS\\src\\main\\webapp\\upload\\网站环境搭建过程_简版4.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 22:16:07', null);
INSERT INTO `files` VALUES ('11', 'annex', '网站环境搭建过程_简版4.docx', '网站环境搭建过程_简版.docx', 'G:\\workspaces\\workspace4jee\\sitesCMS\\src\\main\\webapp\\upload\\网站环境搭建过程_简版4.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 22:19:55', null);
INSERT INTO `files` VALUES ('12', 'annex', '网站环境搭建过程_简版4.docx', '网站环境搭建过程_简版.docx', 'G:\\workspaces\\workspace4jee\\sitesCMS\\src\\main\\webapp\\upload\\网站环境搭建过程_简版4.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 22:20:34', null);
INSERT INTO `files` VALUES ('13', 'annex', '网站环境搭建过程_简版4.docx', '网站环境搭建过程_简版.docx', 'G:\\workspaces\\workspace4jee\\sitesCMS\\src\\main\\webapp\\upload\\网站环境搭建过程_简版4.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 22:22:12', null);
INSERT INTO `files` VALUES ('14', 'annex', '网站环境搭建过程_简版5.docx', '网站环境搭建过程_简版.docx', 'G:\\workspaces\\workspace4jee\\sitesCMS\\src\\main\\webapp\\upload\\网站环境搭建过程_简版5.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 22:23:51', null);
INSERT INTO `files` VALUES ('15', 'annex', '几个文件.docx', '几个文件.docx', 'G:\\upload\\202002\\几个文件.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 22:26:53', null);
INSERT INTO `files` VALUES ('16', 'annex', '就业网培训.docx', '就业网培训.docx', 'G:\\upload\\202002\\就业网培训.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 22:26:57', null);
INSERT INTO `files` VALUES ('17', 'annex', '几个文件.docx', '几个文件.docx', 'G:\\upload\\202002\\几个文件.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 22:30:35', null);
INSERT INTO `files` VALUES ('18', 'annex', '就业网培训.docx', '就业网培训.docx', 'G:\\upload\\202002\\就业网培训.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-05 22:30:39', null);
INSERT INTO `files` VALUES ('19', 'annex', '032be30e288045dc9425d70cf95d26ec', '网站环境搭建过程_简版.docx', 'G:\\upload\\202002\\032be30e288045dc9425d70cf95d26ec', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 15:39:36', null);
INSERT INTO `files` VALUES ('20', 'annex', '4f00606e983c4343bbd24714e31372cfdocx', '网站环境搭建过程_简版.docx', 'G:\\upload\\202002\\4f00606e983c4343bbd24714e31372cfdocx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 15:42:36', null);
INSERT INTO `files` VALUES ('21', 'annex', '659fa66835c6428697a4c6aee3ad9c96.docx', '网站环境搭建过程_简版.docx', 'G:\\upload\\202002\\659fa66835c6428697a4c6aee3ad9c96.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 15:42:59', null);
INSERT INTO `files` VALUES ('22', 'annex', 'b0057a882c49466eb7af41737c348e7d.docx', '网站环境搭建过程_简版.docx', 'G:\\upload\\202002\\b0057a882c49466eb7af41737c348e7d.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:05:44', null);
INSERT INTO `files` VALUES ('23', 'annex', 'b3ce80d0fbef42009ad3917bafaf6303.docx', '网站环境搭建过程_简版.docx', 'G:\\upload\\202002\\b3ce80d0fbef42009ad3917bafaf6303.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:08:08', null);
INSERT INTO `files` VALUES ('24', 'annex', 'd5bbf8c0df684eb2b18e54981e720a0c.docx', '就业网培训.docx', 'G:\\upload\\202002\\d5bbf8c0df684eb2b18e54981e720a0c.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:11:57', null);
INSERT INTO `files` VALUES ('25', 'annex', '1516e36e704f4c5bb228e39b059bf279.docx', '网站环境搭建过程_简版.docx', 'G:\\upload\\202002\\1516e36e704f4c5bb228e39b059bf279.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:12:09', null);
INSERT INTO `files` VALUES ('26', 'annex', 'ea9bbfefc9ff4933a521f1efb3c722f4.docx', '网站环境搭建过程_简版.docx', 'G:\\upload\\202002\\ea9bbfefc9ff4933a521f1efb3c722f4.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:17:04', null);
INSERT INTO `files` VALUES ('27', 'annex', 'e89856c1aebe4535a19f2e85cd9973aa.docx', '就业网培训.docx', 'G:\\upload\\202002\\e89856c1aebe4535a19f2e85cd9973aa.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:17:10', null);
INSERT INTO `files` VALUES ('28', 'annex', '9ef3167df7574b9cb0cb947784be6022.docx', '几个文件.docx', 'G:\\upload\\202002\\9ef3167df7574b9cb0cb947784be6022.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:17:15', null);
INSERT INTO `files` VALUES ('29', 'annex', '5ab0601c6f4041f5974090e090355c92.docx', '网站环境搭建过程_简版.docx', 'G:\\upload\\202002\\5ab0601c6f4041f5974090e090355c92.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:17:27', null);
INSERT INTO `files` VALUES ('30', 'annex', 'f9c55111996a423bacfdc3059e111d61.docx', '网站环境搭建过程_简版.docx', 'G:\\upload\\202002\\f9c55111996a423bacfdc3059e111d61.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:24:26', null);
INSERT INTO `files` VALUES ('31', 'annex', '8ba5872d7f854975bbed0b50a77357de.docx', '就业网培训.docx', 'G:\\upload\\202002\\8ba5872d7f854975bbed0b50a77357de.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:24:30', null);
INSERT INTO `files` VALUES ('32', 'annex', 'a7c83d57c6614c86bc4f7d210e8d84bf.docx', '网站环境搭建过程_简版.docx', 'G:\\upload\\202002\\a7c83d57c6614c86bc4f7d210e8d84bf.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:28:45', null);
INSERT INTO `files` VALUES ('33', 'annex', 'e4488635f62a43aa91c2569c09026e04.docx', '就业网培训.docx', 'G:\\upload\\202002\\e4488635f62a43aa91c2569c09026e04.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:28:49', null);
INSERT INTO `files` VALUES ('34', 'annex', 'a1c87618eb984d98bbcab267af9d2e8c.docx', '几个文件.docx', 'G:\\upload\\202002\\a1c87618eb984d98bbcab267af9d2e8c.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:28:54', null);
INSERT INTO `files` VALUES ('35', 'annex', '64f421a4c8064147a38de72c60a7865c.docx', '网站环境搭建过程_简版.docx', 'G:\\upload\\202002\\64f421a4c8064147a38de72c60a7865c.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:33:46', null);
INSERT INTO `files` VALUES ('36', 'annex', '668a24b0299e4c8d8b825e631d6cde63.docx', '几个文件.docx', 'G:\\upload\\202002\\668a24b0299e4c8d8b825e631d6cde63.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:34:24', null);
INSERT INTO `files` VALUES ('37', 'annex', 'b58d30a63b714901962538e13c81444f.docx', '就业网培训.docx', 'G:\\upload\\202002\\b58d30a63b714901962538e13c81444f.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:34:28', null);
INSERT INTO `files` VALUES ('38', 'annex', '616e6eb40e5643988b82b423d7beabe4.docx', '网站环境搭建过程_简版.docx', 'G:\\upload\\202002\\616e6eb40e5643988b82b423d7beabe4.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 16:34:31', null);
INSERT INTO `files` VALUES ('39', 'annex', 'dd0ff843eb444219843ec80d391f16eb.docx', '网站环境搭建过程_简版.docx', 'G:\\upload\\202002\\dd0ff843eb444219843ec80d391f16eb.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-06 17:14:03', null);
INSERT INTO `files` VALUES ('40', 'annex', '9f752c5ae1dd46e59f6043ca600a01cd.docx', '网站环境搭建过程_简版.docx', 'G:\\upload\\202002\\9f752c5ae1dd46e59f6043ca600a01cd.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '2020-02-07 11:23:48', null);
INSERT INTO `files` VALUES ('41', 'thumbnail', '976eb07565624cb2b97262c9ce26f98e.png', '人生轨迹线.png', 'G:\\upload\\202002\\976eb07565624cb2b97262c9ce26f98e.png', 'image/png', '2020-02-07 11:51:28', null);
INSERT INTO `files` VALUES ('42', 'thumbnail', '7ab46934aacd491da625c429999f854c.png', '人生轨迹线.png', 'G:\\upload\\202002\\7ab46934aacd491da625c429999f854c.png', 'image/png', '2020-02-07 11:52:05', null);
INSERT INTO `files` VALUES ('43', 'thumbnail', '505a4a4413b54355931e8d6269d0fa20.png', '人生轨迹线.png', 'G:\\upload\\202002\\505a4a4413b54355931e8d6269d0fa20.png', 'image/png', '2020-02-07 11:53:44', null);
INSERT INTO `files` VALUES ('44', 'thumbnail', '22c056da7f6a4f9bb436f22d37c1da57.png', '数据库.png', 'G:\\upload\\202002\\22c056da7f6a4f9bb436f22d37c1da57.png', 'image/png', '2020-02-07 11:55:29', null);
INSERT INTO `files` VALUES ('45', 'thumbnail', '5a3213c7275447639b515e6e2db4d773.png', '人生轨迹线.png', 'G:\\upload\\202002\\5a3213c7275447639b515e6e2db4d773.png', 'image/png', '2020-02-07 11:56:10', null);
INSERT INTO `files` VALUES ('46', 'annex', 'a994dad2afa94d53a35c000bd531b062.pptx', '带日期时间的甘特图PPT模板.pptx', 'G:\\upload\\202002\\a994dad2afa94d53a35c000bd531b062.pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', '2020-02-07 11:56:17', null);
INSERT INTO `files` VALUES ('47', 'annex', '943c6e33940f4699b7ae5e94ea3e3743.rar', '1-160223105223.rar', 'G:\\upload\\202002\\943c6e33940f4699b7ae5e94ea3e3743.rar', 'application/octet-stream', '2020-02-07 11:56:25', null);
INSERT INTO `files` VALUES ('48', 'thumbnail', '11bdf2f6ac2847b3a81840d05f3d3be5.png', '人生轨迹线.png', 'G:\\upload\\202002\\11bdf2f6ac2847b3a81840d05f3d3be5.png', 'image/png', '2020-02-07 12:00:31', null);
INSERT INTO `files` VALUES ('49', 'thumbnail', '3f30238942b842f7bb946f4cecbc4c92.png', '人生轨迹线.png', 'G:\\upload\\202002\\3f30238942b842f7bb946f4cecbc4c92.png', 'image/png', '2020-02-07 12:00:44', null);
INSERT INTO `files` VALUES ('50', 'thumbnail', 'bb70ebaf4d4e403fa9dbcc05f03bf91f.png', '人生轨迹线.png', 'G:\\upload\\202002\\bb70ebaf4d4e403fa9dbcc05f03bf91f.png', 'image/png', '2020-02-07 12:01:47', null);
INSERT INTO `files` VALUES ('51', 'thumbnail', '281e1d471cac4f41abb50007e42150df.png', '人生轨迹线.png', 'G:\\upload\\202002\\281e1d471cac4f41abb50007e42150df.png', 'image/png', '2020-02-07 12:26:13', null);
INSERT INTO `files` VALUES ('52', 'thumbnail', '811af3f2fb264ca5af6e125adfdd6c87.png', '人生轨迹线.png', 'G:\\upload\\202002\\811af3f2fb264ca5af6e125adfdd6c87.png', 'image/png', '2020-02-07 12:26:56', null);
INSERT INTO `files` VALUES ('53', 'thumbnail', 'e34bcf19d4984b1bbefaf8c25cd5fac8.png', '人生轨迹线.png', 'G:\\upload\\202002\\e34bcf19d4984b1bbefaf8c25cd5fac8.png', 'image/png', '2020-02-07 12:28:20', null);
INSERT INTO `files` VALUES ('54', 'thumbnail', '6feae71aad8a4dca953d7c6ca7abf38d.png', '人生轨迹线.png', 'G:\\upload\\202002\\6feae71aad8a4dca953d7c6ca7abf38d.png', 'image/png', '2020-02-07 15:01:34', null);
INSERT INTO `files` VALUES ('55', 'thumbnail', 'b24949566be74bcb83a382bb8523419b.png', '人生轨迹线.png', 'G:\\upload\\202002\\b24949566be74bcb83a382bb8523419b.png', 'image/png', '2020-02-07 15:10:55', null);
INSERT INTO `files` VALUES ('56', 'thumbnail', 'b3a705a7f4f64410baf381314127a625.png', '人生轨迹线.png', 'G:\\upload\\202002\\b3a705a7f4f64410baf381314127a625.png', 'image/png', '2020-02-07 17:37:22', null);
INSERT INTO `files` VALUES ('57', 'thumbnail', '995ae3d1228a445eb22883511f261e2a.png', '人生轨迹线.png', 'G:\\upload\\202002\\995ae3d1228a445eb22883511f261e2a.png', 'image/png', '2020-02-07 17:41:01', null);
INSERT INTO `files` VALUES ('58', 'annex', 'edf1df901c45412fa39f8bbf5b159f0c.pptx', '带日期时间的甘特图PPT模板.pptx', 'G:\\upload\\202002\\edf1df901c45412fa39f8bbf5b159f0c.pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', '2020-02-07 17:42:15', null);
INSERT INTO `files` VALUES ('59', 'thumbnail', 'f3e2233119a14c6db359c6bfdca18035.png', '招标平台.png', 'G:\\upload\\202002\\f3e2233119a14c6db359c6bfdca18035.png', 'image/png', '2020-02-07 17:44:23', null);
INSERT INTO `files` VALUES ('60', 'annex', '26d3ed9cb28c4abe93a0c2ff97c051de.pptx', '带日期时间的甘特图PPT模板.pptx', 'G:\\upload\\202002\\26d3ed9cb28c4abe93a0c2ff97c051de.pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', '2020-02-07 17:45:17', null);
INSERT INTO `files` VALUES ('61', 'annex', '13d524aead4f4c04871387430c459643.rar', '1-141224134T0.rar', 'G:\\upload\\202002\\13d524aead4f4c04871387430c459643.rar', 'application/octet-stream', '2020-02-07 17:45:34', null);
INSERT INTO `files` VALUES ('62', 'thumbnail', 'a189877237e04e47994f14f956f3472d.png', '人生轨迹线.png', 'G:\\upload\\202002\\a189877237e04e47994f14f956f3472d.png', 'image/png', '2020-02-07 17:45:58', null);
INSERT INTO `files` VALUES ('63', 'thumbnail', '8c2322c6fb754ee8963a290a48f567ea.png', '人生轨迹线.png', '202002\\8c2322c6fb754ee8963a290a48f567ea.png', 'image/png', '2020-02-07 21:28:38', null);
INSERT INTO `files` VALUES ('64', 'thumbnail', 'e624d4985e834718b38d761523b8d0b6.png', '人生轨迹线.png', '\\202002\\e624d4985e834718b38d761523b8d0b6.png', 'image/png', '2020-02-07 21:30:06', null);
INSERT INTO `files` VALUES ('65', 'annex', 'ded5b52f7e6f4d7db6e342336aae6881.pptx', '带日期时间的甘特图PPT模板.pptx', '\\202002\\ded5b52f7e6f4d7db6e342336aae6881.pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', '2020-02-07 21:30:21', null);
INSERT INTO `files` VALUES ('66', 'thumbnail', '9e8b6712e14c42f78359419a262ce465.jpg', '诗意2.jpg', '\\202002\\9e8b6712e14c42f78359419a262ce465.jpg', 'image/jpeg', '2020-02-09 21:13:32', null);
INSERT INTO `files` VALUES ('67', 'thumbnail', '6fa034b8fb524160ad1a9f92d8b31f36.jpg', '诗意3.jpg', '\\202002\\6fa034b8fb524160ad1a9f92d8b31f36.jpg', 'image/jpeg', '2020-02-09 21:13:52', null);
INSERT INTO `files` VALUES ('68', 'thumbnail', 'e7339fee9a02409d990831bd7cc9f1a0.jpg', '唯美古风2.jpg', '\\202002\\e7339fee9a02409d990831bd7cc9f1a0.jpg', 'image/jpeg', '2020-02-09 21:14:12', null);
INSERT INTO `files` VALUES ('69', 'thumbnail', 'be5b57c5bd874dd9bb6cd957db47c293.jpg', '唯美古风4.jpg', '\\202002\\be5b57c5bd874dd9bb6cd957db47c293.jpg', 'image/jpeg', '2020-02-09 21:23:52', null);
INSERT INTO `files` VALUES ('70', 'thumbnail', 'd8098431d8ea46ff8a9d500c229d67d5.jpg', '金庸   鹿鼎记3.jpg', '\\202002\\d8098431d8ea46ff8a9d500c229d67d5.jpg', 'image/jpeg', '2020-02-16 20:54:00', null);
INSERT INTO `files` VALUES ('71', 'thumbnail', 'a5764803b0ce449eadcd8c22f58203a9.png', 'JFinal 极速开发官方社区.png', '\\202002\\a5764803b0ce449eadcd8c22f58203a9.png', 'image/png', '2020-02-16 21:15:24', null);
INSERT INTO `files` VALUES ('72', 'thumbnail', '053024c6bdb649a5a1d227eeb9112e3b.png', '拼图Pintuer-跨屏响应式布局前端开发CSS框架.png', '\\202002\\053024c6bdb649a5a1d227eeb9112e3b.png', 'image/png', '2020-02-16 21:15:51', null);
INSERT INTO `files` VALUES ('73', 'thumbnail', '5b333c4a08e64bfc96f4cce801d41dd0.png', 'Layui - 经典模块化前端 UI 框架.png', '\\202002\\5b333c4a08e64bfc96f4cce801d41dd0.png', 'image/png', '2020-02-16 21:16:14', null);
INSERT INTO `files` VALUES ('74', 'thumbnail', 'd598fdf6f07f4394bd6ff4c1b6a644e2.png', 'Bootstrap中文网.png', '\\202002\\d598fdf6f07f4394bd6ff4c1b6a644e2.png', 'image/png', '2020-02-16 21:16:50', null);
INSERT INTO `files` VALUES ('75', 'thumbnail', 'a4381a53de7a482bb872929d26c21d28.png', 'Vue.js.png', '\\202002\\a4381a53de7a482bb872929d26c21d28.png', 'image/png', '2020-02-16 21:17:06', null);
INSERT INTO `files` VALUES ('76', 'thumbnail', '15da7dc8b42a481ca92015596dec72f1.png', '响应式网站模板,深圳响应式网站建设 - 经济套餐.png', '\\202002\\15da7dc8b42a481ca92015596dec72f1.png', 'image/png', '2020-02-16 21:17:52', null);
INSERT INTO `files` VALUES ('77', 'thumbnail', 'c5a95552440047abac389e5ac8340069.png', '人生轨迹线.png', '\\202002\\c5a95552440047abac389e5ac8340069.png', 'image/png', '2020-02-16 21:56:12', null);
INSERT INTO `files` VALUES ('78', 'annex', '2b6ed51dee1d4c0e86018db7d8cbe38e.rar', '1-1P531221008.rar', '\\202002\\2b6ed51dee1d4c0e86018db7d8cbe38e.rar', 'application/octet-stream', '2020-02-20 21:01:47', null);
INSERT INTO `files` VALUES ('79', 'annex', 'cfac351662694eca8868de40c9ad91b5.rar', '1-160223105223.rar', '\\202002\\cfac351662694eca8868de40c9ad91b5.rar', 'application/octet-stream', '2020-02-20 21:02:15', null);
INSERT INTO `files` VALUES ('80', 'artPic', '05f1acd16b844c4ab056066fd61a1473.jpg', '壁纸 (5).jpg', '\\202002\\05f1acd16b844c4ab056066fd61a1473.jpg', 'image/jpeg', '2020-02-24 21:04:07', null);
INSERT INTO `files` VALUES ('81', 'artPic', '4cfa710569d44c6e8b40b130b70249ef.jpg', '壁纸 (5).jpg', '\\202002\\4cfa710569d44c6e8b40b130b70249ef.jpg', 'image/jpeg', '2020-02-24 21:06:29', null);
INSERT INTO `files` VALUES ('82', 'annex', 'edc3d4a2f2224208aa9d7cdb9e0fa013.xls', '2016-事项完成情况记录表.xls', '\\202002\\edc3d4a2f2224208aa9d7cdb9e0fa013.xls', 'application/vnd.ms-excel', '2020-02-24 21:13:57', null);
INSERT INTO `files` VALUES ('83', 'artPic', '4ebf762356e445419103b3c4816d1ef7.jpg', '侧脸1.jpg', '\\202002\\4ebf762356e445419103b3c4816d1ef7.jpg', 'image/jpeg', '2020-02-24 21:15:14', null);
INSERT INTO `files` VALUES ('84', 'artPic', 'a21134f31f0e41c38f726ea723a91b01.jpg', '壁纸 (5).jpg', '\\202002\\a21134f31f0e41c38f726ea723a91b01.jpg', 'image/jpeg', '2020-02-24 21:15:14', null);
INSERT INTO `files` VALUES ('85', 'artPic', '06b208c00aeb473882a74ed7a40961ee.jpg', '侧脸1.jpg', '\\202002\\06b208c00aeb473882a74ed7a40961ee.jpg', 'image/jpeg', '2020-02-24 21:15:50', null);
INSERT INTO `files` VALUES ('86', 'artPic', '96b0d4d486134849bcf21a479d31290d.jpg', '壁纸 (5).jpg', '\\202002\\96b0d4d486134849bcf21a479d31290d.jpg', 'image/jpeg', '2020-02-24 21:15:50', null);
INSERT INTO `files` VALUES ('87', 'thumbnail', '9b5c42d9849e4e669f9cdd7736307b9a.jpg', '壁纸 (5).jpg', '\\202002\\9b5c42d9849e4e669f9cdd7736307b9a.jpg', 'image/jpeg', '2020-02-24 21:43:48', null);
INSERT INTO `files` VALUES ('88', 'thumbnail', '2f40027446f04065adaf6e353b9d7640.jpg', '美女 蔡少芬1.jpg', '\\202002\\2f40027446f04065adaf6e353b9d7640.jpg', 'image/jpeg', '2020-02-24 21:43:57', null);
INSERT INTO `files` VALUES ('89', 'annex', '0453a06debc441d58f5e55172dbf3073.xls', '2015-每天都得做的几件事完成情况记录表.xls', '\\202002\\0453a06debc441d58f5e55172dbf3073.xls', 'application/vnd.ms-excel', '2020-02-24 21:44:06', null);
INSERT INTO `files` VALUES ('90', 'annex', 'a1db16625aea484b88a7535f0ec96556.xls', '2014-每天都得做的几件事完成情况记录表.xls', '\\202002\\a1db16625aea484b88a7535f0ec96556.xls', 'application/vnd.ms-excel', '2020-02-24 21:44:11', null);
INSERT INTO `files` VALUES ('91', 'artPic', '99beb77114124b55b1bf66a3c12f70cd.JPG', '壁纸.JPG', '\\202002\\99beb77114124b55b1bf66a3c12f70cd.JPG', 'image/jpeg', '2020-02-24 22:03:43', null);
INSERT INTO `files` VALUES ('92', 'artPic', '037b3b43f6684cf586539caf3b5f6743.jpg', '壁纸 (5).jpg', '\\202002\\037b3b43f6684cf586539caf3b5f6743.jpg', 'image/jpeg', '2020-02-24 22:03:43', null);
INSERT INTO `files` VALUES ('93', 'artPic', '6ccafa61969a4a8a9d20454178ae74f7.jpg', '美女 王璐丹2.jpg', '\\202002\\6ccafa61969a4a8a9d20454178ae74f7.jpg', 'image/jpeg', '2020-02-24 22:04:05', null);
INSERT INTO `files` VALUES ('94', 'artPic', 'e80ed9f76e1b49f491e17545e5245f18.jpg', '美女 王璐丹3.jpg', '\\202002\\e80ed9f76e1b49f491e17545e5245f18.jpg', 'image/jpeg', '2020-02-24 22:04:05', null);
INSERT INTO `files` VALUES ('95', 'artPic', '84fba152b8fe43d38e217feebe4633fd.jpg', '美女 王璐丹4.jpg', '\\202002\\84fba152b8fe43d38e217feebe4633fd.jpg', 'image/jpeg', '2020-02-24 22:04:05', null);
INSERT INTO `files` VALUES ('96', 'artPic', '6e5680b09f1c49b7ae738480880c594e.jpg', '美女 叶璇.jpg', '\\202002\\6e5680b09f1c49b7ae738480880c594e.jpg', 'image/jpeg', '2020-02-24 22:04:32', null);
INSERT INTO `files` VALUES ('97', 'artPic', '183962d4243e4857a1b8db8a31d0caaf.jpg', '美女 李冰冰6.jpg', '\\202002\\183962d4243e4857a1b8db8a31d0caaf.jpg', 'image/jpeg', '2020-02-24 22:04:42', null);
INSERT INTO `files` VALUES ('98', 'artPic', '260c31c5753d4e49bdc171506b4fbb42.jpg', '美女 李冰冰5.jpg', '\\202002\\260c31c5753d4e49bdc171506b4fbb42.jpg', 'image/jpeg', '2020-02-24 22:04:42', null);
INSERT INTO `files` VALUES ('99', 'artPic', 'cdc1401af618480199aefd20df71b6e9.jpg', '美女 李冰冰4.jpg', '\\202002\\cdc1401af618480199aefd20df71b6e9.jpg', 'image/jpeg', '2020-02-24 22:04:42', null);
INSERT INTO `files` VALUES ('100', 'artPic', 'daeeab05f9964674825f96efe23f3e4b.jpg', '美女 蔡少芬1.jpg', '\\202002\\daeeab05f9964674825f96efe23f3e4b.jpg', 'image/jpeg', '2020-02-24 22:05:36', null);
INSERT INTO `files` VALUES ('101', 'artPic', '9ca8926dc071491dbfc268de98f09781.jpg', '侧脸1.jpg', '\\202002\\9ca8926dc071491dbfc268de98f09781.jpg', 'image/jpeg', '2020-02-24 22:05:59', null);
INSERT INTO `files` VALUES ('102', 'artPic', '6e399d2c231946b38bb9c139d77bf5e2.jpg', '侧脸2.jpg', '\\202002\\6e399d2c231946b38bb9c139d77bf5e2.jpg', 'image/jpeg', '2020-02-24 22:06:22', null);
INSERT INTO `files` VALUES ('103', 'artPic', '7a07d026c9ee4de29d41a7401c8822f2.jpg', '美女 李冰冰6.jpg', '\\202002\\7a07d026c9ee4de29d41a7401c8822f2.jpg', 'image/jpeg', '2020-02-26 21:21:13', null);
INSERT INTO `files` VALUES ('104', 'artPic', '12f92520210340b9b3fc37faedff2b6d.jpg', '美女 李冰冰4.jpg', '\\202002\\12f92520210340b9b3fc37faedff2b6d.jpg', 'image/jpeg', '2020-02-26 21:21:13', null);
INSERT INTO `files` VALUES ('105', 'artPic', '87f9a617f15245b5889951122d15331e.jpg', '美女 李冰冰5.jpg', '\\202002\\87f9a617f15245b5889951122d15331e.jpg', 'image/jpeg', '2020-02-26 21:21:13', null);
INSERT INTO `files` VALUES ('106', 'artPic', 'c97b425fa0b3484cbfb3fbc2ca2c3d48.jpg', '美女 蔡少芬2.jpg', '\\202002\\c97b425fa0b3484cbfb3fbc2ca2c3d48.jpg', 'image/jpeg', '2020-02-26 21:23:07', null);
INSERT INTO `files` VALUES ('107', 'artPic', '34a88760f5b9499bb567bcd1f6626d9d.jpg', '美女 蔡少芬1.jpg', '\\202002\\34a88760f5b9499bb567bcd1f6626d9d.jpg', 'image/jpeg', '2020-02-26 21:23:07', null);
INSERT INTO `files` VALUES ('108', 'artPic', 'fcb84858e34b4543ac350fd5a27c1f0b.jpg', '美女  长裙1.jpg', '\\202002\\fcb84858e34b4543ac350fd5a27c1f0b.jpg', 'image/jpeg', '2020-02-26 21:23:07', null);
INSERT INTO `files` VALUES ('109', 'artPic', '4466b5012ac94558bf59a923ff50c072.jpg', '美女  长裙1.jpg', '\\202002\\4466b5012ac94558bf59a923ff50c072.jpg', 'image/jpeg', '2020-02-26 21:46:46', null);
INSERT INTO `files` VALUES ('110', 'artPic', '118381500537498297b57e8e664c71bc.jpg', '壁纸 (5).jpg', '\\202002\\118381500537498297b57e8e664c71bc.jpg', 'image/jpeg', '2020-02-26 22:13:52', null);
INSERT INTO `files` VALUES ('111', 'artPic', '3b6d3d8c41c342c78eae306b3896262d.jpg', '壁纸 (5).jpg', '\\202002\\3b6d3d8c41c342c78eae306b3896262d.jpg', 'image/jpeg', '2020-02-26 22:14:53', null);
INSERT INTO `files` VALUES ('112', 'artPic', 'af9d5f463d4044ab92559eb0eaf20d74.jpg', '壁纸 (5).jpg', '\\202002\\af9d5f463d4044ab92559eb0eaf20d74.jpg', 'image/jpeg', '2020-02-26 22:15:43', null);
INSERT INTO `files` VALUES ('113', 'artPic', '3640153e3f3b40b89758de223dd4006b.jpg', '美女 蔡少芬1.jpg', '\\202002\\3640153e3f3b40b89758de223dd4006b.jpg', 'image/jpeg', '2020-02-26 22:17:42', null);
INSERT INTO `files` VALUES ('114', 'artPic', 'd9d8578e77954f63bc9ee795a1bb4cf6.jpg', '壁纸 (5).jpg', '\\202002\\d9d8578e77954f63bc9ee795a1bb4cf6.jpg', 'image/jpeg', '2020-02-26 22:22:50', null);
INSERT INTO `files` VALUES ('115', 'artPic', 'f1fdf600638546f6b99b38e4e4309ac5.jpg', '美女  长裙1.jpg', '\\202002\\f1fdf600638546f6b99b38e4e4309ac5.jpg', 'image/jpeg', '2020-02-26 22:24:34', null);
INSERT INTO `files` VALUES ('116', 'artPic', '16cc644427334a11b238152a62ada157.jpg', '壁纸 (4).jpg', '\\202002\\16cc644427334a11b238152a62ada157.jpg', 'image/jpeg', '2020-02-26 22:26:13', null);
INSERT INTO `files` VALUES ('117', 'artPic', 'c4725fe26b7d4a93b7674a23b01d043c.jpg', '美女  长裙1.jpg', '\\202002\\c4725fe26b7d4a93b7674a23b01d043c.jpg', 'image/jpeg', '2020-02-26 22:27:19', null);
INSERT INTO `files` VALUES ('118', 'artPic', '9718542bff7c414eaeb685e31ff92ab6.jpg', '美女  长裙1.jpg', '\\202002\\9718542bff7c414eaeb685e31ff92ab6.jpg', 'image/jpeg', '2020-02-26 22:28:20', null);
INSERT INTO `files` VALUES ('119', 'artPic', '454ac8fafc4b42efa4c7715c971a0c3e.jpg', '侧脸2.jpg', '\\202002\\454ac8fafc4b42efa4c7715c971a0c3e.jpg', 'image/jpeg', '2020-02-26 22:31:04', '图片描述测试');
INSERT INTO `files` VALUES ('120', 'thumbnail', '08b2602837cd454ba6e61c5ad8eb3f49.jpg', '壁纸 (4).jpg', '\\202003\\08b2602837cd454ba6e61c5ad8eb3f49.jpg', 'image/jpeg', '2020-03-08 13:35:21', null);
INSERT INTO `files` VALUES ('121', 'artPic', '5fd489963e5745e7a57faa2400f4b3d4.jpg', '壁纸 (5).jpg', '\\202003\\5fd489963e5745e7a57faa2400f4b3d4.jpg', 'image/jpeg', '2020-03-08 13:35:30', null);
INSERT INTO `files` VALUES ('122', 'artPic', '3a2608371a9c4962aaff8af5bbd0c19b.jpg', '侧脸1.jpg', '\\202003\\3a2608371a9c4962aaff8af5bbd0c19b.jpg', 'image/jpeg', '2020-03-08 13:35:30', null);
INSERT INTO `files` VALUES ('123', 'artPic', '8ffd9bdad0aa40859194ad0a5caa3fb2.jpg', '侧脸2.jpg', '\\202003\\8ffd9bdad0aa40859194ad0a5caa3fb2.jpg', 'image/jpeg', '2020-03-08 13:35:30', null);
INSERT INTO `files` VALUES ('124', 'artPic', 'deafacb4233546e39b682da21af5fbfc.jpg', '美女  长裙1.jpg', '\\202003\\deafacb4233546e39b682da21af5fbfc.jpg', 'image/jpeg', '2020-03-08 13:35:30', null);
INSERT INTO `files` VALUES ('125', 'artPic', '525e010c16484d419d2db78cf8a216fd.jpg', '美女 蔡少芬1.jpg', '\\202003\\525e010c16484d419d2db78cf8a216fd.jpg', 'image/jpeg', '2020-03-08 13:35:30', null);
INSERT INTO `files` VALUES ('126', 'artPic', '390dd49005bc4f6b99adcd5e2dd02ae3.jpg', '壁纸 (4).jpg', '\\202003\\390dd49005bc4f6b99adcd5e2dd02ae3.jpg', 'image/jpeg', '2020-03-08 13:35:30', null);
INSERT INTO `files` VALUES ('127', 'artPic', '6af2b2cc526a4a4d9acb5954d39991fa.jpg', '美女 蔡少芬2.jpg', '\\202003\\6af2b2cc526a4a4d9acb5954d39991fa.jpg', 'image/jpeg', '2020-03-08 13:35:30', null);
INSERT INTO `files` VALUES ('128', 'thumbnail', 'c924b44a237a4e89920ab237dbe74551.jpg', '壁纸 (5).jpg', '\\202003\\c924b44a237a4e89920ab237dbe74551.jpg', 'image/jpeg', '2020-03-08 17:05:00', null);
INSERT INTO `files` VALUES ('129', 'artPic', '746bef6c7358485696fc97e31782780f.jpg', '诗意3.jpg', '\\202003\\746bef6c7358485696fc97e31782780f.jpg', 'image/jpeg', '2020-03-08 17:05:21', null);
INSERT INTO `files` VALUES ('130', 'artPic', '580844420d924a1287e273d95fefb61a.jpg', '诗意2.jpg', '\\202003\\580844420d924a1287e273d95fefb61a.jpg', 'image/jpeg', '2020-03-08 17:05:21', null);
INSERT INTO `files` VALUES ('131', 'artPic', '3e069370165746188b4ef505bf780e0e.png', '唯美古风5_副本.png', '\\202003\\3e069370165746188b4ef505bf780e0e.png', 'image/png', '2020-03-08 17:05:21', null);
INSERT INTO `files` VALUES ('132', 'artPic', '87dd55c4ace748a3935928dda46c868c.jpg', '唯美古风4.jpg', '\\202003\\87dd55c4ace748a3935928dda46c868c.jpg', 'image/jpeg', '2020-03-08 17:05:21', null);
INSERT INTO `files` VALUES ('133', 'artPic', '02d839e9ddf64fc1ab08fb3183fc0480.jpg', '唯美古风5.jpg', '\\202003\\02d839e9ddf64fc1ab08fb3183fc0480.jpg', 'image/jpeg', '2020-03-08 17:05:21', null);
INSERT INTO `files` VALUES ('134', 'artPic', 'f76918f233fc45d6865482d9916ef37d.jpg', '唯美古风6.jpg', '\\202003\\f76918f233fc45d6865482d9916ef37d.jpg', 'image/jpeg', '2020-03-08 17:05:21', null);
INSERT INTO `files` VALUES ('135', 'artPic', 'e52a0acebd564615a1a8c7b3593eb6bf.jpg', '唯美古风8.jpg', '\\202003\\e52a0acebd564615a1a8c7b3593eb6bf.jpg', 'image/jpeg', '2020-03-08 17:05:21', null);
INSERT INTO `files` VALUES ('136', 'artPic', '2240ac2250294a1aae6e812fcf7edb75.jpg', '唯美古风7.jpg', '\\202003\\2240ac2250294a1aae6e812fcf7edb75.jpg', 'image/jpeg', '2020-03-08 17:05:21', null);
INSERT INTO `files` VALUES ('137', 'artPic', '8e23f2aa93df4721beea6c5d7154c099.jpg', '唯美古风3.jpg', '\\202003\\8e23f2aa93df4721beea6c5d7154c099.jpg', 'image/jpeg', '2020-03-08 17:05:21', null);
INSERT INTO `files` VALUES ('138', 'artPic', '9b4dff3408944348b7453b47f1e614ae.jpg', '唯美古风2.jpg', '\\202003\\9b4dff3408944348b7453b47f1e614ae.jpg', 'image/jpeg', '2020-03-08 17:05:21', null);
INSERT INTO `files` VALUES ('139', 'artPic', '4f3282f84d684c3893c32333604d2327.jpg', '唯美古风3.jpg', '\\202003\\4f3282f84d684c3893c32333604d2327.jpg', 'image/jpeg', '2020-03-08 17:33:08', null);
INSERT INTO `files` VALUES ('140', 'artPic', '55b1da081fb545c583d279e19def16a2.jpg', '唯美古风2.jpg', '\\202003\\55b1da081fb545c583d279e19def16a2.jpg', 'image/jpeg', '2020-03-08 17:33:08', null);
INSERT INTO `files` VALUES ('141', 'thumbnail', '806cdf4f57b448caa32fa95c91260347.jpg', '诗意2.jpg', '\\202003\\806cdf4f57b448caa32fa95c91260347.jpg', 'image/jpeg', '2020-03-08 17:34:44', null);
INSERT INTO `files` VALUES ('142', 'annex', '12cabf2691aa4eb0b464f1e85ef006cf.xls', '2016-事项完成情况记录表.xls', '\\202003\\12cabf2691aa4eb0b464f1e85ef006cf.xls', 'application/vnd.ms-excel', '2020-03-08 17:35:08', null);
INSERT INTO `files` VALUES ('143', 'thumbnail', '7634d08b9d6d4e92993c72d036aa02fb.jpg', '2002蜘蛛侠.jpg', '\\202003\\7634d08b9d6d4e92993c72d036aa02fb.jpg', 'image/jpeg', '2020-03-08 17:35:42', null);
INSERT INTO `files` VALUES ('144', 'artPic', '84c55294aa5749159a0c03252e85327e.jpg', '钢铁侠1.jpg', '\\202003\\84c55294aa5749159a0c03252e85327e.jpg', 'image/jpeg', '2020-03-08 17:40:04', null);
INSERT INTO `files` VALUES ('145', 'thumbnail', 'c1fc64cd541a4a9dbab83442a1df89a4.jpg', '哆啦A梦1.jpg', '\\202003\\c1fc64cd541a4a9dbab83442a1df89a4.jpg', 'image/jpeg', '2020-03-08 21:24:58', null);
INSERT INTO `files` VALUES ('146', 'artPic', 'ccc865bbac284d7f83329033b6674ab7.jpg', '桌面壁纸9.jpg', '\\202003\\ccc865bbac284d7f83329033b6674ab7.jpg', 'image/jpeg', '2020-03-08 21:25:06', null);
INSERT INTO `files` VALUES ('147', 'artPic', '3faaf2ad101c4d68b62bce0e130e6ae5.jpg', '桌面壁纸8.jpg', '\\202003\\3faaf2ad101c4d68b62bce0e130e6ae5.jpg', 'image/jpeg', '2020-03-08 21:25:06', null);
INSERT INTO `files` VALUES ('148', 'artPic', '79a7ea9914ba46148564865fc2c23950.jpg', '桌面壁纸3.jpg', '\\202003\\79a7ea9914ba46148564865fc2c23950.jpg', 'image/jpeg', '2020-03-08 21:25:06', null);
INSERT INTO `files` VALUES ('149', 'artPic', 'd5aacc20246142b09a180207886a828c.jpg', '桌面壁纸1.jpg', '\\202003\\d5aacc20246142b09a180207886a828c.jpg', 'image/jpeg', '2020-03-08 21:25:06', null);
INSERT INTO `files` VALUES ('150', 'thumbnail', 'c4f3aced0aaa4b739c5fc15f0b70db39.jpg', 'f64d39d942fbd98bfbf19c9d0afee291.jpg', '\\202003\\c4f3aced0aaa4b739c5fc15f0b70db39.jpg', 'image/jpeg', '2020-03-15 11:25:47', null);
INSERT INTO `files` VALUES ('151', 'artPic', 'cabb7dca8a8140dc804adb05bdcebb84.jpg', 'f64d39d942fbd98bfbf19c9d0afee291.jpg', '\\202003\\cabb7dca8a8140dc804adb05bdcebb84.jpg', 'image/jpeg', '2020-03-15 11:25:56', null);
INSERT INTO `files` VALUES ('152', 'artPic', 'a2db6faea1684cb8ae9ccbc31a593f2c.jpg', '钢铁侠1.jpg', '\\202003\\a2db6faea1684cb8ae9ccbc31a593f2c.jpg', 'image/jpeg', '2020-03-15 11:25:56', null);
INSERT INTO `files` VALUES ('153', 'artPic', '11ce28d60b3c4ac3b103a4d0bbaf8aa2.jpg', '哆啦A梦1.jpg', '\\202003\\11ce28d60b3c4ac3b103a4d0bbaf8aa2.jpg', 'image/jpeg', '2020-03-15 11:25:56', null);
INSERT INTO `files` VALUES ('154', 'artPic', 'd197e212a8e64cfba11715f6c65311d5.jpg', '桌面壁纸3.jpg', '\\202003\\d197e212a8e64cfba11715f6c65311d5.jpg', 'image/jpeg', '2020-03-15 11:25:57', null);
INSERT INTO `files` VALUES ('155', 'artPic', '00bc1d8bd120460a8a222d583d51c076.jpg', '桌面壁纸8.jpg', '\\202003\\00bc1d8bd120460a8a222d583d51c076.jpg', 'image/jpeg', '2020-03-15 11:25:57', null);
INSERT INTO `files` VALUES ('156', 'artPic', '0ade3479cf714fe29b0e5e56d44a26db.jpg', '桌面壁纸9.jpg', '\\202003\\0ade3479cf714fe29b0e5e56d44a26db.jpg', 'image/jpeg', '2020-03-15 11:25:57', null);
INSERT INTO `files` VALUES ('157', 'artPic', 'b54c1ee376cb4283a36ebb90a9a74707.jpg', '桌面壁纸1.jpg', '\\202003\\b54c1ee376cb4283a36ebb90a9a74707.jpg', 'image/jpeg', '2020-03-15 11:25:57', null);
INSERT INTO `files` VALUES ('158', 'thumbnail', '115cdbf710514a70b6608ff1f14d327a.jpg', '1-14122H03210495.jpg', '\\202003\\115cdbf710514a70b6608ff1f14d327a.jpg', 'image/jpeg', '2020-03-15 14:27:13', null);
INSERT INTO `files` VALUES ('159', 'artImg', '2b745a1a47334758b12d2fb6cd7b4df2.png', 'image.png', '\\202009\\2b745a1a47334758b12d2fb6cd7b4df2.png', 'image/png', '2020-09-20 18:25:25', null);
INSERT INTO `files` VALUES ('160', 'artImg', '6b4fdd29762848bfa360e2a5e3b90dff.png', 'image.png', '\\202009\\6b4fdd29762848bfa360e2a5e3b90dff.png', 'image/png', '2020-09-20 18:25:59', null);
INSERT INTO `files` VALUES ('161', 'artImg', '92191e43d3874283977bb478f2220c1d.png', 'image.png', '\\202009\\92191e43d3874283977bb478f2220c1d.png', 'image/png', '2020-09-20 18:26:23', null);
INSERT INTO `files` VALUES ('162', 'artImg', '8c5668aa594f490a9cd15bb26a493ae7.png', 'image.png', '\\202009\\8c5668aa594f490a9cd15bb26a493ae7.png', 'image/png', '2020-09-20 18:26:53', null);
INSERT INTO `files` VALUES ('163', 'artImg', '8bee1e5e795144adbebe36f4da910b2a.png', 'image.png', '\\202009\\8bee1e5e795144adbebe36f4da910b2a.png', 'image/png', '2020-09-20 18:27:13', null);
INSERT INTO `files` VALUES ('164', 'artImg', 'f9c300086e9b448fa073990a66a708bf.png', 'image.png', '\\202009\\f9c300086e9b448fa073990a66a708bf.png', 'image/png', '2020-09-20 18:27:28', null);
INSERT INTO `files` VALUES ('165', 'artImg', 'c1b68ead988349e4a397a2f377dcf945.png', 'image.png', '\\202009\\c1b68ead988349e4a397a2f377dcf945.png', 'image/png', '2020-09-20 18:27:39', null);
INSERT INTO `files` VALUES ('166', 'artImg', '73a1dd408142439c98038c30d4bde14b.png', 'image.png', '\\202009\\73a1dd408142439c98038c30d4bde14b.png', 'image/png', '2020-09-20 18:27:58', null);
INSERT INTO `files` VALUES ('167', 'thumbnail', '441fe7b45f7d454eb17ddc22f5910389.jpg', 'qrcode_for_gh_fbd4bda6778f_430.jpg', '\\202009\\441fe7b45f7d454eb17ddc22f5910389.jpg', 'image/jpeg', '2020-09-20 18:32:19', null);
INSERT INTO `files` VALUES ('168', 'thumbnail', '123f0cfac0484c6d89a7c494963e4785.png', 'sitesCMS官网.png', '\\202009\\123f0cfac0484c6d89a7c494963e4785.png', 'image/png', '2020-09-20 20:27:11', null);
INSERT INTO `files` VALUES ('169', 'thumbnail', '9581084adb7744aa81cd20687e8fcb30.png', '环保科技公司-仿.png', '\\202009\\9581084adb7744aa81cd20687e8fcb30.png', 'image/png', '2020-09-21 20:42:55', null);
INSERT INTO `files` VALUES ('170', 'thumbnail', '89da5be24b844d4b8c7fd11865110234.jpg', 'Product_img4.jpg', '\\202009\\89da5be24b844d4b8c7fd11865110234.jpg', 'image/jpeg', '2020-09-22 20:20:16', null);
INSERT INTO `files` VALUES ('171', 'thumbnail', '841f1f9b66714cc6bd491c31607db2a9.jpg', 'Product_img3.jpg', '\\202009\\841f1f9b66714cc6bd491c31607db2a9.jpg', 'image/jpeg', '2020-09-22 20:21:21', null);
INSERT INTO `files` VALUES ('172', 'thumbnail', '3518a1fc115b4eb288f78904f98582bf.jpg', 'Product_img2.jpg', '\\202009\\3518a1fc115b4eb288f78904f98582bf.jpg', 'image/jpeg', '2020-09-22 20:22:00', null);
INSERT INTO `files` VALUES ('173', 'thumbnail', '08923c482938418fb28348ca1013a98f.jpg', 'Product_img1.jpg', '\\202009\\08923c482938418fb28348ca1013a98f.jpg', 'image/jpeg', '2020-09-22 20:22:29', null);
INSERT INTO `files` VALUES ('174', 'annex', '033d8c1c957042b08ec762a9b268402f.jpg', 'Product_img4.jpg', '\\202009\\033d8c1c957042b08ec762a9b268402f.jpg', 'image/jpeg', '2020-09-22 20:31:26', null);
INSERT INTO `files` VALUES ('175', 'thumbnail', '96947e4798334412b07f2a67a69c3b0d.png', 'Big_icon4.png', '\\202009\\96947e4798334412b07f2a67a69c3b0d.png', 'image/png', '2020-09-22 20:31:33', null);
INSERT INTO `files` VALUES ('176', 'thumbnail', '8cf5fc4c2ec24d019e3bf4692c28234d.png', 'Big_icon3.png', '\\202009\\8cf5fc4c2ec24d019e3bf4692c28234d.png', 'image/png', '2020-09-22 20:31:49', null);
INSERT INTO `files` VALUES ('177', 'annex', 'dd3450752c5742e59c240268c28972dc.jpg', 'Product_img3.jpg', '\\202009\\dd3450752c5742e59c240268c28972dc.jpg', 'image/jpeg', '2020-09-22 20:31:56', null);
INSERT INTO `files` VALUES ('178', 'thumbnail', 'f4480759a57142e389ab1785be4739bb.png', 'Big_icon2.png', '\\202009\\f4480759a57142e389ab1785be4739bb.png', 'image/png', '2020-09-22 20:32:08', null);
INSERT INTO `files` VALUES ('179', 'annex', '3ef5ec68e6af416881dc5b377dcb5f0d.jpg', 'Product_img2.jpg', '\\202009\\3ef5ec68e6af416881dc5b377dcb5f0d.jpg', 'image/jpeg', '2020-09-22 20:32:14', null);
INSERT INTO `files` VALUES ('180', 'thumbnail', '7a3662f183b740b28cbafac0527c3583.png', 'Big_icon1.png', '\\202009\\7a3662f183b740b28cbafac0527c3583.png', 'image/png', '2020-09-22 20:32:25', null);
INSERT INTO `files` VALUES ('181', 'annex', '836d28925abf43abb23693c6db90143b.jpg', 'Product_img1.jpg', '\\202009\\836d28925abf43abb23693c6db90143b.jpg', 'image/jpeg', '2020-09-22 20:32:32', null);
INSERT INTO `files` VALUES ('182', 'thumbnail', 'af375af28ef8458180163bdc28e1f485.jpg', 'news_img1.jpg', '\\202009\\af375af28ef8458180163bdc28e1f485.jpg', 'image/jpeg', '2020-09-23 23:05:24', null);
INSERT INTO `files` VALUES ('183', 'thumbnail', '9a97ef6ca3f346fbb2e515845d84a6ef.jpg', 'news_img1.jpg', '\\202009\\9a97ef6ca3f346fbb2e515845d84a6ef.jpg', 'image/jpeg', '2020-09-23 23:35:19', null);
INSERT INTO `files` VALUES ('184', 'thumbnail', '4a368d8fd57947f98cf28b7a1776c06d.jpg', 'case1.jpg', '\\202009\\4a368d8fd57947f98cf28b7a1776c06d.jpg', 'image/jpeg', '2020-09-24 19:27:02', null);
INSERT INTO `files` VALUES ('185', 'thumbnail', '593608c966164eeca6048cc9c91ef6bd.jpg', 'case2.jpg', '\\202009\\593608c966164eeca6048cc9c91ef6bd.jpg', 'image/jpeg', '2020-09-24 19:27:31', null);
INSERT INTO `files` VALUES ('186', 'thumbnail', '9aef6ac13bfc4072923b70e2b654e838.jpg', 'case3.jpg', '\\202009\\9aef6ac13bfc4072923b70e2b654e838.jpg', 'image/jpeg', '2020-09-24 19:27:56', null);
INSERT INTO `files` VALUES ('187', 'thumbnail', '11ec764f83354e13814c6a446f40a9ed.jpg', 'case4.jpg', '\\202009\\11ec764f83354e13814c6a446f40a9ed.jpg', 'image/jpeg', '2020-09-24 19:34:44', null);
INSERT INTO `files` VALUES ('188', 'thumbnail', 'a062f3d0a91541f3bd3ea4bf4fc0335d.jpg', 'case5.jpg', '\\202009\\a062f3d0a91541f3bd3ea4bf4fc0335d.jpg', 'image/jpeg', '2020-09-24 19:35:23', null);
INSERT INTO `files` VALUES ('189', 'thumbnail', '1dbadeaba6fc4074abca0630d50748ef.jpg', 'IMG_20171201_193029.jpg', '\\202010\\1dbadeaba6fc4074abca0630d50748ef.jpg', 'image/jpeg', '2020-10-02 20:46:29', null);
INSERT INTO `files` VALUES ('190', 'annex', '25cfa498ac8948d3be5b83b328f4546a.jpg', 'sitesCMS-v2.jpg', '\\202010\\25cfa498ac8948d3be5b83b328f4546a.jpg', 'image/jpeg', '2020-10-02 20:47:13', null);
INSERT INTO `files` VALUES ('191', 'thumbnail', 'bf90251437e54c94966d7bc2e3e0a52a.jpg', 'sitesCMS.jpg', '\\202010\\bf90251437e54c94966d7bc2e3e0a52a.jpg', 'image/jpeg', '2020-10-02 20:47:30', null);
INSERT INTO `files` VALUES ('192', 'artPic', '123ba0847b5642d3a880f87b0dbb92d3.jpg', 'sitesCMS.jpg', '\\202010\\123ba0847b5642d3a880f87b0dbb92d3.jpg', 'image/jpeg', '2020-10-02 20:47:39', null);
INSERT INTO `files` VALUES ('193', 'thumbnail', '1aaa35796dcc45c386a85cfd88eab9ff.jpg', 'sitesCMS.jpg', '\\202010\\1aaa35796dcc45c386a85cfd88eab9ff.jpg', 'image/jpeg', '2020-10-02 20:47:52', null);

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL auto_increment,
  `actionKey` varchar(512) default NULL,
  `controller` varchar(512) default NULL,
  `remark` varchar(1024) default NULL,
  `updateTime` datetime default NULL,
  `status` varchar(1) default '1' COMMENT '启用状态',
  `siteId` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=391 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('22', '/cms', 'com.sites.cms.index.IndexController', '菜单：后台首页', '2020-01-31 19:38:39', '2', '0');
INSERT INTO `permission` VALUES ('23', '/cmsAccount/deleteAccount', 'com.sites.cms.account.AccountController', '按钮：删除用户', null, '2', '0');
INSERT INTO `permission` VALUES ('24', '/cmsAccount/enterAccountAdd', 'com.sites.cms.account.AccountController', '菜单：新建用户', null, '1', '0');
INSERT INTO `permission` VALUES ('25', '/cmsAccount/enterAccountEdit', 'com.sites.cms.account.AccountController', '按钮：编辑用户', '2020-02-02 20:47:57', '2', '0');
INSERT INTO `permission` VALUES ('26', '/cmsAccount/enterAccountMng', 'com.sites.cms.account.AccountController', '菜单：用户管理', null, '1', '0');
INSERT INTO `permission` VALUES ('27', '/cmsAccount/enterAccountRole', 'com.sites.cms.account.AccountController', '按钮：用户分配角色', '2020-02-02 20:48:02', '2', '0');
INSERT INTO `permission` VALUES ('28', '/cmsAccount/saveAccount', 'com.sites.cms.account.AccountController', '按钮：保存用户', '2020-02-02 20:48:05', '2', '0');
INSERT INTO `permission` VALUES ('29', '/cmsAccount/updateAccount', 'com.sites.cms.account.AccountController', '按钮：更新用户', '2020-02-02 20:48:11', '2', '0');
INSERT INTO `permission` VALUES ('30', '/cmsAccount/updateAccountRole', 'com.sites.cms.account.AccountController', '按钮：更新用户角色', '2020-02-02 20:48:16', '2', '0');
INSERT INTO `permission` VALUES ('31', '/cmsLogin', 'com.sites.cms.login.LoginController', null, '2020-01-31 19:14:03', '2', '0');
INSERT INTO `permission` VALUES ('32', '/cmsLogin/createCaptcha', 'com.sites.cms.login.LoginController', null, '2020-01-31 19:14:07', '2', '0');
INSERT INTO `permission` VALUES ('33', '/cmsLogin/doLogin', 'com.sites.cms.login.LoginController', null, '2020-01-31 19:14:10', '2', '0');
INSERT INTO `permission` VALUES ('34', '/cmsLogin/logOut', 'com.sites.cms.login.LoginController', null, '2020-01-31 19:14:13', '2', '0');
INSERT INTO `permission` VALUES ('35', '/cmsPerm/deletePerm', 'com.sites.cms.permission.PermissionController', '按钮：删除权限信息', '2020-02-02 20:48:21', '2', '0');
INSERT INTO `permission` VALUES ('36', '/cmsPerm/enterPermEdit', 'com.sites.cms.permission.PermissionController', '按钮：编辑权限信息', '2020-02-02 20:48:25', '2', '0');
INSERT INTO `permission` VALUES ('37', '/cmsPerm/enterPermMng', 'com.sites.cms.permission.PermissionController', '菜单：权限管理', null, '1', '0');
INSERT INTO `permission` VALUES ('38', '/cmsPerm/reloadPerm', 'com.sites.cms.permission.PermissionController', '按钮：一键同步权限列表', '2020-02-02 20:48:31', '2', '0');
INSERT INTO `permission` VALUES ('39', '/cmsPerm/updatePerm', 'com.sites.cms.permission.PermissionController', '按钮：更新权限信息', '2020-02-02 20:48:36', '2', '0');
INSERT INTO `permission` VALUES ('40', '/cmsRole/enterRoleAdd', 'com.sites.cms.role.RoleController', '菜单：添加角色', '2020-01-31 19:56:56', '1', '0');
INSERT INTO `permission` VALUES ('41', '/cmsRole/enterRoleEdit', 'com.sites.cms.role.RoleController', '按钮：编辑角色信息', '2020-02-02 20:48:44', '2', '0');
INSERT INTO `permission` VALUES ('42', '/cmsRole/enterRoleMng', 'com.sites.cms.role.RoleController', '菜单：角色管理', null, '1', '0');
INSERT INTO `permission` VALUES ('43', '/cmsRole/enterRolePerm', 'com.sites.cms.role.RoleController', '按钮：编辑角色权限', '2020-02-02 20:48:52', '2', '0');
INSERT INTO `permission` VALUES ('44', '/cmsRole/saveRole', 'com.sites.cms.role.RoleController', '按钮：保存角色信息', '2020-02-02 20:48:56', '2', '0');
INSERT INTO `permission` VALUES ('45', '/cmsRole/updateRole', 'com.sites.cms.role.RoleController', '按钮：更新角色信息', '2020-02-02 20:49:02', '2', '0');
INSERT INTO `permission` VALUES ('46', '/cmsRole/updateRolePerm', 'com.sites.cms.role.RoleController', '按钮：更新角色权限', '2020-02-02 20:49:08', '2', '0');
INSERT INTO `permission` VALUES ('47', '/cmsSite/enterSiteAdd', 'com.sites.cms.site.SiteController', '菜单：新增站点', '2020-02-01 13:44:26', '1', '0');
INSERT INTO `permission` VALUES ('48', '/cms', 'com.sites.cms.index.IndexController', '菜单：后台首页', '2020-02-02 17:57:11', '2', '8');
INSERT INTO `permission` VALUES ('49', '/cmsAccount/deleteAccount', 'com.sites.cms.account.AccountController', '按钮：删除用户', '2020-02-02 17:57:11', '2', '8');
INSERT INTO `permission` VALUES ('50', '/cmsAccount/enterAccountAdd', 'com.sites.cms.account.AccountController', '菜单：新建用户', '2020-02-02 17:57:11', '1', '8');
INSERT INTO `permission` VALUES ('51', '/cmsAccount/enterAccountEdit', 'com.sites.cms.account.AccountController', '按钮：编辑用户', '2020-02-02 17:57:11', '1', '8');
INSERT INTO `permission` VALUES ('52', '/cmsAccount/enterAccountMng', 'com.sites.cms.account.AccountController', '菜单：用户管理', '2020-02-02 17:57:11', '1', '8');
INSERT INTO `permission` VALUES ('53', '/cmsAccount/enterAccountRole', 'com.sites.cms.account.AccountController', '按钮：用户分配角色', '2020-02-02 17:57:11', '1', '8');
INSERT INTO `permission` VALUES ('54', '/cmsAccount/saveAccount', 'com.sites.cms.account.AccountController', '按钮：保存用户', '2020-02-02 17:57:11', '1', '8');
INSERT INTO `permission` VALUES ('55', '/cmsAccount/updateAccount', 'com.sites.cms.account.AccountController', '按钮：更新用户', '2020-02-02 17:57:11', '1', '8');
INSERT INTO `permission` VALUES ('56', '/cmsAccount/updateAccountRole', 'com.sites.cms.account.AccountController', '按钮：更新用户角色', '2020-02-02 17:57:11', '1', '8');
INSERT INTO `permission` VALUES ('57', '/cmsLogin', 'com.sites.cms.login.LoginController', null, '2020-02-02 17:57:11', '2', '8');
INSERT INTO `permission` VALUES ('58', '/cmsLogin/createCaptcha', 'com.sites.cms.login.LoginController', null, '2020-02-02 17:57:11', '2', '8');
INSERT INTO `permission` VALUES ('59', '/cmsLogin/doLogin', 'com.sites.cms.login.LoginController', null, '2020-02-02 17:57:11', '2', '8');
INSERT INTO `permission` VALUES ('60', '/cmsLogin/logOut', 'com.sites.cms.login.LoginController', null, '2020-02-02 17:57:11', '2', '8');
INSERT INTO `permission` VALUES ('61', '/cmsPerm/deletePerm', 'com.sites.cms.permission.PermissionController', '按钮：删除权限信息', '2020-02-02 17:57:11', '1', '8');
INSERT INTO `permission` VALUES ('62', '/cmsPerm/enterPermEdit', 'com.sites.cms.permission.PermissionController', '按钮：编辑权限信息', '2020-02-02 17:57:11', '1', '8');
INSERT INTO `permission` VALUES ('63', '/cmsPerm/enterPermMng', 'com.sites.cms.permission.PermissionController', '菜单：权限管理', '2020-02-02 17:57:11', '1', '8');
INSERT INTO `permission` VALUES ('64', '/cmsPerm/reloadPerm', 'com.sites.cms.permission.PermissionController', '按钮：一键同步权限列表', '2020-02-02 17:57:11', '1', '8');
INSERT INTO `permission` VALUES ('65', '/cmsPerm/updatePerm', 'com.sites.cms.permission.PermissionController', '按钮：更新权限信息', '2020-02-02 17:57:11', '1', '8');
INSERT INTO `permission` VALUES ('66', '/cmsRole/enterRoleAdd', 'com.sites.cms.role.RoleController', '菜单：添加角色', '2020-02-02 17:57:11', '1', '8');
INSERT INTO `permission` VALUES ('67', '/cmsRole/enterRoleEdit', 'com.sites.cms.role.RoleController', '按钮：编辑角色信息', '2020-02-02 17:57:11', '1', '8');
INSERT INTO `permission` VALUES ('68', '/cmsRole/enterRoleMng', 'com.sites.cms.role.RoleController', '菜单：角色管理', '2020-02-02 17:57:11', '1', '8');
INSERT INTO `permission` VALUES ('69', '/cmsRole/enterRolePerm', 'com.sites.cms.role.RoleController', '按钮：编辑角色权限', '2020-02-02 17:57:12', '1', '8');
INSERT INTO `permission` VALUES ('70', '/cmsRole/saveRole', 'com.sites.cms.role.RoleController', '按钮：保存角色信息', '2020-02-02 17:57:12', '1', '8');
INSERT INTO `permission` VALUES ('71', '/cmsRole/updateRole', 'com.sites.cms.role.RoleController', '按钮：更新角色信息', '2020-02-02 17:57:12', '1', '8');
INSERT INTO `permission` VALUES ('72', '/cmsRole/updateRolePerm', 'com.sites.cms.role.RoleController', '按钮：更新角色权限', '2020-02-02 17:57:12', '1', '8');
INSERT INTO `permission` VALUES ('73', '/cmsSite/enterSiteAdd', 'com.sites.cms.site.SiteController', '菜单：新增站点', '2020-02-02 17:57:12', '2', '8');
INSERT INTO `permission` VALUES ('74', '/cms', 'com.sites.cms.index.IndexController', '菜单：后台首页', '2020-02-02 17:58:00', '2', '9');
INSERT INTO `permission` VALUES ('75', '/cmsAccount/deleteAccount', 'com.sites.cms.account.AccountController', '按钮：删除用户', '2020-02-02 17:58:00', '2', '9');
INSERT INTO `permission` VALUES ('76', '/cmsAccount/enterAccountAdd', 'com.sites.cms.account.AccountController', '菜单：新建用户', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('77', '/cmsAccount/enterAccountEdit', 'com.sites.cms.account.AccountController', '按钮：编辑用户', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('78', '/cmsAccount/enterAccountMng', 'com.sites.cms.account.AccountController', '菜单：用户管理', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('79', '/cmsAccount/enterAccountRole', 'com.sites.cms.account.AccountController', '按钮：用户分配角色', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('80', '/cmsAccount/saveAccount', 'com.sites.cms.account.AccountController', '按钮：保存用户', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('81', '/cmsAccount/updateAccount', 'com.sites.cms.account.AccountController', '按钮：更新用户', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('82', '/cmsAccount/updateAccountRole', 'com.sites.cms.account.AccountController', '按钮：更新用户角色', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('83', '/cmsLogin', 'com.sites.cms.login.LoginController', null, '2020-02-02 17:58:01', '2', '9');
INSERT INTO `permission` VALUES ('84', '/cmsLogin/createCaptcha', 'com.sites.cms.login.LoginController', null, '2020-02-02 17:58:01', '2', '9');
INSERT INTO `permission` VALUES ('85', '/cmsLogin/doLogin', 'com.sites.cms.login.LoginController', null, '2020-02-02 17:58:01', '2', '9');
INSERT INTO `permission` VALUES ('86', '/cmsLogin/logOut', 'com.sites.cms.login.LoginController', null, '2020-02-02 17:58:01', '2', '9');
INSERT INTO `permission` VALUES ('87', '/cmsPerm/deletePerm', 'com.sites.cms.permission.PermissionController', '按钮：删除权限信息', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('88', '/cmsPerm/enterPermEdit', 'com.sites.cms.permission.PermissionController', '按钮：编辑权限信息', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('89', '/cmsPerm/enterPermMng', 'com.sites.cms.permission.PermissionController', '菜单：权限管理', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('90', '/cmsPerm/reloadPerm', 'com.sites.cms.permission.PermissionController', '按钮：一键同步权限列表', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('91', '/cmsPerm/updatePerm', 'com.sites.cms.permission.PermissionController', '按钮：更新权限信息', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('92', '/cmsRole/enterRoleAdd', 'com.sites.cms.role.RoleController', '菜单：添加角色', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('93', '/cmsRole/enterRoleEdit', 'com.sites.cms.role.RoleController', '按钮：编辑角色信息', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('94', '/cmsRole/enterRoleMng', 'com.sites.cms.role.RoleController', '菜单：角色管理', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('95', '/cmsRole/enterRolePerm', 'com.sites.cms.role.RoleController', '按钮：编辑角色权限', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('96', '/cmsRole/saveRole', 'com.sites.cms.role.RoleController', '按钮：保存角色信息', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('97', '/cmsRole/updateRole', 'com.sites.cms.role.RoleController', '按钮：更新角色信息', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('98', '/cmsRole/updateRolePerm', 'com.sites.cms.role.RoleController', '按钮：更新角色权限', '2020-02-02 17:58:01', '1', '9');
INSERT INTO `permission` VALUES ('99', '/cmsSite/enterSiteAdd', 'com.sites.cms.site.SiteController', '菜单：新增站点', '2020-02-02 17:58:01', '2', '9');
INSERT INTO `permission` VALUES ('100', '/cms', 'com.sites.cms.index.IndexController', '菜单：后台首页', '2020-02-02 17:59:21', '2', '10');
INSERT INTO `permission` VALUES ('101', '/cmsAccount/deleteAccount', 'com.sites.cms.account.AccountController', '按钮：删除用户', '2020-02-02 17:59:21', '2', '10');
INSERT INTO `permission` VALUES ('102', '/cmsAccount/enterAccountAdd', 'com.sites.cms.account.AccountController', '菜单：新建用户', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('103', '/cmsAccount/enterAccountEdit', 'com.sites.cms.account.AccountController', '按钮：编辑用户', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('104', '/cmsAccount/enterAccountMng', 'com.sites.cms.account.AccountController', '菜单：用户管理', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('105', '/cmsAccount/enterAccountRole', 'com.sites.cms.account.AccountController', '按钮：用户分配角色', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('106', '/cmsAccount/saveAccount', 'com.sites.cms.account.AccountController', '按钮：保存用户', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('107', '/cmsAccount/updateAccount', 'com.sites.cms.account.AccountController', '按钮：更新用户', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('108', '/cmsAccount/updateAccountRole', 'com.sites.cms.account.AccountController', '按钮：更新用户角色', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('109', '/cmsLogin', 'com.sites.cms.login.LoginController', null, '2020-02-02 17:59:21', '2', '10');
INSERT INTO `permission` VALUES ('110', '/cmsLogin/createCaptcha', 'com.sites.cms.login.LoginController', null, '2020-02-02 17:59:21', '2', '10');
INSERT INTO `permission` VALUES ('111', '/cmsLogin/doLogin', 'com.sites.cms.login.LoginController', null, '2020-02-02 17:59:21', '2', '10');
INSERT INTO `permission` VALUES ('112', '/cmsLogin/logOut', 'com.sites.cms.login.LoginController', null, '2020-02-02 17:59:21', '2', '10');
INSERT INTO `permission` VALUES ('113', '/cmsPerm/deletePerm', 'com.sites.cms.permission.PermissionController', '按钮：删除权限信息', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('114', '/cmsPerm/enterPermEdit', 'com.sites.cms.permission.PermissionController', '按钮：编辑权限信息', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('115', '/cmsPerm/enterPermMng', 'com.sites.cms.permission.PermissionController', '菜单：权限管理', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('116', '/cmsPerm/reloadPerm', 'com.sites.cms.permission.PermissionController', '按钮：一键同步权限列表', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('117', '/cmsPerm/updatePerm', 'com.sites.cms.permission.PermissionController', '按钮：更新权限信息', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('118', '/cmsRole/enterRoleAdd', 'com.sites.cms.role.RoleController', '菜单：添加角色', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('119', '/cmsRole/enterRoleEdit', 'com.sites.cms.role.RoleController', '按钮：编辑角色信息', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('120', '/cmsRole/enterRoleMng', 'com.sites.cms.role.RoleController', '菜单：角色管理', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('121', '/cmsRole/enterRolePerm', 'com.sites.cms.role.RoleController', '按钮：编辑角色权限', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('122', '/cmsRole/saveRole', 'com.sites.cms.role.RoleController', '按钮：保存角色信息', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('123', '/cmsRole/updateRole', 'com.sites.cms.role.RoleController', '按钮：更新角色信息', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('124', '/cmsRole/updateRolePerm', 'com.sites.cms.role.RoleController', '按钮：更新角色权限', '2020-02-02 17:59:21', '1', '10');
INSERT INTO `permission` VALUES ('125', '/cmsSite/enterSiteAdd', 'com.sites.cms.site.SiteController', '菜单：新增站点', '2020-02-02 17:59:22', '2', '10');
INSERT INTO `permission` VALUES ('126', '/cmsSite/enterSiteEdit', 'com.sites.cms.site.SiteController', '按钮：编辑站点', '2020-02-02 20:49:44', '2', '0');
INSERT INTO `permission` VALUES ('127', '/cmsSite/enterSiteMng', 'com.sites.cms.site.SiteController', '菜单：站点管理', null, '1', '0');
INSERT INTO `permission` VALUES ('128', '/cmsSite/saveSite', 'com.sites.cms.site.SiteController', '按钮：保存站点', '2020-02-02 20:49:36', '2', '0');
INSERT INTO `permission` VALUES ('129', '/cmsSite/setMainSite', 'com.sites.cms.site.SiteController', '按钮：设置当前站点', '2020-02-02 20:49:31', '2', '0');
INSERT INTO `permission` VALUES ('130', '/cmsSite/updateSite', 'com.sites.cms.site.SiteController', '按钮：更新站点', '2020-02-02 20:49:27', '2', '0');
INSERT INTO `permission` VALUES ('131', '/cms', 'com.sites.cms.index.IndexController', '菜单：后台首页', '2020-02-02 20:58:55', '2', '11');
INSERT INTO `permission` VALUES ('132', '/cmsAccount/deleteAccount', 'com.sites.cms.account.AccountController', '按钮：删除用户', '2020-02-02 20:58:55', '2', '11');
INSERT INTO `permission` VALUES ('133', '/cmsAccount/enterAccountAdd', 'com.sites.cms.account.AccountController', '菜单：新建用户', '2020-02-02 20:58:55', '1', '11');
INSERT INTO `permission` VALUES ('134', '/cmsAccount/enterAccountEdit', 'com.sites.cms.account.AccountController', '按钮：编辑用户', '2020-02-02 20:58:55', '2', '11');
INSERT INTO `permission` VALUES ('135', '/cmsAccount/enterAccountMng', 'com.sites.cms.account.AccountController', '菜单：用户管理', '2020-02-02 20:58:55', '1', '11');
INSERT INTO `permission` VALUES ('136', '/cmsAccount/enterAccountRole', 'com.sites.cms.account.AccountController', '按钮：用户分配角色', '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('137', '/cmsAccount/saveAccount', 'com.sites.cms.account.AccountController', '按钮：保存用户', '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('138', '/cmsAccount/updateAccount', 'com.sites.cms.account.AccountController', '按钮：更新用户', '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('139', '/cmsAccount/updateAccountRole', 'com.sites.cms.account.AccountController', '按钮：更新用户角色', '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('140', '/cmsLogin', 'com.sites.cms.login.LoginController', null, '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('141', '/cmsLogin/createCaptcha', 'com.sites.cms.login.LoginController', null, '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('142', '/cmsLogin/doLogin', 'com.sites.cms.login.LoginController', null, '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('143', '/cmsLogin/logOut', 'com.sites.cms.login.LoginController', null, '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('144', '/cmsPerm/deletePerm', 'com.sites.cms.permission.PermissionController', '按钮：删除权限信息', '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('145', '/cmsPerm/enterPermEdit', 'com.sites.cms.permission.PermissionController', '按钮：编辑权限信息', '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('146', '/cmsPerm/enterPermMng', 'com.sites.cms.permission.PermissionController', '菜单：权限管理', '2020-02-02 20:58:56', '1', '11');
INSERT INTO `permission` VALUES ('147', '/cmsPerm/reloadPerm', 'com.sites.cms.permission.PermissionController', '按钮：一键同步权限列表', '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('148', '/cmsPerm/updatePerm', 'com.sites.cms.permission.PermissionController', '按钮：更新权限信息', '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('149', '/cmsRole/enterRoleAdd', 'com.sites.cms.role.RoleController', '菜单：添加角色', '2020-02-02 20:58:56', '1', '11');
INSERT INTO `permission` VALUES ('150', '/cmsRole/enterRoleEdit', 'com.sites.cms.role.RoleController', '按钮：编辑角色信息', '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('151', '/cmsRole/enterRoleMng', 'com.sites.cms.role.RoleController', '菜单：角色管理', '2020-02-02 20:58:56', '1', '11');
INSERT INTO `permission` VALUES ('152', '/cmsRole/enterRolePerm', 'com.sites.cms.role.RoleController', '按钮：编辑角色权限', '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('153', '/cmsRole/saveRole', 'com.sites.cms.role.RoleController', '按钮：保存角色信息', '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('154', '/cmsRole/updateRole', 'com.sites.cms.role.RoleController', '按钮：更新角色信息', '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('155', '/cmsRole/updateRolePerm', 'com.sites.cms.role.RoleController', '按钮：更新角色权限', '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('156', '/cmsSite/enterSiteAdd', 'com.sites.cms.site.SiteController', '菜单：新增站点', '2020-02-02 20:58:56', '1', '11');
INSERT INTO `permission` VALUES ('157', '/cmsSite/enterSiteEdit', 'com.sites.cms.site.SiteController', '按钮：编辑站点', '2020-02-02 20:58:56', '2', '11');
INSERT INTO `permission` VALUES ('158', '/cmsSite/enterSiteMng', 'com.sites.cms.site.SiteController', '菜单：站点管理', '2020-02-02 20:58:57', '1', '11');
INSERT INTO `permission` VALUES ('159', '/cmsSite/saveSite', 'com.sites.cms.site.SiteController', '按钮：保存站点', '2020-02-02 20:58:57', '2', '11');
INSERT INTO `permission` VALUES ('160', '/cmsSite/setMainSite', 'com.sites.cms.site.SiteController', '按钮：设置当前站点', '2020-02-02 20:58:57', '2', '11');
INSERT INTO `permission` VALUES ('161', '/cmsSite/updateSite', 'com.sites.cms.site.SiteController', '按钮：更新站点', '2020-02-02 20:58:57', '2', '11');
INSERT INTO `permission` VALUES ('162', '/cmsArt/deleteArt', 'com.sites.cms.article.ArticleController', '按钮：删除文章', '2020-02-07 18:38:42', '2', '0');
INSERT INTO `permission` VALUES ('163', '/cmsArt/enterArtAdd', 'com.sites.cms.article.ArticleController', '菜单：新增文章', null, '1', '0');
INSERT INTO `permission` VALUES ('164', '/cmsArt/enterArtDustbin', 'com.sites.cms.article.ArticleController', '菜单：文章回收站', null, '1', '0');
INSERT INTO `permission` VALUES ('165', '/cmsArt/enterArtEdit', 'com.sites.cms.article.ArticleController', '按钮：编辑文章', '2020-02-07 18:38:53', '2', '0');
INSERT INTO `permission` VALUES ('166', '/cmsArt/enterArtMng', 'com.sites.cms.article.ArticleController', '菜单：管理文章', null, '1', '0');
INSERT INTO `permission` VALUES ('167', '/cmsArt/publishArt', 'com.sites.cms.article.ArticleController', '按钮：回收站中的发布文章', '2020-02-07 18:39:00', '2', '0');
INSERT INTO `permission` VALUES ('168', '/cmsArt/saveArt', 'com.sites.cms.article.ArticleController', '按钮：新增文章', '2020-02-07 18:39:03', '2', '0');
INSERT INTO `permission` VALUES ('169', '/cmsArt/updateArt', 'com.sites.cms.article.ArticleController', '按钮：更新文章', '2020-02-07 18:39:08', '2', '0');
INSERT INTO `permission` VALUES ('170', '/cmsColumn/enterColumnAdd', 'com.sites.cms.column.ColumnController', '菜单：新增栏目', null, '1', '0');
INSERT INTO `permission` VALUES ('171', '/cmsColumn/enterColumnEdit', 'com.sites.cms.column.ColumnController', '按钮：编辑栏目', '2020-02-07 18:39:17', '2', '0');
INSERT INTO `permission` VALUES ('172', '/cmsColumn/enterColumnMng', 'com.sites.cms.column.ColumnController', '菜单：栏目管理', null, '1', '0');
INSERT INTO `permission` VALUES ('173', '/cmsColumn/saveColumn', 'com.sites.cms.column.ColumnController', '按钮：保存栏目', '2020-02-07 18:39:24', '2', '0');
INSERT INTO `permission` VALUES ('174', '/cmsColumn/updateColumn', 'com.sites.cms.column.ColumnController', '按钮：更新栏目', '2020-02-07 18:39:29', '2', '0');
INSERT INTO `permission` VALUES ('175', '/cmsAccount/enterPwdEdit', 'com.sites.cms.account.AccountController', '按钮：修改个人密码', '2020-02-07 18:59:54', '2', '0');
INSERT INTO `permission` VALUES ('176', '/cmsAccount/updatePwd', 'com.sites.cms.account.AccountController', '按钮：更新个人密码', '2020-02-07 19:00:00', '2', '0');
INSERT INTO `permission` VALUES ('177', '/cms', 'com.sites.cms.index.IndexController', '菜单：后台首页', '2020-02-09 17:10:03', '2', '12');
INSERT INTO `permission` VALUES ('178', '/cmsAccount/deleteAccount', 'com.sites.cms.account.AccountController', '按钮：删除用户', '2020-02-09 17:10:03', '2', '12');
INSERT INTO `permission` VALUES ('179', '/cmsAccount/enterAccountAdd', 'com.sites.cms.account.AccountController', '菜单：新建用户', '2020-02-09 17:10:03', '1', '12');
INSERT INTO `permission` VALUES ('180', '/cmsAccount/enterAccountEdit', 'com.sites.cms.account.AccountController', '按钮：编辑用户', '2020-02-09 17:10:03', '2', '12');
INSERT INTO `permission` VALUES ('181', '/cmsAccount/enterAccountMng', 'com.sites.cms.account.AccountController', '菜单：用户管理', '2020-02-09 17:10:03', '1', '12');
INSERT INTO `permission` VALUES ('182', '/cmsAccount/enterAccountRole', 'com.sites.cms.account.AccountController', '按钮：用户分配角色', '2020-02-09 17:10:03', '2', '12');
INSERT INTO `permission` VALUES ('183', '/cmsAccount/enterPwdEdit', 'com.sites.cms.account.AccountController', '按钮：修改个人密码', '2020-02-09 17:10:03', '2', '12');
INSERT INTO `permission` VALUES ('184', '/cmsAccount/saveAccount', 'com.sites.cms.account.AccountController', '按钮：保存用户', '2020-02-09 17:10:03', '2', '12');
INSERT INTO `permission` VALUES ('185', '/cmsAccount/updateAccount', 'com.sites.cms.account.AccountController', '按钮：更新用户', '2020-02-09 17:10:03', '2', '12');
INSERT INTO `permission` VALUES ('186', '/cmsAccount/updateAccountRole', 'com.sites.cms.account.AccountController', '按钮：更新用户角色', '2020-02-09 17:10:03', '2', '12');
INSERT INTO `permission` VALUES ('187', '/cmsAccount/updatePwd', 'com.sites.cms.account.AccountController', '按钮：更新个人密码', '2020-02-09 17:10:03', '2', '12');
INSERT INTO `permission` VALUES ('188', '/cmsArt/deleteArt', 'com.sites.cms.article.ArticleController', '按钮：删除文章', '2020-02-09 17:10:03', '2', '12');
INSERT INTO `permission` VALUES ('189', '/cmsArt/enterArtAdd', 'com.sites.cms.article.ArticleController', '菜单：新增文章', '2020-02-09 17:10:03', '1', '12');
INSERT INTO `permission` VALUES ('190', '/cmsArt/enterArtDustbin', 'com.sites.cms.article.ArticleController', '菜单：文章回收站', '2020-02-09 17:10:03', '1', '12');
INSERT INTO `permission` VALUES ('191', '/cmsArt/enterArtEdit', 'com.sites.cms.article.ArticleController', '按钮：编辑文章', '2020-02-09 17:10:03', '2', '12');
INSERT INTO `permission` VALUES ('192', '/cmsArt/enterArtMng', 'com.sites.cms.article.ArticleController', '菜单：管理文章', '2020-02-09 17:10:03', '1', '12');
INSERT INTO `permission` VALUES ('193', '/cmsArt/publishArt', 'com.sites.cms.article.ArticleController', '按钮：回收站中的发布文章', '2020-02-09 17:10:03', '2', '12');
INSERT INTO `permission` VALUES ('194', '/cmsArt/saveArt', 'com.sites.cms.article.ArticleController', '按钮：新增文章', '2020-02-09 17:10:03', '2', '12');
INSERT INTO `permission` VALUES ('195', '/cmsArt/updateArt', 'com.sites.cms.article.ArticleController', '按钮：更新文章', '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('196', '/cmsColumn/enterColumnAdd', 'com.sites.cms.column.ColumnController', '菜单：新增栏目', '2020-02-09 17:10:04', '1', '12');
INSERT INTO `permission` VALUES ('197', '/cmsColumn/enterColumnEdit', 'com.sites.cms.column.ColumnController', '按钮：编辑栏目', '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('198', '/cmsColumn/enterColumnMng', 'com.sites.cms.column.ColumnController', '菜单：栏目管理', '2020-02-09 17:10:04', '1', '12');
INSERT INTO `permission` VALUES ('199', '/cmsColumn/saveColumn', 'com.sites.cms.column.ColumnController', '按钮：保存栏目', '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('200', '/cmsColumn/updateColumn', 'com.sites.cms.column.ColumnController', '按钮：更新栏目', '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('201', '/cmsLogin', 'com.sites.cms.login.LoginController', null, '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('202', '/cmsLogin/createCaptcha', 'com.sites.cms.login.LoginController', null, '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('203', '/cmsLogin/doLogin', 'com.sites.cms.login.LoginController', null, '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('204', '/cmsLogin/logOut', 'com.sites.cms.login.LoginController', null, '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('205', '/cmsPerm/deletePerm', 'com.sites.cms.permission.PermissionController', '按钮：删除权限信息', '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('206', '/cmsPerm/enterPermEdit', 'com.sites.cms.permission.PermissionController', '按钮：编辑权限信息', '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('207', '/cmsPerm/enterPermMng', 'com.sites.cms.permission.PermissionController', '菜单：权限管理', '2020-02-09 17:10:04', '1', '12');
INSERT INTO `permission` VALUES ('208', '/cmsPerm/reloadPerm', 'com.sites.cms.permission.PermissionController', '按钮：一键同步权限列表', '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('209', '/cmsPerm/updatePerm', 'com.sites.cms.permission.PermissionController', '按钮：更新权限信息', '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('210', '/cmsRole/enterRoleAdd', 'com.sites.cms.role.RoleController', '菜单：添加角色', '2020-02-09 17:10:04', '1', '12');
INSERT INTO `permission` VALUES ('211', '/cmsRole/enterRoleEdit', 'com.sites.cms.role.RoleController', '按钮：编辑角色信息', '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('212', '/cmsRole/enterRoleMng', 'com.sites.cms.role.RoleController', '菜单：角色管理', '2020-02-09 17:10:04', '1', '12');
INSERT INTO `permission` VALUES ('213', '/cmsRole/enterRolePerm', 'com.sites.cms.role.RoleController', '按钮：编辑角色权限', '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('214', '/cmsRole/saveRole', 'com.sites.cms.role.RoleController', '按钮：保存角色信息', '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('215', '/cmsRole/updateRole', 'com.sites.cms.role.RoleController', '按钮：更新角色信息', '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('216', '/cmsRole/updateRolePerm', 'com.sites.cms.role.RoleController', '按钮：更新角色权限', '2020-02-09 17:10:04', '2', '12');
INSERT INTO `permission` VALUES ('217', '/cmsSite/enterSiteAdd', 'com.sites.cms.site.SiteController', '菜单：新增站点', '2020-02-09 17:10:04', '1', '12');
INSERT INTO `permission` VALUES ('218', '/cmsSite/enterSiteEdit', 'com.sites.cms.site.SiteController', '按钮：编辑站点', '2020-02-09 17:10:05', '2', '12');
INSERT INTO `permission` VALUES ('219', '/cmsSite/enterSiteMng', 'com.sites.cms.site.SiteController', '菜单：站点管理', '2020-02-09 17:10:05', '1', '12');
INSERT INTO `permission` VALUES ('220', '/cmsSite/saveSite', 'com.sites.cms.site.SiteController', '按钮：保存站点', '2020-02-09 17:10:05', '2', '12');
INSERT INTO `permission` VALUES ('221', '/cmsSite/setMainSite', 'com.sites.cms.site.SiteController', '按钮：设置当前站点', '2020-02-09 17:10:05', '2', '12');
INSERT INTO `permission` VALUES ('222', '/cmsSite/updateSite', 'com.sites.cms.site.SiteController', '按钮：更新站点', '2020-02-09 17:10:05', '2', '12');
INSERT INTO `permission` VALUES ('223', '/cms', 'com.sites.cms.index.IndexController', '菜单：后台首页', '2020-02-27 22:26:27', '2', '13');
INSERT INTO `permission` VALUES ('224', '/cmsAccount/deleteAccount', 'com.sites.cms.account.AccountController', '按钮：删除用户', '2020-02-27 22:26:27', '2', '13');
INSERT INTO `permission` VALUES ('225', '/cmsAccount/enterAccountAdd', 'com.sites.cms.account.AccountController', '菜单：新建用户', '2020-02-27 22:26:27', '1', '13');
INSERT INTO `permission` VALUES ('226', '/cmsAccount/enterAccountEdit', 'com.sites.cms.account.AccountController', '按钮：编辑用户', '2020-02-27 22:26:27', '2', '13');
INSERT INTO `permission` VALUES ('227', '/cmsAccount/enterAccountMng', 'com.sites.cms.account.AccountController', '菜单：用户管理', '2020-02-27 22:26:27', '1', '13');
INSERT INTO `permission` VALUES ('228', '/cmsAccount/enterAccountRole', 'com.sites.cms.account.AccountController', '按钮：用户分配角色', '2020-02-27 22:26:27', '2', '13');
INSERT INTO `permission` VALUES ('229', '/cmsAccount/enterPwdEdit', 'com.sites.cms.account.AccountController', '按钮：修改个人密码', '2020-02-27 22:26:27', '2', '13');
INSERT INTO `permission` VALUES ('230', '/cmsAccount/saveAccount', 'com.sites.cms.account.AccountController', '按钮：保存用户', '2020-02-27 22:26:27', '2', '13');
INSERT INTO `permission` VALUES ('231', '/cmsAccount/updateAccount', 'com.sites.cms.account.AccountController', '按钮：更新用户', '2020-02-27 22:26:27', '2', '13');
INSERT INTO `permission` VALUES ('232', '/cmsAccount/updateAccountRole', 'com.sites.cms.account.AccountController', '按钮：更新用户角色', '2020-02-27 22:26:27', '2', '13');
INSERT INTO `permission` VALUES ('233', '/cmsAccount/updatePwd', 'com.sites.cms.account.AccountController', '按钮：更新个人密码', '2020-02-27 22:26:27', '2', '13');
INSERT INTO `permission` VALUES ('234', '/cmsArt/deleteArt', 'com.sites.cms.article.ArticleController', '按钮：删除文章', '2020-02-27 22:26:27', '2', '13');
INSERT INTO `permission` VALUES ('235', '/cmsArt/enterArtAdd', 'com.sites.cms.article.ArticleController', '菜单：新增文章', '2020-02-27 22:26:27', '1', '13');
INSERT INTO `permission` VALUES ('236', '/cmsArt/enterArtDustbin', 'com.sites.cms.article.ArticleController', '菜单：文章回收站', '2020-02-27 22:26:27', '1', '13');
INSERT INTO `permission` VALUES ('237', '/cmsArt/enterArtEdit', 'com.sites.cms.article.ArticleController', '按钮：编辑文章', '2020-02-27 22:26:27', '2', '13');
INSERT INTO `permission` VALUES ('238', '/cmsArt/enterArtMng', 'com.sites.cms.article.ArticleController', '菜单：管理文章', '2020-02-27 22:26:27', '1', '13');
INSERT INTO `permission` VALUES ('239', '/cmsArt/publishArt', 'com.sites.cms.article.ArticleController', '按钮：回收站中的发布文章', '2020-02-27 22:26:27', '2', '13');
INSERT INTO `permission` VALUES ('240', '/cmsArt/saveArt', 'com.sites.cms.article.ArticleController', '按钮：新增文章', '2020-02-27 22:26:27', '2', '13');
INSERT INTO `permission` VALUES ('241', '/cmsArt/updateArt', 'com.sites.cms.article.ArticleController', '按钮：更新文章', '2020-02-27 22:26:27', '2', '13');
INSERT INTO `permission` VALUES ('242', '/cmsColumn/enterColumnAdd', 'com.sites.cms.column.ColumnController', '菜单：新增栏目', '2020-02-27 22:26:28', '1', '13');
INSERT INTO `permission` VALUES ('243', '/cmsColumn/enterColumnEdit', 'com.sites.cms.column.ColumnController', '按钮：编辑栏目', '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('244', '/cmsColumn/enterColumnMng', 'com.sites.cms.column.ColumnController', '菜单：栏目管理', '2020-02-27 22:26:28', '1', '13');
INSERT INTO `permission` VALUES ('245', '/cmsColumn/saveColumn', 'com.sites.cms.column.ColumnController', '按钮：保存栏目', '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('246', '/cmsColumn/updateColumn', 'com.sites.cms.column.ColumnController', '按钮：更新栏目', '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('247', '/cmsLogin', 'com.sites.cms.login.LoginController', null, '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('248', '/cmsLogin/createCaptcha', 'com.sites.cms.login.LoginController', null, '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('249', '/cmsLogin/doLogin', 'com.sites.cms.login.LoginController', null, '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('250', '/cmsLogin/logOut', 'com.sites.cms.login.LoginController', null, '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('251', '/cmsPerm/deletePerm', 'com.sites.cms.permission.PermissionController', '按钮：删除权限信息', '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('252', '/cmsPerm/enterPermEdit', 'com.sites.cms.permission.PermissionController', '按钮：编辑权限信息', '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('253', '/cmsPerm/enterPermMng', 'com.sites.cms.permission.PermissionController', '菜单：权限管理', '2020-02-27 22:26:28', '1', '13');
INSERT INTO `permission` VALUES ('254', '/cmsPerm/reloadPerm', 'com.sites.cms.permission.PermissionController', '按钮：一键同步权限列表', '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('255', '/cmsPerm/updatePerm', 'com.sites.cms.permission.PermissionController', '按钮：更新权限信息', '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('256', '/cmsRole/enterRoleAdd', 'com.sites.cms.role.RoleController', '菜单：添加角色', '2020-02-27 22:26:28', '1', '13');
INSERT INTO `permission` VALUES ('257', '/cmsRole/enterRoleEdit', 'com.sites.cms.role.RoleController', '按钮：编辑角色信息', '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('258', '/cmsRole/enterRoleMng', 'com.sites.cms.role.RoleController', '菜单：角色管理', '2020-02-27 22:26:28', '1', '13');
INSERT INTO `permission` VALUES ('259', '/cmsRole/enterRolePerm', 'com.sites.cms.role.RoleController', '按钮：编辑角色权限', '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('260', '/cmsRole/saveRole', 'com.sites.cms.role.RoleController', '按钮：保存角色信息', '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('261', '/cmsRole/updateRole', 'com.sites.cms.role.RoleController', '按钮：更新角色信息', '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('262', '/cmsRole/updateRolePerm', 'com.sites.cms.role.RoleController', '按钮：更新角色权限', '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('263', '/cmsSite/enterSiteAdd', 'com.sites.cms.site.SiteController', '菜单：新增站点', '2020-02-27 22:26:28', '1', '13');
INSERT INTO `permission` VALUES ('264', '/cmsSite/enterSiteEdit', 'com.sites.cms.site.SiteController', '按钮：编辑站点', '2020-02-27 22:26:28', '2', '13');
INSERT INTO `permission` VALUES ('265', '/cmsSite/enterSiteMng', 'com.sites.cms.site.SiteController', '菜单：站点管理', '2020-02-27 22:26:29', '1', '13');
INSERT INTO `permission` VALUES ('266', '/cmsSite/saveSite', 'com.sites.cms.site.SiteController', '按钮：保存站点', '2020-02-27 22:26:29', '2', '13');
INSERT INTO `permission` VALUES ('267', '/cmsSite/setMainSite', 'com.sites.cms.site.SiteController', '按钮：设置当前站点', '2020-02-27 22:26:29', '2', '13');
INSERT INTO `permission` VALUES ('268', '/cmsSite/updateSite', 'com.sites.cms.site.SiteController', '按钮：更新站点', '2020-02-27 22:26:29', '2', '13');
INSERT INTO `permission` VALUES ('269', '/cmsArt/enterPicArtAdd', 'com.sites.cms.article.ArticleController', '菜单：新增图文', null, '1', '0');
INSERT INTO `permission` VALUES ('270', '/cmsArt/savePicArt', 'com.sites.cms.article.ArticleController', '按钮：保存图文', '2020-03-08 20:50:16', '2', '0');
INSERT INTO `permission` VALUES ('271', '/cmsColumn/activateCol', 'com.sites.cms.column.ColumnController', '按钮：启用/激活栏目', '2020-03-08 20:50:23', '2', '0');
INSERT INTO `permission` VALUES ('272', '/cmsColumn/delColumn', 'com.sites.cms.column.ColumnController', '按钮：删除栏目', '2020-03-08 20:50:28', '2', '0');
INSERT INTO `permission` VALUES ('273', '/cmsArt/enterVideoArtAdd', 'com.sites.cms.article.ArticleController', '菜单：新增视频', null, '1', '0');
INSERT INTO `permission` VALUES ('274', '/cmsArt/saveVideoArt', 'com.sites.cms.article.ArticleController', '按钮：保存视频', '2020-09-08 12:01:10', '2', '0');
INSERT INTO `permission` VALUES ('275', '/cmsLog/enterCdsLogView', 'com.sites.cms.accessLog.AccessLogController', '菜单：用户访问日志', null, '1', '0');
INSERT INTO `permission` VALUES ('276', '/cmsLog/enterCmsLogView', 'com.sites.cms.accessLog.AccessLogController', '菜单：管理端访问日志', null, '1', '0');
INSERT INTO `permission` VALUES ('277', '/cmsSite/enterCurrentSiteEdit', 'com.sites.cms.site.SiteController', '菜单：系统设置', null, '1', '0');
INSERT INTO `permission` VALUES ('278', '/cmsSite/updateCurrentSite', 'com.sites.cms.site.SiteController', '按钮：更新系统设置信息', null, '1', '0');
INSERT INTO `permission` VALUES ('279', '/cms', 'com.sites.cms.index.IndexController', '菜单：后台首页', '2020-09-21 20:26:08', '2', '13');
INSERT INTO `permission` VALUES ('280', '/cmsAccount/deleteAccount', 'com.sites.cms.account.AccountController', '按钮：删除用户', '2020-09-21 20:26:08', '2', '13');
INSERT INTO `permission` VALUES ('281', '/cmsAccount/enterAccountAdd', 'com.sites.cms.account.AccountController', '菜单：新建用户', '2020-09-21 20:26:08', '1', '13');
INSERT INTO `permission` VALUES ('282', '/cmsAccount/enterAccountEdit', 'com.sites.cms.account.AccountController', '按钮：编辑用户', '2020-09-21 20:26:08', '2', '13');
INSERT INTO `permission` VALUES ('283', '/cmsAccount/enterAccountMng', 'com.sites.cms.account.AccountController', '菜单：用户管理', '2020-09-21 20:26:08', '1', '13');
INSERT INTO `permission` VALUES ('284', '/cmsAccount/enterAccountRole', 'com.sites.cms.account.AccountController', '按钮：用户分配角色', '2020-09-21 20:26:08', '2', '13');
INSERT INTO `permission` VALUES ('285', '/cmsAccount/enterPwdEdit', 'com.sites.cms.account.AccountController', '按钮：修改个人密码', '2020-09-21 20:26:08', '2', '13');
INSERT INTO `permission` VALUES ('286', '/cmsAccount/saveAccount', 'com.sites.cms.account.AccountController', '按钮：保存用户', '2020-09-21 20:26:08', '2', '13');
INSERT INTO `permission` VALUES ('287', '/cmsAccount/updateAccount', 'com.sites.cms.account.AccountController', '按钮：更新用户', '2020-09-21 20:26:08', '2', '13');
INSERT INTO `permission` VALUES ('288', '/cmsAccount/updateAccountRole', 'com.sites.cms.account.AccountController', '按钮：更新用户角色', '2020-09-21 20:26:09', '2', '13');
INSERT INTO `permission` VALUES ('289', '/cmsAccount/updatePwd', 'com.sites.cms.account.AccountController', '按钮：更新个人密码', '2020-09-21 20:26:09', '2', '13');
INSERT INTO `permission` VALUES ('290', '/cmsArt/deleteArt', 'com.sites.cms.article.ArticleController', '按钮：删除文章', '2020-09-21 20:26:09', '2', '13');
INSERT INTO `permission` VALUES ('291', '/cmsArt/enterArtAdd', 'com.sites.cms.article.ArticleController', '菜单：新增文章', '2020-09-21 20:26:09', '1', '13');
INSERT INTO `permission` VALUES ('292', '/cmsArt/enterArtDustbin', 'com.sites.cms.article.ArticleController', '菜单：文章回收站', '2020-09-21 20:26:09', '1', '13');
INSERT INTO `permission` VALUES ('293', '/cmsArt/enterArtEdit', 'com.sites.cms.article.ArticleController', '按钮：编辑文章', '2020-09-21 20:26:09', '2', '13');
INSERT INTO `permission` VALUES ('294', '/cmsArt/enterArtMng', 'com.sites.cms.article.ArticleController', '菜单：管理文章', '2020-09-21 20:26:09', '1', '13');
INSERT INTO `permission` VALUES ('295', '/cmsArt/enterPicArtAdd', 'com.sites.cms.article.ArticleController', '菜单：新增图文', '2020-09-21 20:26:09', '1', '13');
INSERT INTO `permission` VALUES ('296', '/cmsArt/enterVideoArtAdd', 'com.sites.cms.article.ArticleController', '菜单：新增视频', '2020-09-21 20:26:09', '1', '13');
INSERT INTO `permission` VALUES ('297', '/cmsArt/publishArt', 'com.sites.cms.article.ArticleController', '按钮：回收站中的发布文章', '2020-09-21 20:26:09', '2', '13');
INSERT INTO `permission` VALUES ('298', '/cmsArt/saveArt', 'com.sites.cms.article.ArticleController', '按钮：新增文章', '2020-09-21 20:26:09', '2', '13');
INSERT INTO `permission` VALUES ('299', '/cmsArt/savePicArt', 'com.sites.cms.article.ArticleController', '按钮：保存图文', '2020-09-21 20:26:09', '2', '13');
INSERT INTO `permission` VALUES ('300', '/cmsArt/saveVideoArt', 'com.sites.cms.article.ArticleController', '按钮：保存视频', '2020-09-21 20:26:09', '2', '13');
INSERT INTO `permission` VALUES ('301', '/cmsArt/updateArt', 'com.sites.cms.article.ArticleController', '按钮：更新文章', '2020-09-21 20:26:09', '2', '13');
INSERT INTO `permission` VALUES ('302', '/cmsColumn/activateCol', 'com.sites.cms.column.ColumnController', '按钮：启用/激活栏目', '2020-09-21 20:26:09', '2', '13');
INSERT INTO `permission` VALUES ('303', '/cmsColumn/delColumn', 'com.sites.cms.column.ColumnController', '按钮：删除栏目', '2020-09-21 20:26:09', '2', '13');
INSERT INTO `permission` VALUES ('304', '/cmsColumn/enterColumnAdd', 'com.sites.cms.column.ColumnController', '菜单：新增栏目', '2020-09-21 20:26:09', '1', '13');
INSERT INTO `permission` VALUES ('305', '/cmsColumn/enterColumnEdit', 'com.sites.cms.column.ColumnController', '按钮：编辑栏目', '2020-09-21 20:26:09', '2', '13');
INSERT INTO `permission` VALUES ('306', '/cmsColumn/enterColumnMng', 'com.sites.cms.column.ColumnController', '菜单：栏目管理', '2020-09-21 20:26:09', '1', '13');
INSERT INTO `permission` VALUES ('307', '/cmsColumn/saveColumn', 'com.sites.cms.column.ColumnController', '按钮：保存栏目', '2020-09-21 20:26:09', '2', '13');
INSERT INTO `permission` VALUES ('308', '/cmsColumn/updateColumn', 'com.sites.cms.column.ColumnController', '按钮：更新栏目', '2020-09-21 20:26:09', '2', '13');
INSERT INTO `permission` VALUES ('309', '/cmsLog/enterCdsLogView', 'com.sites.cms.accessLog.AccessLogController', '菜单：用户访问日志', '2020-09-21 20:26:09', '1', '13');
INSERT INTO `permission` VALUES ('310', '/cmsLog/enterCmsLogView', 'com.sites.cms.accessLog.AccessLogController', '菜单：管理端访问日志', '2020-09-21 20:26:09', '1', '13');
INSERT INTO `permission` VALUES ('311', '/cmsLogin', 'com.sites.cms.login.LoginController', null, '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('312', '/cmsLogin/createCaptcha', 'com.sites.cms.login.LoginController', null, '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('313', '/cmsLogin/doLogin', 'com.sites.cms.login.LoginController', null, '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('314', '/cmsLogin/logOut', 'com.sites.cms.login.LoginController', null, '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('315', '/cmsPerm/deletePerm', 'com.sites.cms.permission.PermissionController', '按钮：删除权限信息', '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('316', '/cmsPerm/enterPermEdit', 'com.sites.cms.permission.PermissionController', '按钮：编辑权限信息', '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('317', '/cmsPerm/enterPermMng', 'com.sites.cms.permission.PermissionController', '菜单：权限管理', '2020-09-21 20:26:10', '1', '13');
INSERT INTO `permission` VALUES ('318', '/cmsPerm/reloadPerm', 'com.sites.cms.permission.PermissionController', '按钮：一键同步权限列表', '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('319', '/cmsPerm/updatePerm', 'com.sites.cms.permission.PermissionController', '按钮：更新权限信息', '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('320', '/cmsRole/enterRoleAdd', 'com.sites.cms.role.RoleController', '菜单：添加角色', '2020-09-21 20:26:10', '1', '13');
INSERT INTO `permission` VALUES ('321', '/cmsRole/enterRoleEdit', 'com.sites.cms.role.RoleController', '按钮：编辑角色信息', '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('322', '/cmsRole/enterRoleMng', 'com.sites.cms.role.RoleController', '菜单：角色管理', '2020-09-21 20:26:10', '1', '13');
INSERT INTO `permission` VALUES ('323', '/cmsRole/enterRolePerm', 'com.sites.cms.role.RoleController', '按钮：编辑角色权限', '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('324', '/cmsRole/saveRole', 'com.sites.cms.role.RoleController', '按钮：保存角色信息', '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('325', '/cmsRole/updateRole', 'com.sites.cms.role.RoleController', '按钮：更新角色信息', '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('326', '/cmsRole/updateRolePerm', 'com.sites.cms.role.RoleController', '按钮：更新角色权限', '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('327', '/cmsSite/enterCurrentSiteEdit', 'com.sites.cms.site.SiteController', '菜单：系统设置', '2020-09-21 20:26:10', '1', '13');
INSERT INTO `permission` VALUES ('328', '/cmsSite/enterSiteAdd', 'com.sites.cms.site.SiteController', '菜单：新增站点', '2020-09-21 20:26:10', '1', '13');
INSERT INTO `permission` VALUES ('329', '/cmsSite/enterSiteEdit', 'com.sites.cms.site.SiteController', '按钮：编辑站点', '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('330', '/cmsSite/enterSiteMng', 'com.sites.cms.site.SiteController', '菜单：站点管理', '2020-09-21 20:26:10', '1', '13');
INSERT INTO `permission` VALUES ('331', '/cmsSite/saveSite', 'com.sites.cms.site.SiteController', '按钮：保存站点', '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('332', '/cmsSite/setMainSite', 'com.sites.cms.site.SiteController', '按钮：设置当前站点', '2020-09-21 20:26:10', '2', '13');
INSERT INTO `permission` VALUES ('333', '/cmsSite/updateCurrentSite', 'com.sites.cms.site.SiteController', '按钮：更新系统设置信息', '2020-09-21 20:26:11', '1', '13');
INSERT INTO `permission` VALUES ('334', '/cmsSite/updateSite', 'com.sites.cms.site.SiteController', '按钮：更新站点', '2020-09-21 20:26:11', '2', '13');
INSERT INTO `permission` VALUES ('335', '/cms', 'com.sites.cms.index.IndexController', '菜单：后台首页', '2020-09-21 20:55:24', '2', '14');
INSERT INTO `permission` VALUES ('336', '/cmsAccount/deleteAccount', 'com.sites.cms.account.AccountController', '按钮：删除用户', '2020-09-21 20:55:24', '2', '14');
INSERT INTO `permission` VALUES ('337', '/cmsAccount/enterAccountAdd', 'com.sites.cms.account.AccountController', '菜单：新建用户', '2020-09-21 20:55:24', '1', '14');
INSERT INTO `permission` VALUES ('338', '/cmsAccount/enterAccountEdit', 'com.sites.cms.account.AccountController', '按钮：编辑用户', '2020-09-21 20:55:24', '2', '14');
INSERT INTO `permission` VALUES ('339', '/cmsAccount/enterAccountMng', 'com.sites.cms.account.AccountController', '菜单：用户管理', '2020-09-21 20:55:24', '1', '14');
INSERT INTO `permission` VALUES ('340', '/cmsAccount/enterAccountRole', 'com.sites.cms.account.AccountController', '按钮：用户分配角色', '2020-09-21 20:55:24', '2', '14');
INSERT INTO `permission` VALUES ('341', '/cmsAccount/enterPwdEdit', 'com.sites.cms.account.AccountController', '按钮：修改个人密码', '2020-09-21 20:55:24', '2', '14');
INSERT INTO `permission` VALUES ('342', '/cmsAccount/saveAccount', 'com.sites.cms.account.AccountController', '按钮：保存用户', '2020-09-21 20:55:24', '2', '14');
INSERT INTO `permission` VALUES ('343', '/cmsAccount/updateAccount', 'com.sites.cms.account.AccountController', '按钮：更新用户', '2020-09-21 20:55:24', '2', '14');
INSERT INTO `permission` VALUES ('344', '/cmsAccount/updateAccountRole', 'com.sites.cms.account.AccountController', '按钮：更新用户角色', '2020-09-21 20:55:24', '2', '14');
INSERT INTO `permission` VALUES ('345', '/cmsAccount/updatePwd', 'com.sites.cms.account.AccountController', '按钮：更新个人密码', '2020-09-21 20:55:24', '2', '14');
INSERT INTO `permission` VALUES ('346', '/cmsArt/deleteArt', 'com.sites.cms.article.ArticleController', '按钮：删除文章', '2020-09-21 20:55:24', '2', '14');
INSERT INTO `permission` VALUES ('347', '/cmsArt/enterArtAdd', 'com.sites.cms.article.ArticleController', '菜单：新增文章', '2020-09-21 20:55:24', '1', '14');
INSERT INTO `permission` VALUES ('348', '/cmsArt/enterArtDustbin', 'com.sites.cms.article.ArticleController', '菜单：文章回收站', '2020-09-21 20:55:24', '1', '14');
INSERT INTO `permission` VALUES ('349', '/cmsArt/enterArtEdit', 'com.sites.cms.article.ArticleController', '按钮：编辑文章', '2020-09-21 20:55:24', '2', '14');
INSERT INTO `permission` VALUES ('350', '/cmsArt/enterArtMng', 'com.sites.cms.article.ArticleController', '菜单：管理文章', '2020-09-21 20:55:24', '1', '14');
INSERT INTO `permission` VALUES ('351', '/cmsArt/enterPicArtAdd', 'com.sites.cms.article.ArticleController', '菜单：新增图文', '2020-09-21 20:55:24', '1', '14');
INSERT INTO `permission` VALUES ('352', '/cmsArt/enterVideoArtAdd', 'com.sites.cms.article.ArticleController', '菜单：新增视频', '2020-09-21 20:55:24', '1', '14');
INSERT INTO `permission` VALUES ('353', '/cmsArt/publishArt', 'com.sites.cms.article.ArticleController', '按钮：回收站中的发布文章', '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('354', '/cmsArt/saveArt', 'com.sites.cms.article.ArticleController', '按钮：新增文章', '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('355', '/cmsArt/savePicArt', 'com.sites.cms.article.ArticleController', '按钮：保存图文', '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('356', '/cmsArt/saveVideoArt', 'com.sites.cms.article.ArticleController', '按钮：保存视频', '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('357', '/cmsArt/updateArt', 'com.sites.cms.article.ArticleController', '按钮：更新文章', '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('358', '/cmsColumn/activateCol', 'com.sites.cms.column.ColumnController', '按钮：启用/激活栏目', '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('359', '/cmsColumn/delColumn', 'com.sites.cms.column.ColumnController', '按钮：删除栏目', '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('360', '/cmsColumn/enterColumnAdd', 'com.sites.cms.column.ColumnController', '菜单：新增栏目', '2020-09-21 20:55:25', '1', '14');
INSERT INTO `permission` VALUES ('361', '/cmsColumn/enterColumnEdit', 'com.sites.cms.column.ColumnController', '按钮：编辑栏目', '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('362', '/cmsColumn/enterColumnMng', 'com.sites.cms.column.ColumnController', '菜单：栏目管理', '2020-09-21 20:55:25', '1', '14');
INSERT INTO `permission` VALUES ('363', '/cmsColumn/saveColumn', 'com.sites.cms.column.ColumnController', '按钮：保存栏目', '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('364', '/cmsColumn/updateColumn', 'com.sites.cms.column.ColumnController', '按钮：更新栏目', '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('365', '/cmsLog/enterCdsLogView', 'com.sites.cms.accessLog.AccessLogController', '菜单：用户访问日志', '2020-09-21 20:55:25', '1', '14');
INSERT INTO `permission` VALUES ('366', '/cmsLog/enterCmsLogView', 'com.sites.cms.accessLog.AccessLogController', '菜单：管理端访问日志', '2020-09-21 20:55:25', '1', '14');
INSERT INTO `permission` VALUES ('367', '/cmsLogin', 'com.sites.cms.login.LoginController', null, '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('368', '/cmsLogin/createCaptcha', 'com.sites.cms.login.LoginController', null, '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('369', '/cmsLogin/doLogin', 'com.sites.cms.login.LoginController', null, '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('370', '/cmsLogin/logOut', 'com.sites.cms.login.LoginController', null, '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('371', '/cmsPerm/deletePerm', 'com.sites.cms.permission.PermissionController', '按钮：删除权限信息', '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('372', '/cmsPerm/enterPermEdit', 'com.sites.cms.permission.PermissionController', '按钮：编辑权限信息', '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('373', '/cmsPerm/enterPermMng', 'com.sites.cms.permission.PermissionController', '菜单：权限管理', '2020-09-21 20:55:25', '1', '14');
INSERT INTO `permission` VALUES ('374', '/cmsPerm/reloadPerm', 'com.sites.cms.permission.PermissionController', '按钮：一键同步权限列表', '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('375', '/cmsPerm/updatePerm', 'com.sites.cms.permission.PermissionController', '按钮：更新权限信息', '2020-09-21 20:55:25', '2', '14');
INSERT INTO `permission` VALUES ('376', '/cmsRole/enterRoleAdd', 'com.sites.cms.role.RoleController', '菜单：添加角色', '2020-09-21 20:55:26', '1', '14');
INSERT INTO `permission` VALUES ('377', '/cmsRole/enterRoleEdit', 'com.sites.cms.role.RoleController', '按钮：编辑角色信息', '2020-09-21 20:55:26', '2', '14');
INSERT INTO `permission` VALUES ('378', '/cmsRole/enterRoleMng', 'com.sites.cms.role.RoleController', '菜单：角色管理', '2020-09-21 20:55:26', '1', '14');
INSERT INTO `permission` VALUES ('379', '/cmsRole/enterRolePerm', 'com.sites.cms.role.RoleController', '按钮：编辑角色权限', '2020-09-21 20:55:26', '2', '14');
INSERT INTO `permission` VALUES ('380', '/cmsRole/saveRole', 'com.sites.cms.role.RoleController', '按钮：保存角色信息', '2020-09-21 20:55:26', '2', '14');
INSERT INTO `permission` VALUES ('381', '/cmsRole/updateRole', 'com.sites.cms.role.RoleController', '按钮：更新角色信息', '2020-09-21 20:55:26', '2', '14');
INSERT INTO `permission` VALUES ('382', '/cmsRole/updateRolePerm', 'com.sites.cms.role.RoleController', '按钮：更新角色权限', '2020-09-21 20:55:26', '2', '14');
INSERT INTO `permission` VALUES ('383', '/cmsSite/enterCurrentSiteEdit', 'com.sites.cms.site.SiteController', '菜单：系统设置', '2020-09-21 20:55:26', '1', '14');
INSERT INTO `permission` VALUES ('384', '/cmsSite/enterSiteAdd', 'com.sites.cms.site.SiteController', '菜单：新增站点', '2020-09-21 20:55:26', '1', '14');
INSERT INTO `permission` VALUES ('385', '/cmsSite/enterSiteEdit', 'com.sites.cms.site.SiteController', '按钮：编辑站点', '2020-09-21 20:55:26', '2', '14');
INSERT INTO `permission` VALUES ('386', '/cmsSite/enterSiteMng', 'com.sites.cms.site.SiteController', '菜单：站点管理', '2020-09-21 20:55:26', '1', '14');
INSERT INTO `permission` VALUES ('387', '/cmsSite/saveSite', 'com.sites.cms.site.SiteController', '按钮：保存站点', '2020-09-21 20:55:26', '2', '14');
INSERT INTO `permission` VALUES ('388', '/cmsSite/setMainSite', 'com.sites.cms.site.SiteController', '按钮：设置当前站点', '2020-09-21 20:55:26', '2', '14');
INSERT INTO `permission` VALUES ('389', '/cmsSite/updateCurrentSite', 'com.sites.cms.site.SiteController', '按钮：更新系统设置信息', '2020-09-21 20:55:26', '1', '14');
INSERT INTO `permission` VALUES ('390', '/cmsSite/updateSite', 'com.sites.cms.site.SiteController', '按钮：更新站点', '2020-09-21 20:55:26', '2', '14');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(128) default NULL COMMENT '角色名',
  `remark` varchar(512) default NULL COMMENT '备注',
  `createTime` datetime default NULL COMMENT '创建时间',
  `updateTime` datetime default NULL COMMENT '更新时间',
  `siteId` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '权限管理员', '负责权限管理', '2020-01-28 21:15:56', '2020-01-31 21:00:56', '0');
INSERT INTO `role` VALUES ('4', '用户管理员', '新建、管理用户', '2020-01-29 21:47:07', '2020-02-02 20:52:48', '0');
INSERT INTO `role` VALUES ('5', '角色管理员', '新建、管理角色', '2020-01-29 21:55:43', '2020-02-02 20:52:35', '0');
INSERT INTO `role` VALUES ('6', '权限管理员', '负责权限管理', '2020-02-02 17:58:01', '2020-01-31 21:00:56', '9');
INSERT INTO `role` VALUES ('8', '角色管理员', '管理角色', '2020-02-02 17:58:02', null, '9');
INSERT INTO `role` VALUES ('9', '用户管理员', '管理用户', '2020-02-02 17:58:02', null, '9');
INSERT INTO `role` VALUES ('16', '站点管理员', '新建、管理站点', '2020-02-02 20:53:20', null, '0');
INSERT INTO `role` VALUES ('17', '新增管理员', '新增用户、角色、站点', '2020-02-02 20:54:32', null, '0');
INSERT INTO `role` VALUES ('18', '用户管理员', '新建、管理用户', '2020-02-02 20:58:57', '2020-02-02 20:52:48', '11');
INSERT INTO `role` VALUES ('19', '角色管理员', '新建、管理角色', '2020-02-02 20:58:57', '2020-02-02 20:52:35', '11');
INSERT INTO `role` VALUES ('20', '权限管理员', '负责权限管理', '2020-02-02 20:58:57', '2020-01-31 21:00:56', '11');
INSERT INTO `role` VALUES ('21', '新增管理员', '新增用户、角色、站点', '2020-02-02 20:58:57', null, '11');
INSERT INTO `role` VALUES ('22', '站点管理员', '新建、管理站点', '2020-02-02 20:58:57', null, '11');
INSERT INTO `role` VALUES ('23', '文章管理员', '拥有所有的文章管理权限', '2020-02-07 18:40:03', null, '0');
INSERT INTO `role` VALUES ('24', '文章新增管理员', '拥有文章新增权限', '2020-02-07 18:40:28', null, '0');
INSERT INTO `role` VALUES ('25', '用户管理员', '新建、管理用户', '2020-02-09 17:10:05', '2020-02-02 20:52:48', '12');
INSERT INTO `role` VALUES ('26', '角色管理员', '新建、管理角色', '2020-02-09 17:10:05', '2020-02-02 20:52:35', '12');
INSERT INTO `role` VALUES ('27', '权限管理员', '负责权限管理', '2020-02-09 17:10:05', '2020-01-31 21:00:56', '12');
INSERT INTO `role` VALUES ('28', '文章新增管理员', '拥有文章新增权限', '2020-02-09 17:10:05', null, '12');
INSERT INTO `role` VALUES ('29', '文章管理员', '拥有所有的文章管理权限', '2020-02-09 17:10:05', null, '12');
INSERT INTO `role` VALUES ('30', '新增管理员', '新增用户、角色、站点', '2020-02-09 17:10:05', null, '12');
INSERT INTO `role` VALUES ('31', '站点管理员', '新建、管理站点', '2020-02-09 17:10:05', null, '12');
INSERT INTO `role` VALUES ('32', '用户管理员', '新建、管理用户', '2020-02-27 22:26:29', '2020-02-02 20:52:48', '13');
INSERT INTO `role` VALUES ('33', '角色管理员', '新建、管理角色', '2020-02-27 22:26:29', '2020-02-02 20:52:35', '13');
INSERT INTO `role` VALUES ('34', '权限管理员', '负责权限管理', '2020-02-27 22:26:29', '2020-01-31 21:00:56', '13');
INSERT INTO `role` VALUES ('35', '文章新增管理员', '拥有文章新增权限', '2020-02-27 22:26:29', null, '13');
INSERT INTO `role` VALUES ('36', '文章管理员', '拥有所有的文章管理权限', '2020-02-27 22:26:29', null, '13');
INSERT INTO `role` VALUES ('37', '新增管理员', '新增用户、角色、站点', '2020-02-27 22:26:29', null, '13');
INSERT INTO `role` VALUES ('38', '站点管理员', '新建、管理站点', '2020-02-27 22:26:29', null, '13');
INSERT INTO `role` VALUES ('39', '用户管理员', '新建、管理用户', '2020-09-21 20:26:11', '2020-02-02 20:52:48', '13');
INSERT INTO `role` VALUES ('40', '角色管理员', '新建、管理角色', '2020-09-21 20:26:11', '2020-02-02 20:52:35', '13');
INSERT INTO `role` VALUES ('41', '权限管理员', '负责权限管理', '2020-09-21 20:26:11', '2020-01-31 21:00:56', '13');
INSERT INTO `role` VALUES ('42', '文章新增管理员', '拥有文章新增权限', '2020-09-21 20:26:11', null, '13');
INSERT INTO `role` VALUES ('43', '文章管理员', '拥有所有的文章管理权限', '2020-09-21 20:26:11', null, '13');
INSERT INTO `role` VALUES ('44', '新增管理员', '新增用户、角色、站点', '2020-09-21 20:26:11', null, '13');
INSERT INTO `role` VALUES ('45', '站点管理员', '新建、管理站点', '2020-09-21 20:26:11', null, '13');
INSERT INTO `role` VALUES ('46', '用户管理员', '新建、管理用户', '2020-09-21 20:55:26', '2020-02-02 20:52:48', '14');
INSERT INTO `role` VALUES ('47', '角色管理员', '新建、管理角色', '2020-09-21 20:55:26', '2020-02-02 20:52:35', '14');
INSERT INTO `role` VALUES ('48', '权限管理员', '负责权限管理', '2020-09-21 20:55:26', '2020-01-31 21:00:56', '14');
INSERT INTO `role` VALUES ('49', '文章新增管理员', '拥有文章新增权限', '2020-09-21 20:55:26', null, '14');
INSERT INTO `role` VALUES ('50', '文章管理员', '拥有所有的文章管理权限', '2020-09-21 20:55:26', null, '14');
INSERT INTO `role` VALUES ('51', '新增管理员', '新增用户、角色、站点', '2020-09-21 20:55:26', null, '14');
INSERT INTO `role` VALUES ('52', '站点管理员', '新建、管理站点', '2020-09-21 20:55:26', null, '14');

-- ----------------------------
-- Table structure for rolepermission
-- ----------------------------
DROP TABLE IF EXISTS `rolepermission`;
CREATE TABLE `rolepermission` (
  `roleId` int(11) default NULL,
  `permissionId` int(11) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rolepermission
-- ----------------------------
INSERT INTO `rolepermission` VALUES ('1', '3');
INSERT INTO `rolepermission` VALUES ('1', '2');
INSERT INTO `rolepermission` VALUES ('1', '4');
INSERT INTO `rolepermission` VALUES ('4', '7');
INSERT INTO `rolepermission` VALUES ('4', '6');
INSERT INTO `rolepermission` VALUES ('4', '5');
INSERT INTO `rolepermission` VALUES ('4', '4');
INSERT INTO `rolepermission` VALUES ('4', '3');
INSERT INTO `rolepermission` VALUES ('4', '2');
INSERT INTO `rolepermission` VALUES ('5', '13');
INSERT INTO `rolepermission` VALUES ('5', '14');
INSERT INTO `rolepermission` VALUES ('5', '15');
INSERT INTO `rolepermission` VALUES ('5', '20');
INSERT INTO `rolepermission` VALUES ('5', '16');
INSERT INTO `rolepermission` VALUES ('5', '17');
INSERT INTO `rolepermission` VALUES ('5', '21');
INSERT INTO `rolepermission` VALUES ('1', '18');
INSERT INTO `rolepermission` VALUES ('1', '11');
INSERT INTO `rolepermission` VALUES ('1', '13');
INSERT INTO `rolepermission` VALUES ('5', '40');
INSERT INTO `rolepermission` VALUES ('5', '41');
INSERT INTO `rolepermission` VALUES ('5', '42');
INSERT INTO `rolepermission` VALUES ('5', '43');
INSERT INTO `rolepermission` VALUES ('5', '44');
INSERT INTO `rolepermission` VALUES ('5', '45');
INSERT INTO `rolepermission` VALUES ('5', '46');
INSERT INTO `rolepermission` VALUES ('4', '25');
INSERT INTO `rolepermission` VALUES ('4', '26');
INSERT INTO `rolepermission` VALUES ('4', '27');
INSERT INTO `rolepermission` VALUES ('4', '28');
INSERT INTO `rolepermission` VALUES ('4', '29');
INSERT INTO `rolepermission` VALUES ('4', '30');
INSERT INTO `rolepermission` VALUES ('1', '35');
INSERT INTO `rolepermission` VALUES ('1', '36');
INSERT INTO `rolepermission` VALUES ('1', '37');
INSERT INTO `rolepermission` VALUES ('1', '38');
INSERT INTO `rolepermission` VALUES ('1', '39');
INSERT INTO `rolepermission` VALUES ('13', '40');
INSERT INTO `rolepermission` VALUES ('13', '41');
INSERT INTO `rolepermission` VALUES ('13', '42');
INSERT INTO `rolepermission` VALUES ('13', '43');
INSERT INTO `rolepermission` VALUES ('13', '44');
INSERT INTO `rolepermission` VALUES ('13', '45');
INSERT INTO `rolepermission` VALUES ('13', '46');
INSERT INTO `rolepermission` VALUES ('14', '24');
INSERT INTO `rolepermission` VALUES ('14', '25');
INSERT INTO `rolepermission` VALUES ('14', '26');
INSERT INTO `rolepermission` VALUES ('14', '27');
INSERT INTO `rolepermission` VALUES ('14', '28');
INSERT INTO `rolepermission` VALUES ('14', '29');
INSERT INTO `rolepermission` VALUES ('14', '30');
INSERT INTO `rolepermission` VALUES ('11', '35');
INSERT INTO `rolepermission` VALUES ('11', '36');
INSERT INTO `rolepermission` VALUES ('11', '37');
INSERT INTO `rolepermission` VALUES ('11', '38');
INSERT INTO `rolepermission` VALUES ('11', '39');
INSERT INTO `rolepermission` VALUES ('16', '47');
INSERT INTO `rolepermission` VALUES ('16', '127');
INSERT INTO `rolepermission` VALUES ('17', '24');
INSERT INTO `rolepermission` VALUES ('17', '40');
INSERT INTO `rolepermission` VALUES ('17', '47');
INSERT INTO `rolepermission` VALUES ('19', '40');
INSERT INTO `rolepermission` VALUES ('19', '41');
INSERT INTO `rolepermission` VALUES ('19', '42');
INSERT INTO `rolepermission` VALUES ('19', '43');
INSERT INTO `rolepermission` VALUES ('19', '44');
INSERT INTO `rolepermission` VALUES ('19', '45');
INSERT INTO `rolepermission` VALUES ('19', '46');
INSERT INTO `rolepermission` VALUES ('18', '24');
INSERT INTO `rolepermission` VALUES ('18', '25');
INSERT INTO `rolepermission` VALUES ('18', '26');
INSERT INTO `rolepermission` VALUES ('18', '27');
INSERT INTO `rolepermission` VALUES ('18', '28');
INSERT INTO `rolepermission` VALUES ('18', '29');
INSERT INTO `rolepermission` VALUES ('18', '30');
INSERT INTO `rolepermission` VALUES ('20', '35');
INSERT INTO `rolepermission` VALUES ('20', '36');
INSERT INTO `rolepermission` VALUES ('20', '37');
INSERT INTO `rolepermission` VALUES ('20', '38');
INSERT INTO `rolepermission` VALUES ('20', '39');
INSERT INTO `rolepermission` VALUES ('22', '47');
INSERT INTO `rolepermission` VALUES ('22', '127');
INSERT INTO `rolepermission` VALUES ('21', '24');
INSERT INTO `rolepermission` VALUES ('21', '40');
INSERT INTO `rolepermission` VALUES ('21', '47');
INSERT INTO `rolepermission` VALUES ('24', '163');
INSERT INTO `rolepermission` VALUES ('23', '163');
INSERT INTO `rolepermission` VALUES ('23', '164');
INSERT INTO `rolepermission` VALUES ('23', '166');
INSERT INTO `rolepermission` VALUES ('26', '40');
INSERT INTO `rolepermission` VALUES ('26', '41');
INSERT INTO `rolepermission` VALUES ('26', '42');
INSERT INTO `rolepermission` VALUES ('26', '43');
INSERT INTO `rolepermission` VALUES ('26', '44');
INSERT INTO `rolepermission` VALUES ('26', '45');
INSERT INTO `rolepermission` VALUES ('26', '46');
INSERT INTO `rolepermission` VALUES ('25', '24');
INSERT INTO `rolepermission` VALUES ('25', '25');
INSERT INTO `rolepermission` VALUES ('25', '26');
INSERT INTO `rolepermission` VALUES ('25', '27');
INSERT INTO `rolepermission` VALUES ('25', '28');
INSERT INTO `rolepermission` VALUES ('25', '29');
INSERT INTO `rolepermission` VALUES ('25', '30');
INSERT INTO `rolepermission` VALUES ('27', '35');
INSERT INTO `rolepermission` VALUES ('27', '36');
INSERT INTO `rolepermission` VALUES ('27', '37');
INSERT INTO `rolepermission` VALUES ('27', '38');
INSERT INTO `rolepermission` VALUES ('27', '39');
INSERT INTO `rolepermission` VALUES ('31', '47');
INSERT INTO `rolepermission` VALUES ('31', '127');
INSERT INTO `rolepermission` VALUES ('30', '24');
INSERT INTO `rolepermission` VALUES ('30', '40');
INSERT INTO `rolepermission` VALUES ('30', '47');
INSERT INTO `rolepermission` VALUES ('28', '163');
INSERT INTO `rolepermission` VALUES ('29', '163');
INSERT INTO `rolepermission` VALUES ('29', '164');
INSERT INTO `rolepermission` VALUES ('29', '166');
INSERT INTO `rolepermission` VALUES ('33', '40');
INSERT INTO `rolepermission` VALUES ('33', '41');
INSERT INTO `rolepermission` VALUES ('33', '42');
INSERT INTO `rolepermission` VALUES ('33', '43');
INSERT INTO `rolepermission` VALUES ('33', '44');
INSERT INTO `rolepermission` VALUES ('33', '45');
INSERT INTO `rolepermission` VALUES ('33', '46');
INSERT INTO `rolepermission` VALUES ('32', '24');
INSERT INTO `rolepermission` VALUES ('32', '25');
INSERT INTO `rolepermission` VALUES ('32', '26');
INSERT INTO `rolepermission` VALUES ('32', '27');
INSERT INTO `rolepermission` VALUES ('32', '28');
INSERT INTO `rolepermission` VALUES ('32', '29');
INSERT INTO `rolepermission` VALUES ('32', '30');
INSERT INTO `rolepermission` VALUES ('34', '35');
INSERT INTO `rolepermission` VALUES ('34', '36');
INSERT INTO `rolepermission` VALUES ('34', '37');
INSERT INTO `rolepermission` VALUES ('34', '38');
INSERT INTO `rolepermission` VALUES ('34', '39');
INSERT INTO `rolepermission` VALUES ('38', '47');
INSERT INTO `rolepermission` VALUES ('38', '127');
INSERT INTO `rolepermission` VALUES ('37', '24');
INSERT INTO `rolepermission` VALUES ('37', '40');
INSERT INTO `rolepermission` VALUES ('37', '47');
INSERT INTO `rolepermission` VALUES ('35', '163');
INSERT INTO `rolepermission` VALUES ('36', '163');
INSERT INTO `rolepermission` VALUES ('36', '164');
INSERT INTO `rolepermission` VALUES ('36', '166');
INSERT INTO `rolepermission` VALUES ('4', '24');
INSERT INTO `rolepermission` VALUES ('24', '269');
INSERT INTO `rolepermission` VALUES ('23', '269');
INSERT INTO `rolepermission` VALUES ('33', '256');
INSERT INTO `rolepermission` VALUES ('33', '257');
INSERT INTO `rolepermission` VALUES ('33', '258');
INSERT INTO `rolepermission` VALUES ('33', '259');
INSERT INTO `rolepermission` VALUES ('33', '260');
INSERT INTO `rolepermission` VALUES ('33', '261');
INSERT INTO `rolepermission` VALUES ('33', '262');
INSERT INTO `rolepermission` VALUES ('32', '226');
INSERT INTO `rolepermission` VALUES ('32', '227');
INSERT INTO `rolepermission` VALUES ('32', '228');
INSERT INTO `rolepermission` VALUES ('32', '230');
INSERT INTO `rolepermission` VALUES ('32', '231');
INSERT INTO `rolepermission` VALUES ('32', '232');
INSERT INTO `rolepermission` VALUES ('34', '251');
INSERT INTO `rolepermission` VALUES ('34', '252');
INSERT INTO `rolepermission` VALUES ('34', '253');
INSERT INTO `rolepermission` VALUES ('34', '254');
INSERT INTO `rolepermission` VALUES ('34', '255');
INSERT INTO `rolepermission` VALUES ('38', '263');
INSERT INTO `rolepermission` VALUES ('38', '265');
INSERT INTO `rolepermission` VALUES ('37', '225');
INSERT INTO `rolepermission` VALUES ('37', '256');
INSERT INTO `rolepermission` VALUES ('37', '263');
INSERT INTO `rolepermission` VALUES ('35', '235');
INSERT INTO `rolepermission` VALUES ('36', '235');
INSERT INTO `rolepermission` VALUES ('36', '236');
INSERT INTO `rolepermission` VALUES ('36', '238');
INSERT INTO `rolepermission` VALUES ('32', '225');
INSERT INTO `rolepermission` VALUES ('35', '295');
INSERT INTO `rolepermission` VALUES ('36', '295');
INSERT INTO `rolepermission` VALUES ('47', '376');
INSERT INTO `rolepermission` VALUES ('47', '377');
INSERT INTO `rolepermission` VALUES ('47', '378');
INSERT INTO `rolepermission` VALUES ('47', '379');
INSERT INTO `rolepermission` VALUES ('47', '380');
INSERT INTO `rolepermission` VALUES ('47', '381');
INSERT INTO `rolepermission` VALUES ('47', '382');
INSERT INTO `rolepermission` VALUES ('46', '338');
INSERT INTO `rolepermission` VALUES ('46', '339');
INSERT INTO `rolepermission` VALUES ('46', '340');
INSERT INTO `rolepermission` VALUES ('46', '342');
INSERT INTO `rolepermission` VALUES ('46', '343');
INSERT INTO `rolepermission` VALUES ('46', '344');
INSERT INTO `rolepermission` VALUES ('48', '371');
INSERT INTO `rolepermission` VALUES ('48', '372');
INSERT INTO `rolepermission` VALUES ('48', '373');
INSERT INTO `rolepermission` VALUES ('48', '374');
INSERT INTO `rolepermission` VALUES ('48', '375');
INSERT INTO `rolepermission` VALUES ('52', '384');
INSERT INTO `rolepermission` VALUES ('52', '386');
INSERT INTO `rolepermission` VALUES ('51', '337');
INSERT INTO `rolepermission` VALUES ('51', '376');
INSERT INTO `rolepermission` VALUES ('51', '384');
INSERT INTO `rolepermission` VALUES ('49', '347');
INSERT INTO `rolepermission` VALUES ('50', '347');
INSERT INTO `rolepermission` VALUES ('50', '348');
INSERT INTO `rolepermission` VALUES ('50', '350');
INSERT INTO `rolepermission` VALUES ('46', '337');
INSERT INTO `rolepermission` VALUES ('49', '351');
INSERT INTO `rolepermission` VALUES ('50', '351');

-- ----------------------------
-- Table structure for site
-- ----------------------------
DROP TABLE IF EXISTS `site`;
CREATE TABLE `site` (
  `id` int(11) NOT NULL auto_increment,
  `siteName` varchar(128) default NULL COMMENT '站点中文名称',
  `siteSign` varchar(128) default NULL COMMENT '站点的英文标示，唯一不可重复',
  `siteUrl` varchar(128) default NULL COMMENT '站点地址',
  `siteDes` varchar(512) default NULL COMMENT '站点描述',
  `cdsAccessLog` varchar(1) default '0' COMMENT 'cds访问日志（默认0不启用，1启用）',
  `cmsAccessLog` varchar(1) default '0' COMMENT 'cms访问日志（默认0不启用，1启用）',
  `fileMaxSize` int(1) default NULL COMMENT '上传文件最大限定，单位M',
  `fileSuffix` varchar(256) default NULL COMMENT '支持文件后缀，以|分割',
  `createTime` datetime default NULL COMMENT '创建时间',
  `updateTime` datetime default NULL COMMENT '更新时间',
  `status` varchar(1) default NULL COMMENT '是否是当前站点，1标识是，2标识不是，只能有一个1',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of site
-- ----------------------------
INSERT INTO `site` VALUES ('12', '环保科技公司-仿', 'hbkjgs', null, '仿写一个环保科技公司前端页面', '0', '0', '3', 'jpg|pdf|gif', '2020-02-09 17:10:03', null, '2');
INSERT INTO `site` VALUES ('14', '类友网络-仿', 'layuiNet', null, '由layui模板改造而成', '0', '0', '5', null, '2020-09-21 20:55:24', null, '2');
