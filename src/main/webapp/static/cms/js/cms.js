/**
 * 通用方法：保存类form提交
 * @param selector form选择器，可以是id也可以是class
 */
function ajaxSaveForm(selector){
	var loadIndex = layer.load();
	var form = $(selector);
	var action = form.attr('action');
	$.post(action, form.serialize(), function(data){
		layer.close(loadIndex);
		if('ok'==data.state){
			layer.msg(data.msg);
			$.pjax.reload('#rightMain');
		} else {
			layer.msg(data.msg,{icon: 7});
		}
	});
}

/**
 * 通用方法：搜索类表单提交
 * @param selector form选择器，可以是id也可以是class
 * @param container	局部刷新的页面容器，可以是id也可以是class
 */
function ajaxSearchForm(selector, container){
	var form = $(selector);
	var action = form.attr('action');
	$.ajax({
		url: action,
		data: form.serialize(),
		type: "POST",
		beforeSend: function(xhr){
			//这里设置header，仿造pjax请求，刷新局部页面
			xhr.setRequestHeader('X-PJAX', 'true');
			NProgress.start();
		},
		success: function(html){
			$(container).html(html);
		},
		complete: function(XMLHttpRequest, textStatus) {
			NProgress.done();
		}
	});
}

/**
 * 通用方法：数据表格中的删除按钮
 * @param url	删除操作的完整url，包含地址和参数
 */
function ajaxDelete(url){
	layer.confirm('确认删除该元素么?', function(index){
		$.post(url, function(data){
			if('ok'==data.state){
				layer.msg(data.msg);
				$.pjax.reload('#rightMain');
			} else {
				layer.alert(data.msg);
			}
		});
		layer.close(index);
	}); 
}

/**
 * 通用方法：类似删除的反响操作，不需要确认直接更改状态
 * @param url
 */
function ajaxOn(url){
	$.post(url, function(data){
		if('ok'==data.state){
			layer.msg(data.msg);
			$.pjax.reload('#rightMain');
		} else {
			layer.alert(data.msg);
		}
	});
}

/**
 * 通用方法：使用pjax方式重新加载指定地址
 * @param url	需要加载的地址
 */
function ajaxReload(url){
	$.post(url, function(data){
		if('ok'==data.state){
			layer.msg(data.msg);
			$.pjax.reload('#rightMain');
		} else {
			layer.alert(data.msg);
		}
	});
}

/**
 * 通用方法：预览图片
 * @param id	图片在files表中的id
 */
function viewImg(id){
	$.getJSON('/file/getImg/'+id, function(json){
		//使用layer的相册层
		layer.photos({
			photos: json,
			anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
		});
	});
}

/**
 * 设置当前站点
 * @param id	要设置的站点id
 */
function setMainSite(id){
	layer.confirm('确认将该站点设置为当前站点么?<br/>设置后站点信息将被重置，需要使用站点用户重新登录！', function(index){
		$.post("/cmsSite/setMainSite/"+id, function(data){
			if('ok'==data.state){
				layer.msg(data.msg);
				window.location.href = "/cmsLogin/logOut";
			} else {
				layer.alert(data.msg);
			}
		});
		layer.close(index);
	});
}

/**
 * 恢复默认站点
 */
function resetSite(){
	layer.confirm('确认恢复默认站点么?<br/>设置后站点信息将被重置，需要使用站点用户重新登录！', function(index){
		$.post("/cmsSite/resetSite", function(data){
			if('ok'==data.state){
				layer.msg(data.msg);
				window.location.href = "/cmsLogin/logOut";
			} else {
				layer.alert(data.msg);
			}
		});
		layer.close(index);
	});
}

/**
 * 删除栏目
 * @param id	栏目id
 */
function delColumn(id){
	layer.confirm('确认删除该栏目么?<br/>删除后子栏目、文章将被同步删除！', function(index){
		$.post("/cmsColumn/delColumn/"+id, function(data){
			if('ok'==data.state){
				layer.msg(data.msg);
				$.pjax.reload('#rightMain');
			} else {
				layer.alert(data.msg);
			}
		});
		layer.close(index);
	});
}

/**
 * 启用/激活栏目
 * @param id	栏目id
 */
function activateCol(id){
	layer.confirm('启用仅针对当前栏目生效！<br/>之前删除的子栏目、文章需单独处理！', function(index){
		$.post("/cmsColumn/activateCol/"+id, function(data){
			if('ok'==data.state){
				layer.msg(data.msg);
				$.pjax.reload('#rightMain');
			} else {
				layer.alert(data.msg);
			}
		});
		layer.close(index);
	});
}