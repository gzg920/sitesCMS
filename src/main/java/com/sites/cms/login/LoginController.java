package com.sites.cms.login;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.sites.cms.common.CmsAccessInterceptor;
import com.sites.cms.permission.PermissionInterceptor;
import com.sites.common.SiteInfo;

/**
 * 登录控制器
 * 
 * @author zyg
 * 2020年1月4日 下午1:16:52
 */
@Clear({LoggedInterceptor.class, PermissionInterceptor.class, CmsAccessInterceptor.class})
public class LoginController extends Controller {

	@Inject
	private LoginService srv;
	
	/**
	 * 默认跳转至登录页面
	 */
	public void index(){
		render("login.html");
	}
	
	/**
	 * 生成验证码
	 */
	public void createCaptcha(){
		renderCaptcha();
	}
	
	/**
	 * 进行用户登录
	 */
	@Before(LoginValidator.class)
	public void doLogin(){
		//获取参数
		String name = getPara("name", "");
		//获取页面输入的密码
		String password = getPara("password","");
		
		Ret ret = srv.doLogin(name, password);
		if(ret.isOk()){//登录成功
			//将当前登录用户保存到session中
			setSessionAttr(SiteInfo.account, ret.get(SiteInfo.account));
			String url = getAttr("ctx")+"/cms";
			ret.set("url", url);
		}
		renderJson(ret);
	}
	
	/**
	 * 退出登录
	 */
	public void logOut(){
		getSession().removeAttribute(SiteInfo.account);
		redirect(getAttr("ctx")+"/cmsLogin");
	}
}
