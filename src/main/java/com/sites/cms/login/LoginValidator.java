package com.sites.cms.login;

import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.validate.Validator;
import com.sites.common.SiteInfo;

/**
 * 登录校验器
 * <br/>校验登录时的用户名、密码、验证码
 * 
 * @author zyg
 * 2020年1月31日 上午11:33:30
 */
public class LoginValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		setShortCircuit(true);
		validateRequired("name", SiteInfo.msgKey, "用户名不可为空！");
		validateRequired("password", SiteInfo.msgKey, "密码不可为空！");
		validateCaptcha("captchaInput", SiteInfo.msgKey, "验证码不正确！");
	}

	@Override
	protected void handleError(Controller c) {
		Ret ret = Ret.fail(SiteInfo.msgKey, c.getAttr(SiteInfo.msgKey));
		c.renderJson(ret);
	}

}
