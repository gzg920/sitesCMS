package com.sites.cms.column;

import java.util.List;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.sites.cms.permission.Remark;
import com.sites.common.SiteInfo;
import com.sites.common.model.Account;
import com.sites.common.model.Column;

/**
 * 栏目控制器
 * 
 * @author zyg
 * 2020年2月3日 上午11:39:18
 */
public class ColumnController extends Controller {

	@Inject
	private ColumnService srv;
	
	@Remark("菜单：新增栏目")
	public void enterColumnAdd(){
		//查询一级栏目列表，栏目新增的时候要使用
		List<Column> columnList = srv.getParentColumns();
		setAttr("columnList", columnList);
		render("pageColumnAdd.html");
	}
	
	@Remark("按钮：保存栏目")
	@Before(ColumnValidator.class)
	public void saveColumn(){
		Column column = getModel(Column.class, "");
		Ret ret = srv.save(column);
		renderJson(ret);
	}
	
	@Remark("菜单：栏目管理")
	public void enterColumnMng(){
		String colTreeStr = srv.getAllColumns4tree();
		setAttr("colTreeStr", colTreeStr);
		render("pageColumnMng.html");
	}
	
	@Remark("按钮：编辑栏目")
	public void enterColumnEdit(){
		int id = getParaToInt(0);
		Column column = srv.getById(id);
		setAttr("column", column);
		List<Column> columnList = srv.getParentColumns();
		setAttr("columnList", columnList);
		render("pageColumnEdit.html");
	}
	
	@Remark("按钮：更新栏目")
	@Before(ColumnValidator.class)
	public void updateColumn(){
		Column column = getModel(Column.class, "");
		Ret ret = srv.updateColumn(column);
		renderJson(ret);
	}
	
	@Remark("按钮：删除栏目")
	public void delColumn(){
		int id = getParaToInt(0);
		Ret ret = srv.delColumn(id, (Account)getSessionAttr(SiteInfo.account));
		renderJson(ret);
	}
	
	@Remark("按钮：启用/激活栏目")
	public void activateCol(){
		int id = getParaToInt(0);
		Ret ret = srv.activateCol(id);
		renderJson(ret);
	}
	
}
