package com.sites.cms.column;

import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.validate.Validator;
import com.sites.common.SiteInfo;

/**
 * 栏目校验器
 * 
 * @author zyg
 * 2020年11月8日 上午11:07:45
 */
public class ColumnValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		setShortCircuit(true);
		validateRequired("name", SiteInfo.msgKey, "栏目名称不可为空！");
		validateString("name", 1, 50, SiteInfo.msgKey, "栏目名称最长为50个字符！");
		
		validateRequired("enName", SiteInfo.msgKey, "英文标识不可为空！");
		validateString("enName", 1, 50, SiteInfo.msgKey, "英文标识最长为50个字符！");
		validateRegex("enName", "[0-9A-Za-z]+", SiteInfo.msgKey, "英文标识只能是英文字母或数字！");
	}

	@Override
	protected void handleError(Controller c) {
		Ret ret = Ret.fail(SiteInfo.msgKey, c.getAttr(SiteInfo.msgKey));
		c.renderJson(ret);
	}

}
