package com.sites.cms.site;

import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.validate.Validator;
import com.sites.common.SiteInfo;

/**
 * 站点校验器
 * 
 * @author zyg
 * 2020年11月8日 下午2:17:24
 */
public class SiteValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		setShortCircuit(true);
		validateRequired("siteName", SiteInfo.msgKey, "站点名称不可为空！");
		validateString("siteName", 1, 100, SiteInfo.msgKey, "站点名称最大长度为100个字符！");
		
		validateRequired("siteSign", SiteInfo.msgKey, "站点标识不可为空！");
		validateString("siteSign", 1, 100, SiteInfo.msgKey, "站点标识最大长度为100个字符！");
		validateRegex("siteSign", "[0-9A-Za-z]+", SiteInfo.msgKey, "站点标识只能是英文字母或数字！");
		
		String fileMaxSize = c.getPara("fileMaxSize");
		if(StrKit.notBlank(fileMaxSize)){
			validateBoolean("fileMaxSize", SiteInfo.msgKey, "文件大小必须输入整数！");
		}
	}

	@Override
	protected void handleError(Controller c) {
		Ret ret = Ret.fail(SiteInfo.msgKey, c.getAttr(SiteInfo.msgKey));
		c.renderJson(ret);
	}

}
