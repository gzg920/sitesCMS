package com.sites.cms.role;

import java.util.List;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Record;
import com.sites.cms.permission.PermissionService;
import com.sites.cms.permission.Remark;
import com.sites.common.model.Permission;
import com.sites.common.model.Role;

/**
 * 角色控制器
 * 
 * @author zyg
 * 2020年1月31日 下午1:13:25
 */
public class RoleController extends Controller {
	
	@Inject
	private RoleService srv;
	@Inject
	private PermissionService permSrv;

	@Remark("菜单：添加角色")
	public void enterRoleAdd(){
		render("pageRoleAdd.html");
	}
	
	@Remark("菜单：角色管理")
	public void enterRoleMng(){
		List<Role> roleList = srv.queryAllRoles();
		setAttr("roleList", roleList);
		render("pageRoleMng.html");
	}
	
	@Remark("按钮：保存角色信息")
	@Before(RoleValidator.class)
	public void saveRole(){
		Role role = getModel(Role.class, "");
		Ret ret = srv.saveRole(role);
		renderJson(ret);
	}
	
	@Remark("按钮：编辑角色信息")
	public void enterRoleEdit(){
		int id = getParaToInt(0);
		Role role = srv.getById(id);
		setAttr("r", role);
		render("pageRoleEdit.html");
	}
	
	@Remark("按钮：更新角色信息")
	@Before(RoleValidator.class)
	public void updateRole(){
		Role role = getModel(Role.class, "");
		Ret ret = srv.updateRole(role);
		renderJson(ret);
	}
	
	@Remark("按钮：编辑角色权限")
	public void enterRolePerm(){
		int id = getParaToInt(0);
		Role role = srv.getById(id);
		List<Record> rolePerms = srv.getRolePerm(id);
		List<Permission> permList = permSrv.getNormalPerms();
		permList = srv.markRolePerms(permList, rolePerms);
		setAttr("permList", permList);
		setAttr("role", role);
		render("pageRolePerm.html");
	}
	
	@Remark("按钮：更新角色权限")
	public void updateRolePerm(){
		int roleId = getParaToInt(0);
		int permId = getParaToInt(1);
		boolean checked = getParaToBoolean(2);
		Ret ret = srv.updateRolePerm(roleId, permId, checked);
		renderJson(ret);
	}
	
}
