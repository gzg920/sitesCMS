package com.sites.cms.role;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.sites.common.SiteInfo;
import com.sites.common.model.Permission;
import com.sites.common.model.Role;

/**
 * 角色处理服务层
 * 
 * @author zyg
 * 2020年1月28日 下午9:03:10
 */
public class RoleService {

	private Role dao = new Role().dao();
	
	/**
	 * 保存角色
	 * @param role
	 * @return
	 */
	public Ret saveRole(Role role){
		Ret ret = Ret.ok();
		Role temp = getByName(role.getName(), SiteInfo.siteId);
		if(temp != null){
			ret.setFail();
			ret.set(SiteInfo.msgKey, "角色名不可重复！");
			return ret;
		}
		role.setSiteId(SiteInfo.siteId);
		role.setCreateTime(new Date());
		role.save();
		ret.set(SiteInfo.msgKey, "角色添加成功！");
		
		return ret;
	}
	
	/**
	 * 根据名称查询角色
	 * @param name
	 * @return
	 */
	public Role getByName(String name, int siteId){
		String sql = dao.getSql("role.getByName");
		return dao.findFirst(sql, name, siteId);
	}
	
	/**
	 * 查询所有角色
	 * @return
	 */
	public List<Role> queryAllRoles(){
		String sql = dao.getSql("role.queryAllRoles");
		return dao.find(sql, SiteInfo.siteId);
	}
	
	/**
	 * 根据id查询角色
	 * @param id
	 * @return
	 */
	public Role getById(int id){
		return dao.findById(id);
	}
	
	/**
	 * 更新角色
	 * @param role
	 * @return
	 */
	public Ret updateRole(Role role){
		Ret ret = Ret.create();
		
		Role r = getById(role.getId());
		r.setName(role.getName());
		r.setRemark(role.getRemark());
		r.setUpdateTime(new Date());
		boolean flag = r.update();
		if(flag){
			ret.setOk();
			ret.set(SiteInfo.msgKey, "更新成功");
		} else {
			ret.setFail();
			ret.set(SiteInfo.msgKey, "更新失败，请联系管理员！");
		}
		return ret;
	}
	
	/**
	 * 获取角色已经拥有的权限
	 * @param id	角色id
	 * @return
	 */
	public List<Record> getRolePerm(int id){
		String sql = dao.getSql("role.getRolePerm");
		return Db.find(sql, id);
	}
	
	/**
	 * 在所有权限中标记角色已经有的权限
	 * @param permissions	所有权限
	 * @param rolePerms		角色已经有的权限
	 * @return
	 */
	public List<Permission> markRolePerms(List<Permission> permissions, List<Record> rolePerms){
		int permId,rolePermId;
		for(Permission p : permissions){
			permId = p.getId();
			for(Record r : rolePerms){
				rolePermId = r.getInt("permissionId");
				if(permId == rolePermId){
					p.put("mark", "checked");
					continue;
				}
			}
		}
		
		return permissions;
	}
	
	/**
	 * 更新角色权限
	 * @param roleId	角色id
	 * @param permId	权限id
	 * @param checked	增加或删除
	 * @return
	 */
	public Ret updateRolePerm(int roleId, int permId, boolean checked){
		Ret ret = Ret.ok();
		String addSql = dao.getSql("role.addPerm");
		String delSql = dao.getSql("role.delPerm");
		int nums = 0;
		if(checked){
			nums = Db.update(addSql, roleId, permId);
		} else {
			nums = Db.delete(delSql, roleId, permId);
		}
		if(nums <= 0){
			ret.setFail();
			ret.set(SiteInfo.msgKey, "赋权失败，请联系管理员！");
		}
		return ret;
	}
}
