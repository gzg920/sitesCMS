package com.sites.cms.accessLog;

import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;
import com.sites.cms.account.AccountService;
import com.sites.cms.permission.PermissionService;
import com.sites.cms.permission.Remark;
import com.sites.common.model.Accesslog;
import com.sites.common.model.Account;
import com.sites.common.model.Permission;

/**
 * 访问日志控制器
 * 
 * @author zyg
 * 2020年8月4日 下午10:51:34
 */
public class AccessLogController extends Controller {

	@Inject
	AccessLogService srv;
	
	@Inject
	AccountService accountSrv;
	
	@Inject
	PermissionService permissionSrv;
	
	/**
	 * 进入用户端日志查看界面，对应的是cds目录下的工能和页面
	 */
	@Remark("菜单：用户访问日志")
	public void enterCdsLogView(){
		int page = getParaToInt("page", 1);
		int limit = getParaToInt("limit", 8);
		String type = getPara("type", "cds");
		String ip = getPara("ip", "");
		String actionKey = getPara("actionKey", "");
		String startDate = getPara("startDate", "");
		String endDate = getPara("endDate", "");
		
		keepPara();//保存参数
		
		Page<Accesslog> logPage = srv.queryCdsLogPage(page, limit, type, ip, actionKey, startDate, endDate);
		setAttr("logPage", logPage);
		render("pageCdsLogView.html");
	}
	
	/**
	 * 进入用户端日志查看界面，对应的是cms目录下的功能和页面
	 */
	@Remark("菜单：管理端访问日志")
	public void enterCmsLogView(){
		int page = getParaToInt("page", 1);
		int limit = getParaToInt("limit", 8);
		String type = getPara("type", "cms");
		String ip = getPara("ip", "");
		String visitor = getPara("visitor", "");
		String actionKey = getPara("actionKey", "");
		String startDate = getPara("startDate", "");
		String endDate = getPara("endDate", "");
		keepPara();//保存参数
		//查询访问日志
		Page<Accesslog> logPage = srv.queryCmsLogPage(page, limit, type, ip, visitor, actionKey, startDate, endDate);
		set("logPage", logPage);
		//查询管理用户，在界面上做筛选
		List<Account> accounts = accountSrv.queryAllAccounts();
		set("accounts", accounts);
		//查询请求地址对应功能
		List<Permission> permissions = permissionSrv.getAllPermission();
		set("permissions", permissions);
		//返回页面
		render("pageCmsLogView.html");
	}
}
