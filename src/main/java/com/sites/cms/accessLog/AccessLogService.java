package com.sites.cms.accessLog;

import java.util.List;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Page;
import com.sites.common.SiteInfo;
import com.sites.common.model.Accesslog;

/**
 * 访问日志服务层
 * 
 * @author zyg
 * 2020年8月4日 下午10:52:03
 */
public class AccessLogService {
	
	Accesslog dao = Accesslog.dao;
	
	/**
	 * 获取网站首页访问次数
	 * 
	 * @return
	 */
	public int countIndexAccess(){
		Kv kv = Kv.create();
		kv.set("type", SiteInfo.cdsLog);
		kv.set("actionKey", "/");
		kv.set("siteId", SiteInfo.siteId);
		List<Accesslog> accesslogs = dao.template("accessLog.getAccessLogs", kv).find();
		return accesslogs.size();
	}
	
	/**
	 * 分页查询cds访问日志
	 * 
	 * @param page		当前页码
	 * @param limit		每页显示条数
	 * @param type		日志类型，此处应为cds
	 * @param ip		访问者IP地址
	 * @param actionKey	请求地址
	 * @param startDate	起始日期
	 * @param endDate	截止日期
	 * @return
	 */
	public Page<Accesslog> queryCdsLogPage(int page, int limit, String type, String ip,
			String actionKey, String startDate, String endDate){
		Kv kv = Kv.create();
		kv.set("type", type);
		kv.set("ip", ip);
		kv.set("actionKey", actionKey);
		kv.set("startDate", startDate);
		kv.set("endDate", endDate);
		kv.set("siteId", SiteInfo.siteId);
		return dao.template("accessLog.queryCdsLogPage", kv).paginate(page, limit);
	}
	
	/**
	 * 分页查询cms访问日志
	 * 
	 * @param page		当前页面
	 * @param limit		每页显示条数
	 * @param type		日志类型，这里是cms
	 * @param ip		用户登录的IP地址
	 * @param visitor	访问用户
	 * @param actionKey	请求地址
	 * @param startDate	起始时间
	 * @param endDate	截止时间
	 * @return
	 */
	public Page<Accesslog> queryCmsLogPage(int page, int limit,String type, String ip,
			String visitor,	String actionKey, String startDate, String endDate){
		Kv kv = Kv.create();
		kv.set("siteId", SiteInfo.siteId);
		kv.set("type", type);
		kv.set("ip", ip);
		kv.set("visitor", visitor);
		kv.set("actionKey", actionKey);
		kv.set("startDate", startDate);
		kv.set("endDate", endDate);
		kv.set("siteId", SiteInfo.siteId);
		return dao.template("accessLog.queryCmsLogPage", kv).paginate(page, limit);
	}
}
