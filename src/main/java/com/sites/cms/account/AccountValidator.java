package com.sites.cms.account;

import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.validate.Validator;
import com.sites.common.SiteInfo;

/**
 * 用户校验器<br/>
 * 主要是校验新增、修改时的参数
 * 
 * @author zyg
 * 2020年11月8日 下午1:11:05
 */
public class AccountValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		setShortCircuit(true);
		validateRequired("nickName", SiteInfo.msgKey, "昵称不可为空！");
		validateString("nickName", 1, 50, SiteInfo.msgKey, "昵称最长为50个字符！");
		
		validateRequired("userName", SiteInfo.msgKey, "登录名不可为空！");
		validateString("userName", 1, 100, SiteInfo.msgKey, "登录名最长为100个字符！");

		// 新增的时候必须校验密码，修改的不是必须得
		String mathodName = getActionMethodName();
		if("saveAccount".equals(mathodName)){
			validateRequired("password", SiteInfo.msgKey, "密码不可为空！");
			validateString("password", 1, 100, SiteInfo.msgKey, "密码最长为100个字符！");
		}
	}

	@Override
	protected void handleError(Controller c) {
		Ret ret = Ret.fail(SiteInfo.msgKey, c.getAttr(SiteInfo.msgKey));
		c.renderJson(ret);
	}

}
