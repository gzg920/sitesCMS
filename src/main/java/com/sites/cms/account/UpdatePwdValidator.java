package com.sites.cms.account;

import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.validate.Validator;
import com.sites.common.SiteInfo;

/**
 * 修改密码校验器
 * 
 * @author zyg
 * 2020年3月8日 下午6:47:39
 */
public class UpdatePwdValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		setShortCircuit(true);
		validateRequired("oldPwd", SiteInfo.msgKey, "原密码不能为空！");
		validateRequired("password", SiteInfo.msgKey, "新密码不能为空！");
		validateString("password", 1, 100, SiteInfo.msgKey, "密码最长为100个字符！");
		
		validateRequired("confirm", SiteInfo.msgKey, "确认密码不能为空！");
		validateEqualField("password", "confirm", SiteInfo.msgKey, "两次密码输入不一致！");
	}

	@Override
	protected void handleError(Controller c) {
		Ret ret = Ret.fail(SiteInfo.msgKey, c.getAttr(SiteInfo.msgKey));
		c.renderJson(ret);
	}

}
