package com.sites.cms.common;

import com.jfinal.template.Directive;
import com.jfinal.template.Env;
import com.jfinal.template.TemplateException;
import com.jfinal.template.expr.ast.Expr;
import com.jfinal.template.expr.ast.ExprList;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.ParseException;
import com.jfinal.template.stat.Scope;
import com.sites.common.SiteInfo;

/**
 * 显示状态的扩展指令
 * 
 * 用例：
 * #status(1) --> 正常
 * #status(2) --> 删除
 * 
 * @author zyg
 * 2020年1月31日 下午6:47:35
 */
public class StatusDirective extends Directive {

	private Expr valueExpr;
	private int paraNum;
	
	@Override
	public void setExprList(ExprList exprList) {
		this.paraNum = exprList.length();
		if (paraNum != 1) {
			throw new ParseException("Wrong number parameter of #status directive, one parameters are allowed here", location);
		}
		this.valueExpr = exprList.getExpr(0);
	}
	
	@Override
	public void exec(Env env, Scope scope, Writer writer) {
		Object valueObject = valueExpr.eval(scope);
		if(valueObject instanceof String){
			String value = (String) valueObject;
			if(SiteInfo.statusNormal.equals(value)){
				write(writer, "正常");
			} else if(SiteInfo.statusDel.equals(value)){
				write(writer, "删除");
			} else {
				write(writer, "其他");
			}
		} else {
			throw new TemplateException("The first parameter of #cutStr directive must be String type", location);
		}
	}

}
