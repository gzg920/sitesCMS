package com.sites.cms.article;

import java.util.Date;
import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.sites.cms.column.ColumnService;
import com.sites.common.SiteInfo;
import com.sites.common.model.Account;
import com.sites.common.model.Article;
import com.sites.common.model.Column;

public class ArticleService {

	private Article dao = new Article().dao();
	@Inject
	private ColumnService colSrv;
	
	/**
	 * 保存文章
	 * @param article	文章(不带附件信息)
	 * @param files		附件id的数组
	 * @param account	登录用户
	 * @return
	 */
	public Ret saveArt(Article article, String[] files, Account account){
		Ret ret = Ret.create();
		article.setStatus(SiteInfo.statusNormal);
		article.setClickNum(0);
		article.setCollectNum(0);
		article.setFavoriteCount(0);
		article.setCreateAccount(account.getId());
		article.setCreateTime(new Date());
		article.setUpdateTime(new Date());
		if(files!=null && files.length>0){
			article.setFiles(StrKit.join(files,","));
		}
		/*
		 * update 2020.10.15
		 * 是否置顶改为switch，否的时候数据为空，兼容历史数据进行手动处理
		 */
		if(StrKit.isBlank(article.getIsTop())){
			article.setIsTop("0");
		}
		if(article.getThumbnail()==null || article.getThumbnail()<=0){
			article.setThumbnail(0);
		}
		article.setSiteId(SiteInfo.siteId);
		article.save();
		ret.setOk();
		ret.set(SiteInfo.msgKey, "保存成功");
		return ret;
	}
	
	public Ret updateArt(Article article, String[] files, Account account){
		Article temp = getById(article.getId());
		temp.setTitle(article.getTitle());
		temp.setSubtitle(article.getSubtitle());
		temp.setColumn(article.getColumn());
		temp.setContent(article.getContent());
		temp.setContentText(article.getContentText());
		temp.setIsTop(article.getIsTop());
		temp.setThumbnail(article.getThumbnail());
		if(files==null){
			temp.setFiles("");
		} else if(files!=null && files.length>0){
			temp.setFiles(StrKit.join(files,","));
		}
		/*
		 * update 2020.10.15
		 * 是否置顶改为switch，否的时候数据为空，兼容历史数据进行手动处理
		 */
		if(StrKit.isBlank(article.getIsTop())){
			temp.setIsTop("0");
		}
		if(article.getThumbnail()==null || article.getThumbnail()<=0){
			temp.setThumbnail(0);
		}
		temp.setUpdateAccount(account.getId());
		temp.setUpdateTime(new Date());
		boolean flag = temp.update();
		Ret ret = Ret.create();
		if(flag){
			ret.setOk();
			ret.set(SiteInfo.msgKey, "更新成功");
		} else {
			ret.setFail();
			ret.set(SiteInfo.msgKey, "更新失败，请联系系统管理员！");
		}
		return ret;
	}
	
	/**
	 * 分页查询文章
	 * @param page		页码
	 * @param limit		每页显示条数
	 * @param title		文章标题
	 * @param column	文章栏目
	 * @param status	文章状态
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Page<Article> queryArtsPage(int page, int limit, String title, int column, String type, String status){
		Kv kv = Kv.create();
		kv.put("siteId", SiteInfo.siteId);
		if(StrKit.notBlank(title)){
			kv.put("title", "%"+title+"%");
		}
		kv.put("column", column);
		kv.put("type", type);
		kv.put("status", status);
		return dao.template("article.queryArtsPage", kv).paginate(page, limit);
	}
	
	public Ret deleteArt(int id, Account account){
		Ret ret = Ret.create();
		Article article = getById(id);
		article.setStatus(SiteInfo.statusDel);
		article.setUpdateAccount(account.getId());
		article.setUpdateTime(new Date());
		boolean flag = article.update();
		if(flag){
			ret.setOk();
			ret.set(SiteInfo.msgKey, "删除成功");
		} else {
			ret.setFail();
			ret.set(SiteInfo.msgKey, "删除失败，请联系管理员");
		}
		return ret;
	}
	
	public Ret publishArt(int id, Account account){
		Ret ret = Ret.create();
		Article article = getById(id);
		article.setStatus(SiteInfo.statusNormal);
		article.setUpdateAccount(account.getId());
		article.setUpdateTime(new Date());
		boolean flag = article.update();
		if(flag){
			ret.setOk();
			ret.set(SiteInfo.msgKey, "发布成功");
		} else {
			ret.setFail();
			ret.set(SiteInfo.msgKey, "发布失败，请联系管理员！");
		}
		return ret;
	}
	
	public Article getById(int id){
		return dao.findById(id);
	}
	
	/**
	 * 删除指定栏目所有文章
	 * @param columnId
	 * @return
	 */
	public Ret delArtByCol(int columnId, Account account){
		Column column = colSrv.getById(columnId);
		String children = column.getChildren();
		if(StrKit.notBlank(children)){
			String[] idStrings = children.split("\\|");//需要进行转译
			for(int i=0,j=idStrings.length; i<j; i++){
				delArtByCol(Integer.parseInt(idStrings[i]), account);
			}
		} else {
			String delArtByColSql = dao.getSql("article.delArtByCol");
			Db.update(delArtByColSql, SiteInfo.statusDel, account.getId(), columnId, SiteInfo.siteId);
		}
		return Ret.ok();
	}
	
	/**
	 * 查询文章总数(未删除的)
	 * 
	 * @return
	 * ret{state=ok, artNums=0, picNums=0, videoNums=0, otherNums=0, nums=0}
	 */
	public Ret queryArticleNums(){
		Ret ret = Ret.ok();
		
		String sql = Db.getSql("article.queryArticleNums");
		List<Record> records = Db.find(sql, SiteInfo.siteId);
		String type = "";
		int artNums=0, picNums=0, videoNums=0,otherNums=0, countNums=0;
		for(Record record : records){
			type = record.getStr("type");
			if("1".equals(type)){//普通文章
				artNums = record.getInt("num");
			} else if ("2".equals(type)) {//图集
				picNums = record.getInt("num");
			} else if ("3".equals(type)) {//视频
				videoNums = record.getInt("num");
			} else {//其他
				otherNums = record.getInt("num");
			}
		}
		countNums = artNums + picNums + videoNums + otherNums;
		ret.set("artNums", artNums);
		ret.set("picNums", picNums);
		ret.set("videoNums", videoNums);
		ret.set("otherNums", otherNums);
		ret.set("countNums", countNums);
		
		return ret;
	}
	
	/**
	 * 查询文章点击数
	 * 
	 * @return
	 */
	public int countArtClickNums(){
		String sql = Db.getSql("article.countArtClickNums");
		return Db.queryInt(sql, SiteInfo.siteId);
	}
}
