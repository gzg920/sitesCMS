package com.sites.cms.article;

import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.validate.Validator;
import com.sites.common.SiteInfo;

/**
 * 文章校验<br/>
 * 用于文章类新增、修改方法，建议在方法前使用
 * 
 * @author zyg
 * 2020年10月19日 下午9:00:57
 */
public class ArticleValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		setShortCircuit(true);
		validateRequired("title", SiteInfo.msgKey, "标题不可为空！");
		validateString("title", 1, 128, SiteInfo.msgKey, "标题最长为128个字符！");
		validateString("subtitle", 0, 512, SiteInfo.msgKey, "摘要最长为512个字符！");
		int column = c.getParaToInt("column");
		if(column <= 0){
			addError(SiteInfo.msgKey, "请选择对应栏目！");
		}
		validateRequired("content", SiteInfo.msgKey, "内容不可为空！");
	}

	@Override
	protected void handleError(Controller c) {
		Ret ret = Ret.fail(SiteInfo.msgKey, c.getAttr(SiteInfo.msgKey));
		c.renderJson(ret);
	}

}
