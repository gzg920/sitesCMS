package com.sites.cds.common;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.render.Render;
import com.jfinal.render.TemplateRender;
import com.sites.common.SiteInfo;

/**
 * cds中动态切换站点目录的拦截器
 * 
 * @author zyg
 * 2020年2月9日 下午6:30:04
 */
public class ViewPathInterceptor implements Interceptor {

	public void intercept(Invocation inv) {
		inv.invoke();
		
		Render r = inv.getController().getRender();
		if (r instanceof TemplateRender) {
			TemplateRender render = (TemplateRender)r;
			String view = render.getView();
			// 这里动态识别当前站点，渲染当前站点下的页面
			render.setView(SiteInfo.siteSign + "/" + view);
		}

	}

}
