package com.sites.cds.common.directive;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.sites.common.SiteInfo;
import com.sites.common.model.Article;
import com.sites.common.model.Column;
import com.sites.common.model.Files;

/**
 * 自定义指令的服务层
 * 
 * @author zyg
 * 2020年2月8日 下午3:25:34
 */
public class DirectiveService {

	private Article artDao = new Article().dao();
	private Column colDao = new Column().dao();
	private Files filesDao = new Files().dao();
	
	/**
	 * 根据栏目的英文名获取指定条数的文章列表
	 * @param columnEnName	栏目英文名
	 * @param listSize		列表条数
	 * @return
	 */
	public List<Article> getArtListByCol(String columnEnName, int listSize){
		String sql = artDao.getSql("directive.getArtListByCol");
		return artDao.find(sql, columnEnName, SiteInfo.siteId, listSize);
	}
	
	/**
	 * 根据栏目英文名获取子栏目列表
	 * @param columnEnName	栏目英文名
	 * @return
	 */
	public List<Column> getColChildrenByCol(String columnEnName){
		String sql = colDao.getSql("directive.getColChildrenByCol");
		return colDao.find(sql, columnEnName, SiteInfo.siteId);
	}
	
	/**
	 * 根据栏目ids获取指定栏目下的最新文章
	 * @param columnIds
	 * @param listSize
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Article> getArtListByColIds(List<Integer> columnIds, int listSize){
		Kv kv = Kv.create();
		kv.put("siteId", SiteInfo.siteId);
		kv.put("listSize", listSize);
		kv.put("columnIds", columnIds);
		return artDao.template("directive.getArtListByColIds", kv).find();
	}
	
	/**
	 * 根据栏目英文名获取指定栏目下的最新文章
	 * @param colEnNameList	栏目英文名列表
	 * @param listSize		文章列表长度
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Article> getArtListByColEnNames(List<String> colEnNameList, int listSize){
		Kv kv = Kv.create();
		kv.put("siteId", SiteInfo.siteId);
		kv.put("listSize", listSize);
		kv.put("colEnNameList", colEnNameList);
		return artDao.template("directive.getArtListByColEnNames", kv).find();
	}
	
	/**
	 * 根据栏目英文名获取指定栏目下的文章
	 * 
	 * @param colEnNameList
	 * @param listSize
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Article> getHotArtListByColEnNames(List<String> colEnNameList, int listSize){
		Kv kv = Kv.create();
		kv.put("siteId", SiteInfo.siteId);
		kv.put("listSize", listSize);
		kv.put("colEnNameList", colEnNameList);
		return artDao.template("directive.getHotArtListByColEnNames", kv).find();
	}
	
	/**
	 * 根据ids获取filesList
	 * 
	 * @param filesIds
	 * @return
	 */
	public List<Files> getFilesByIds(String filesIds){
		List<Files> files = new ArrayList<Files>();
		if(StrKit.isBlank(filesIds)){
			return files;
		}
		
		String[] ids = filesIds.split(",");
		Kv kv = Kv.create();
		kv.set("ids", ids);
		
		files = filesDao.template("directive.getFilesByIds", kv).find();
		return files;
	}
	
	/**
	 * 获取上一篇或者下一篇文章
	 * 
	 * @param artId	文章id
	 * @param columnId	栏目id
	 * @param flag	上一篇或者下一篇的标记，prior表示上一篇，next表示下一篇
	 * @return
	 */
	public Article getArtPriorNext(int artId, int columnId, String flag){
		Kv kv = Kv.by("artId", artId).set("columnId", columnId);
		if("next".equals(flag)){
			return artDao.template("directive.getArtNext", kv).findFirst();
		} else {
			return artDao.template("directive.getArtPrior", kv).findFirst();
		}
	}
}
