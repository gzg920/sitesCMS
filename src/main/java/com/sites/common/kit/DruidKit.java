package com.sites.common.kit;

import com.alibaba.druid.filter.config.ConfigTools;

/**
 * Druid工具类
 * 
 * @author zyg
 * 2020年8月1日 下午1:01:38
 */
public class DruidKit {

	/**
	 * 直接运行这个方法就可以控制台输出加密后的信息了
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		encryptPwd("数据库密码");
	}
	
	/**
	 * 加密数据库密码
	 * 注意：publicKey和password必须是使用同一次生成的，不然会解密失败
	 * 
	 * @param password	需要加密的密码
	 * @throws Exception
	 */
	public static void encryptPwd(String password) throws Exception {
		String[] arr = ConfigTools.genKeyPair(512);
		// 私钥，用于生成密文密码用，不用管
		System.out.println("privateKey:" + arr[0]);
		// 公钥，配置到cmsConfig.txt中的publicKey项
        System.out.println("publicKey:" + arr[1]);
        // 加密后的密码，配置到cmsConfig.txt中的password项
        System.out.println("password:" + ConfigTools.encrypt(arr[0], password));
	}
	
}
